package com.myshipment.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.myshipment.R;
import com.myshipment.app.model.SpinnerDataSet;

public class KPISpinnerBaseAdapter implements SpinnerAdapter {

	 	private Context context;
		private int sp;
	    /**
	     * The internal data (the ArrayList with the Objects).
	     */
	    private ArrayList<SpinnerDataSet> data;

	    public KPISpinnerBaseAdapter(Context context, ArrayList<SpinnerDataSet> data){
	        this.context = context;
	        this.data = data;
	        this.sp = (int) (context.getResources().getDimension(R.dimen.dashboardTabBarwidth_textsize) / context.getResources().getDisplayMetrics().density);
	    }
	    
	    /**
	     * Returns the Size of the ArrayList
	     */
	    @Override
	    public int getCount() {
	        return data.size();
	    }

	    @Override
	    public Object getItem(int position) {
	        return data.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return 0;
	    }

	    @Override
	    public int getItemViewType(int position) {
	        return 0;
	    }

	    @Override
	    public int getViewTypeCount() {
	        return 1;
	    }

	    @Override
	    public boolean hasStableIds() {
	        return false;
	    }

	    @Override
	    public boolean isEmpty() {
	        return false;
	    }
	    
	    @Override
	    public View getDropDownView(int position, View convertView, ViewGroup parent) {
	        if (convertView == null) {
	            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = vi.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
	            convertView.setBackgroundColor(Color.parseColor("#E5E5E5"));
	        }
	        TextView txt=(TextView) convertView;
	        txt.setTextColor(Color.parseColor("#737373"));
	        txt.setTypeface(null, Typeface.NORMAL);
	        txt.setText(data.get(position).getName());
	        txt.setTextSize(sp);
	        return convertView;

	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        TextView textView = (TextView) View.inflate(context, android.R.layout.simple_spinner_item, null);
	        textView.setTextColor(Color.parseColor("#737373"));
	        textView.setTypeface(null, Typeface.NORMAL);
	        textView.setText(data.get(position).getName());
	        textView.setTextSize(sp);
	        return textView;
	    }


	    @Override
	    public void registerDataSetObserver(DataSetObserver observer) {

	    }

	    @Override
	    public void unregisterDataSetObserver(DataSetObserver observer) {

	    }

	
}
