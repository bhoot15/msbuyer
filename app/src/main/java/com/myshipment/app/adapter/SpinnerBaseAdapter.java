package com.myshipment.app.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.myshipment.app.model.SpinnerDataSet;

public class SpinnerBaseAdapter implements SpinnerAdapter {

	private Context context;
	/**
	 * The internal data (the ArrayList with the Objects).
	 */
	private ArrayList<SpinnerDataSet> data;
	private int textSize_;

	public SpinnerBaseAdapter(Context context, ArrayList<SpinnerDataSet> data, int textSize) {
		this.context = context;
		this.data = data;
		this.textSize_ = textSize;
	}

	/**
	 * Returns the Size of the ArrayList
	 */
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
			// convertView.setBackgroundColor(Color.parseColor("#1E272E"));
			convertView.setBackgroundColor(Color.parseColor("#4A7484"));

		}
		TextView txt = (TextView) convertView;
		txt.setTextColor(Color.WHITE);
		txt.setTextSize(textSize_);
		(txt).setText(data.get(position).getName());
		return convertView;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView textView = (TextView) View.inflate(context, android.R.layout.simple_spinner_item, null);
		textView.setTextColor(Color.WHITE);
		textView.setTextSize(textSize_);
		textView.setText(data.get(position).getName());
		return textView;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

}
