//package com.myshipmentBuyer.adapter;
//
//import java.util.ArrayList;
//
//import com.myshipment.R;
//import com.myshipment.app.model.IncidentRepor;
//
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Color;
//import android.net.Uri;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.TextView;
//
//public class IncidentBaseAdapter extends BaseAdapter {
//
//	private Context context;
//	private ArrayList<IncidentRepor> incidentDatas;
//	private LayoutInflater nInflater;
//
//	public IncidentBaseAdapter(Context context, ArrayList<IncidentRepor> incidentDatas) {
//		this.context = context;
//		this.incidentDatas = incidentDatas;
//		nInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//	}
//
//	public void refreshData(ArrayList<IncidentRepor> incidentReporArrayList) {
//		this.incidentDatas = incidentReporArrayList;
//		notifyDataSetChanged();
//	}
//
//	@Override
//	public int getCount() {
//		return incidentDatas.size();
//	}
//
//	@Override
//	public Object getItem(int arg0) {
//		return incidentDatas.get(arg0);
//	}
//
//	@Override
//	public long getItemId(int arg0) {
//		return arg0;
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		ViewHolder holder = null;
//		if (convertView == null) {
//			holder = new ViewHolder();
//			convertView = nInflater.inflate(R.layout.inflator_incident_details, null, false);
//			holder.inflator_incident_text_delay = (TextView) convertView.findViewById(R.id.inflator_incident_text_delay);
//			holder.inflator_incident_text_time = (TextView) convertView.findViewById(R.id.inflator_incident_text_time);
//			convertView.setTag(holder);
//		} else {
//			holder = (ViewHolder) convertView.getTag();
//		}
//
////		IncidentRepor mIncidentRepor = incidentDatas.get(position);
////		final String link = mIncidentRepor.getLink();
////		holder.inflator_incident_text_delay.setText(mIncidentRepor.getIncidentReport());
////		holder.inflator_incident_text_time.setText(mIncidentRepor.getIncidentDate());
////		convertView.setBackgroundColor((position % 2 == 0) ? Color.parseColor("#1E272E") : Color.parseColor("#28333B"));
//
////		if (!TextUtils.isEmpty(link)) {
////			convertView.setOnClickListener(new OnClickListener() {
////
////				@Override
////				public void onClick(View v) {
////					Intent i = new Intent(Intent.ACTION_VIEW);
////					i.setData(Uri.parse(link));
////					context.startActivity(i);
////				}
////			});
////		}
//
//		return convertView;
//	}
//
//	private static class ViewHolder {
//		private TextView inflator_incident_text_delay, inflator_incident_text_time;
//
//	}
//}
