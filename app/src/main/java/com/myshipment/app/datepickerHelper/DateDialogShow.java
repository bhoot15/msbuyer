package com.myshipment.app.datepickerHelper;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.widget.DatePicker;

import com.myshipment.app.model.DateMaxMin;

public class DateDialogShow {
	private Context mContext;
	private DateMaxMin dateMaxMin;
	private boolean isYearGone = false;

	public DateDialogShow(Context context, DateMaxMin dateMaxMin) {
		this.mContext = context;
		this.dateMaxMin = dateMaxMin;
	}

	public void setYearNotVisible(boolean isYearGone_) {
		isYearGone = isYearGone_;
	}

	public void customDatePicker() {
		if (dateMaxMin == null) {
			return;
		}

		DatePickerDialog mDatePickerDialog = new DatePickerDialog(mContext, mDateSetListner, dateMaxMin.getCurrentYear(), dateMaxMin.getCurrentMonth(), dateMaxMin.getCurrentDay());
		try {
			Field[] datePickerDialogFields = mDatePickerDialog.getClass().getDeclaredFields();
			for (Field datePickerDialogField : datePickerDialogFields) {
				if (datePickerDialogField.getName().equals("mDatePicker")) {
					datePickerDialogField.setAccessible(true);
					DatePicker datePicker = (DatePicker) datePickerDialogField.get(mDatePickerDialog);

					Field datePickerFields[] = null;

					if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
						datePicker.setCalendarViewShown(false);
						datePicker.setMinDate(new Date(dateMaxMin.getMinYear() + "/" + dateMaxMin.getMinMonth() + "/" + dateMaxMin.getMinDay()).getTime());

						datePicker.setMaxDate(Calendar.getInstance().getTimeInMillis());
						datePickerFields = datePickerDialogField.getType().getDeclaredFields();
					}

					if (datePickerFields != null) {
						for (Field datePickerField : datePickerFields) {
							if ("mDayPicker".equals(datePickerField.getName()) || "mDaySpinner".equals(datePickerField.getName())) {
								datePickerField.setAccessible(true);
							}
						}
					}
				}

			}
			mDatePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			mDatePickerDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private DatePickerDialog.OnDateSetListener mDateSetListner = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year_, int monthOfYear_, int dayOfMonth_) {
			if (datePickerInterface != null) {
				String day_ = (dayOfMonth_ <= 9) ? "0" + dayOfMonth_ : "" + dayOfMonth_;
				String month_ = ((monthOfYear_ + 1) <= 9) ? "0" + (monthOfYear_ + 1) : "" + (monthOfYear_ + 1);
				datePickerInterface.UpDateDate(day_ + "/" + month_ + "/" + year_);
			}
		}
	};

	public DatePickerInterface datePickerInterface;

	public DatePickerInterface getDatePickerInterface() {
		return datePickerInterface;
	}

	public void setDatePickerInterface(DatePickerInterface datePickerInterface) {
		this.datePickerInterface = datePickerInterface;
	}

	public DatePickerDialog.OnDateSetListener getmDateSetListner() {
		return mDateSetListner;
	}

	public void setmDateSetListner(DatePickerDialog.OnDateSetListener mDateSetListner) {
		this.mDateSetListner = mDateSetListner;
	}

	public interface DatePickerInterface {
		public void UpDateDate(String upDateDate);
	}
}
