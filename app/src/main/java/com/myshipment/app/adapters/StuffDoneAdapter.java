package com.myshipment.app.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myshipment.R;
import com.myshipment.app.Utility.GeneralUtils;
import com.myshipment.app.Utility.SharedPreferenceManager;
import com.myshipment.app.beans.TrackingDataNew;

import java.util.List;
import java.util.Objects;

/**
 * Created by xubair shoumik on 20/10/2017.
 */

public class StuffDoneAdapter extends RecyclerView.Adapter<StuffDoneAdapter.totalShipmentViewHolder> {

    private Context mContext;
    private LayoutInflater layoutInflater;
    private List<TrackingDataNew> trackingDataNewList;

    public StuffDoneAdapter(Context context) {
        this.mContext = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public StuffDoneAdapter(Context mContext, List<TrackingDataNew> dashboardInfoBoxes) {
        this.mContext = mContext;
        this.trackingDataNewList = dashboardInfoBoxes;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public totalShipmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.stuffing_done_row_layout, parent, false);
        return new totalShipmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final totalShipmentViewHolder holder, int position) {

        final TrackingDataNew dashboardInfoBox = trackingDataNewList.get(position);
        holder.bl_no_textview.setText(dashboardInfoBox.getBl_no());
        holder.bl_date_textview.setText(GeneralUtils.getDate(dashboardInfoBox.getBl_bt()));
        holder.buyer_name_textview.setText(dashboardInfoBox.getBuyer());
        holder.shipper_name_textview.setText(dashboardInfoBox.getShipper());
        holder.pol_textview.setText(dashboardInfoBox.getPol());
        holder.pod_textview.setText(dashboardInfoBox.getPod());
        holder.total_quantity_textview.setText(dashboardInfoBox.getTot_qty());

        if (Objects.equals(SharedPreferenceManager.getDivision(mContext), "SE")) {
            holder.gross_weight.setText("Total Gross Weight");
            holder.total_gross_or_charge_weight_textview.setText(dashboardInfoBox.getGrs_wt());
        } else {
            holder.gross_weight.setText("Total Charge Weight");
            holder.total_gross_or_charge_weight_textview.setText(dashboardInfoBox.getCrg_wt());
        }

        holder.total_cbm_textview.setText(dashboardInfoBox.getVolume());


        if (dashboardInfoBox.getEta_dt() != "") {
            holder.date_textview.setText(GeneralUtils.getDate(dashboardInfoBox.getEta_dt()));
            holder.date.setText("ETA (*EST)");
        } else if (dashboardInfoBox.getLoad_dt() != "") {
            holder.date_textview.setText(GeneralUtils.getDate(dashboardInfoBox.getDep_dt()));
            holder.date.setText("ETD (*EST)");
        } else if (dashboardInfoBox.getStuff_dt() != "") {
            holder.date_textview.setText(GeneralUtils.getDate(dashboardInfoBox.getStuff_dt()));
            holder.date.setText("Stuffing Date");
        } else if (dashboardInfoBox.getGr_dt() != "") {
            holder.date_textview.setText(GeneralUtils.getDate(dashboardInfoBox.getGr_dt()));
            holder.date.setText("Goods Received Date");
        } else if (dashboardInfoBox.getBl_bt() != "") {
            holder.date.setText("Booking Date");
            holder.date_textview.setText(GeneralUtils.getDate(dashboardInfoBox.getBl_bt()));
        }

    }




    @Override
    public int getItemCount() {

        return trackingDataNewList.size();
    }


    public class totalShipmentViewHolder extends RecyclerView.ViewHolder {

        public TextView bl_no_textview, bl_date_textview, buyer_name_textview, shipper_name_textview, pol_textview, pod_textview, total_quantity_textview, total_gross_or_charge_weight_textview, total_cbm_textview, gross_weight, date_textview,date;

        public totalShipmentViewHolder(View itemView) {
            super(itemView);
            bl_no_textview = (TextView) itemView.findViewById(R.id.bl_value);
            bl_date_textview = (TextView) itemView.findViewById(R.id.bl_date_value);
            buyer_name_textview = (TextView) itemView.findViewById(R.id.buyer_name_value);
            shipper_name_textview = (TextView) itemView.findViewById(R.id.shipper_name_value);
            pol_textview = (TextView) itemView.findViewById(R.id.pol_value);
            pod_textview = (TextView) itemView.findViewById(R.id.pod_value);
            total_quantity_textview = (TextView) itemView.findViewById(R.id.total_quantity_value);
            total_gross_or_charge_weight_textview = (TextView) itemView.findViewById(R.id.gross_weight_value);
            total_cbm_textview = (TextView) itemView.findViewById(R.id.total_cbm_value);
            date_textview = (TextView) itemView.findViewById(R.id.date_value);
            gross_weight = (TextView) itemView.findViewById(R.id.gross_weight);
            date = (TextView) itemView.findViewById(R.id.date);
        }
    }

}

