package com.myshipment.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.myshipment.R;
import com.myshipment.app.Utility.GeneralUtils;
import com.myshipment.app.beans.ArrayListManagerModel;
import com.myshipment.app.utils.VectorDrawableUtils;


/**
 * Created by Akshay Thapliyal on 26-05-2016.
 */
public class TimelineViewAdapter extends RecyclerView.Adapter<TimelineViewAdapter.TimeLineViewHolder> {
    private String data[] = {"Booking Confirmed   ", "Goods Received   ", "Stuffing Done   "/*, "In Transit   "*/};
    //,"Stage 5", "Stage 6", "Stage 7", "Stage 8", "Stage 9"
    private String checkBookingDate = "", checkGoodsReceived = "", checkStuffingDone = "" /*, checkInTransit = ""*/;
    private Context mContext;
    private ArrayListManagerModel mArrayListManagerModel;

    public TimelineViewAdapter(Context context, ArrayListManagerModel arrayListManagerModel) {
        this.mContext = context;
        this.mArrayListManagerModel = arrayListManagerModel;

    }

    @Override
    public TimelineViewAdapter.TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.milestone_timeline_layout, null);
        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {
        holder.tvName.setText(data[position]);
        holder.mTimelineView.setActivated(false);
        checkBookingDate = GeneralUtils.getDate(mArrayListManagerModel.getTrackingDataNewArrayList().get(0).getBl_bt());
        checkGoodsReceived = GeneralUtils.getDate(mArrayListManagerModel.getTrackingDataNewArrayList().get(0).getGr_dt());
        checkStuffingDone = GeneralUtils.getDate(mArrayListManagerModel.getTrackingDataNewArrayList().get(0).getStuff_dt()); //for sea , if air then shipment_dt
        //checkInTransit = GeneralUtils.getDate(mArrayListManagerModel.getTrackingDataNewArrayList().get(0).getLoad_dt());
        //checkInTransit ="";


        if (checkBookingDate == null || checkBookingDate.isEmpty() && position == 0) {
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_grey));
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_inactive, android.R.color.darker_gray));
            holder.iconStatusTrue.setImageResource(R.drawable.ic_cancel_black_36dp);

        } else {
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_active, R.color.colorPrimary));
        }
        if (checkGoodsReceived == null || checkGoodsReceived.isEmpty() && position == 1) {
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_inactive, android.R.color.darker_gray));
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.iconStatusTrue.setImageResource(R.drawable.ic_cancel_black_36dp);

        } else {
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_active, R.color.colorPrimary));
        }
        if (checkStuffingDone == null || checkStuffingDone.isEmpty() && position == 2) {
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_inactive, android.R.color.darker_gray));
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.iconStatusTrue.setImageResource(R.drawable.ic_cancel_black_36dp);

        } else {
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_active, R.color.colorPrimary));
        }
        /*if (checkInTransit == null || checkInTransit.isEmpty()) {
            if (position == 3) {
                if (checkStuffingDone != null  && checkGoodsReceived != null && checkBookingDate !=null) {
                    holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_inactive, android.R.color.darker_gray));
                    holder.tvName.setTextColor(mContext.getResources().getColor(R.color.yellow));
                    holder.iconStatusTrue.setImageResource(R.drawable.ic_cancel_black_36dp);
                }
                else{
                    holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_inactive, android.R.color.darker_gray));
                    holder.tvName.setTextColor(mContext.getResources().getColor(R.color.color_dark_grey));
                    holder.iconStatusTrue.setImageResource(R.drawable.ic_cancel_grey_36dp);

                }

            }
        } else {
            holder.tvName.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            //holder.dateText.setText("NA");
        }*/

        if (position == 0) {
            holder.dateText.setText(checkBookingDate);
        } else if (position == 1) {
            holder.dateText.setText(checkGoodsReceived);
        } else if (position == 2) {
            holder.dateText.setText(checkStuffingDone);
        } /*else if (position == 3) {
            holder.dateText.setText(checkInTransit);
        }*/
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class TimeLineViewHolder extends RecyclerView.ViewHolder {
        public TimelineView mTimelineView;
        TextView tvName, dateText;
        ImageView iconStatusFalse, iconStatusTrue;

        public TimeLineViewHolder(View itemView, int viewType) {
            super(itemView);
            mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
            tvName = (TextView) itemView.findViewById(R.id.text_timeline_title);
            dateText = (TextView) itemView.findViewById(R.id.text_timeline_date);
            iconStatusTrue = (ImageView) itemView.findViewById(R.id.icon_status);
            mTimelineView.initLine(viewType);
        }
    }
}
