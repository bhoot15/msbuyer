package com.myshipment.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myshipment.R;
import com.myshipment.app.Utility.GeneralUtils;
import com.myshipment.app.beans.ArrayListManagerModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Akshay Thapliyal on 26-05-2016.
 */
public class ProductDetailsAdapter extends RecyclerView.Adapter<ProductDetailsAdapter.MyViewHolder> {

    private String type[] =
            {
                    "HBL No ",
                    "HBL Date ",
                    "Shipper Name ",
                    "Consignee Name ",
                    "Port Of Loading ",
                    "Port Of Discharge ",
                    "Place Of Discharge ",
                    "TOS ",
                    "Total Quantity ",
                    "Total Volume(CBM) ",
                    "Total Gross Weight ",
                    "Commercial Invoice No. ",
                    "Commercial Invoice Date ",
                    "Place of Receipt ",
                    "Cargo Received Date ",
                    "MBL No.",
                    "Shipping Line ",
                    "Stuffing Date ",
                    "Shipped on Board ",
                    "ETD ",
                    "ETA ",
                    "Destination Agent "
            };
    private String data;
    private Context context;
    private ArrayListManagerModel arrayListManagerModel;


    public ProductDetailsAdapter(Context context, ArrayListManagerModel arrayListManagerModel) {
        this.context = context;
        this.arrayListManagerModel = arrayListManagerModel;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_details_layout, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvType.setText(type[position]);

        switch (position) {
            case 0:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getBl_no();
                break;
            case 1:
                data = GeneralUtils.getDate(arrayListManagerModel.getTrackingDataNewArrayList().get(0).getBl_bt());
                //data = "NA";
                break;
            case 2:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getShipper();
                break;
            case 3:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getBuyer();
                break;
            case 4:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getPol();

                /*SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.context);
                SharedPreferences.Editor edit = sp.edit();
                edit.putString("pol",arrayListManagerModel.getTrackingDataNewArrayList().get(0).getPol());
                edit.apply();*/
                break;
            case 5:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getPod();

                /*SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("pod",arrayListManagerModel.getTrackingDataNewArrayList().get(0).getPod());
                editor.apply();*/
                break;
            case 6:
                //data = "NA";
                //data = arrayListManagerModel.getTrackingDataArrayList().get(0).getDocumentNo();
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getPlod();
                break;
            case 7:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getFrt_mode_ds();
                break;
            case 8:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getTot_qty();
                break;
            case 9:
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getVolume();
                break;
            case 10:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getGrs_wt();
                break;
            case 11:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getComm_inv();
                break;
            case 12:
                //data = "NA";
                data = GeneralUtils.getDate(arrayListManagerModel.getTrackingDataNewArrayList().get(0).getComm_inv_dt());
                break;
            case 13:
                //data = "NA";//
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getPor();
                break;
            case 14:
                //data = "NA";
                data = GeneralUtils.getDate(arrayListManagerModel.getTrackingDataNewArrayList().get(0).getGr_dt());
                break;
            case 15:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getMbl_no();
                break;
            case 16:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getCarrier();
                break;
            case 17:
                //data = "NA";
                data = GeneralUtils.getDate(arrayListManagerModel.getTrackingDataNewArrayList().get(0).getStuff_dt());
                break;
            case 18:
                //data = "NA";
                data = GeneralUtils.getDate(arrayListManagerModel.getTrackingDataNewArrayList().get(0).getDep_dt());
                break;
            case 19:
                //data = "NA";
                data = GeneralUtils.getDate(arrayListManagerModel.getTrackingDataNewArrayList().get(0).getDep_dt());
                break;
            case 20:
                //data = "NA";
                data = GeneralUtils.getDate(arrayListManagerModel.getTrackingDataNewArrayList().get(0).getEta_dt());
                break;
            case 21:
                //data = "NA";
                data = arrayListManagerModel.getTrackingDataNewArrayList().get(0).getAgent();
                break;
        }
        holder.tvValue.setText(data);
    }

    private String getDate(String date) {
        String dateString = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return DateFormat.getDateInstance().format(convertedDate);
    }

    @Override
    public int getItemCount() {
        return type.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvType, tvValue;

        public MyViewHolder(View view) {
            super(view);
            tvType = (TextView) view.findViewById(R.id.tvType);
            tvValue = (TextView) view.findViewById(R.id.tvValue);
        }
    }
}
