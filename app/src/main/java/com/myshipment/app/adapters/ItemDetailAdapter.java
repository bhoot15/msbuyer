package com.myshipment.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myshipment.R;
import com.myshipment.app.beans.ArrayListManagerModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Zobair Alam on 15-10-2017.
 */
public class ItemDetailAdapter extends RecyclerView.Adapter<ItemDetailAdapter.MyViewHolder> {

    private String type [] ={
            "PO No ",
            "Commodity " ,
            "Style ",
            "Size ",
            "SKU ",
            "Article No ",
            "Color ",
            "Pieces ",
            "Carton ",
            "Volume ",
            "Weight "
    };
    private String data ;
    private Context context;
    private ArrayListManagerModel arrayListManagerModel;
    public ItemDetailAdapter(Context context, ArrayListManagerModel arrayListManagerModel){
        this.context = context;
        this.arrayListManagerModel = arrayListManagerModel;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_layout, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvType.setText(type[position]);

        switch (position){
            case 0 : data =  arrayListManagerModel.getItemDataArrayList().get(0).getPo_no();
                break;
            case 1 : data =   arrayListManagerModel.getItemDataArrayList().get(0).getCommodity();
                break;
            case 2 : data = arrayListManagerModel.getItemDataArrayList().get(0).getStyle();
                break;
            case 3 : data = arrayListManagerModel.getItemDataArrayList().get(0).getSize();
                break;
            case 4 : data = arrayListManagerModel.getItemDataArrayList().get(0).getSku();
                break;
            case 5 : data =  arrayListManagerModel.getItemDataArrayList().get(0).getArt_no();
                break;
            case 6: data = arrayListManagerModel.getItemDataArrayList().get(0).getColor();
                break;
            case 7: data = arrayListManagerModel.getItemDataArrayList().get(0).getItem_pcs();
                break;
            case 8: data = arrayListManagerModel.getItemDataArrayList().get(0).getCart_sno();
                break;
            case 9: data = arrayListManagerModel.getItemDataArrayList().get(0).getItem_vol();
                break;
            case 10: data = arrayListManagerModel.getItemDataArrayList().get(0).getItem_grwt();
                break;
        }
        holder.tvValue.setText(data);
    }

    private String getDate(String date){
        String dateString = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return DateFormat.getDateInstance().format(convertedDate);
    }

    @Override
    public int getItemCount() {
        return type.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvType, tvValue;

        public MyViewHolder(View view) {
            super(view);
            tvType = (TextView) view.findViewById(R.id.tvType);
            tvValue = (TextView)view.findViewById(R.id.tvValue);
        }
    }
}
