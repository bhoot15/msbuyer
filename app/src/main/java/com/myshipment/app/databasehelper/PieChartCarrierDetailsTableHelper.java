package com.myshipment.app.databasehelper;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.myshipment.app.MyShipmentApplication;
import com.myshipment.app.model.CarrierDetail;

public class PieChartCarrierDetailsTableHelper {
	private Context mContext;
	private static PieChartCarrierDetailsTableHelper instance = null;
	private MyShipmentDBHelper db = null;

	/**
	 * Table Column Name
	 */

	public final static String PIECHARTCARRIERDETAILS_TABLE = "PieChartCarrierDetails";

	public final static String PIECHARTCARRIERDETAILS_Real = "Real";
	public final static String PIECHARTCARRIERDETAILS_DelayDays = "DelayDays";
	public final static String PIECHARTCARRIERDETAILS_Initial = "Initial";
	public final static String PIECHARTCARRIERDETAILS_ContainerNumber = "ContainerNumber";
	public final static String PIECHARTCARRIERDETAILS_CarrierName = "CarrierName";
	public final static String PIECHARTCARRIERDETAILS_PolPod = "PolPod";
	public final static String PIECHARTCARRIERDETAILS_Type = "Type";
	public final static String PIECHARTCARRIERDETAILS_FromDate = "FromDate";
	public final static String PIECHARTCARRIERDETAILS_ToDate = "ToDate";
	public final static String PIECHARTCARRIERDETAILS_IsOrigin = "IsOrigin";

	public static synchronized PieChartCarrierDetailsTableHelper getInstance(Context context) {
		if (instance == null) {
			instance = new PieChartCarrierDetailsTableHelper(context);
		}
		return instance;
	}

	public PieChartCarrierDetailsTableHelper(Context context) {
		this.mContext = context;
		this.db = ((MyShipmentApplication) context.getApplicationContext()).DB_HELPER;
	}

	public void storeGraphOVData(CarrierDetail mCarrierDetail) {
		SQLiteDatabase sqlDB = null;
		try {
			sqlDB = db.getDataBase();
			sqlDB.beginTransaction();
			String sql = "Insert or Replace into " + PIECHARTCARRIERDETAILS_TABLE + " VALUES (?,?,?,?,?,?,?,?,?,?);";
			SQLiteStatement insert = sqlDB.compileStatement(sql);
			if (mCarrierDetail != null) {
				insert.bindString(1, "" + mCarrierDetail.getReal());
				insert.bindString(2, mCarrierDetail.getDelay_Days());
				insert.bindString(3, mCarrierDetail.getInitial());
				insert.bindString(4, mCarrierDetail.getContainer_Number());
				insert.bindString(5, mCarrierDetail.getCarrier_Name());
				insert.bindString(6, mCarrierDetail.getPOL_POD());
				insert.bindString(7, mCarrierDetail.getType());
				insert.bindString(8, mCarrierDetail.getFromdate());
				insert.bindString(9, mCarrierDetail.getTodate());
				insert.bindString(10, mCarrierDetail.getIsOrigin());
				insert.execute();
			}
			sqlDB.setTransactionSuccessful();
		} catch (Exception ex) {
			Log.w("error", ex.getMessage());
		} finally {
			sqlDB.endTransaction();
		}
	}

	public ArrayList<CarrierDetail> getListOfCarrier() {

		ArrayList<CarrierDetail> data = null;
		Cursor mCursor = null;
		try {
			SQLiteDatabase sqlDB = db.getDataBase();
			String qr = "Select * from " + PIECHARTCARRIERDETAILS_TABLE;
			mCursor = sqlDB.rawQuery(qr, null);

			if (mCursor != null && mCursor.getCount() >= 1) {
				data = new ArrayList<CarrierDetail>();

				while (mCursor.moveToNext()) {
					CarrierDetail mCarrierDetail = new CarrierDetail();

					mCarrierDetail.setReal(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_Real)));
					mCarrierDetail.setDelay_Days(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_DelayDays)));
					mCarrierDetail.setInitial(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_Initial)));
					mCarrierDetail.setContainer_Number(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_ContainerNumber)));
					mCarrierDetail.setCarrier_Name(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_CarrierName)));
					mCarrierDetail.setPOL_POD(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_PolPod)));
					mCarrierDetail.setType(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_Type)));
					mCarrierDetail.setFromdate(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_FromDate)));
					mCarrierDetail.setTodate(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_ToDate)));
					mCarrierDetail.setIsOrigin(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIERDETAILS_IsOrigin)));

					data.add(mCarrierDetail);
				}
			}
			closeCursor(mCursor);
		} catch (Exception e) {
			closeCursor(mCursor);
			e.printStackTrace();
		}

		return data;
	}

	private boolean deleteDataFromTable() {
		try {
			SQLiteDatabase sqlDB = db.getDataBase();
			long success = sqlDB.delete(PIECHARTCARRIERDETAILS_TABLE, null, null);
			if (success != -1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private void closeCursor(Cursor cursor) {
		try {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
				cursor = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
