package com.myshipment.app.databasehelper;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.myshipment.app.MyShipmentApplication;
import com.myshipment.app.model.ILN;

public class PieChartILNTableHelper {

	private Context mContext;
	private static PieChartILNTableHelper instance = null;
	private MyShipmentDBHelper db = null;

	/**
	 * Table Column Name
	 */

	public final static String PIECHARTILN_TABLE = "PieChartILN";

	public final static String PIECHARTILN_IndividualPercentage = "IndividualPercentage";
	public final static String PIECHARTILN_OverallPercentage = "OverallPercentage";
	public final static String PIECHARTILN_ILNName = "ILNName";
	public final static String PIECHARTILN_NoOfTypeShipment = "NoOfTypeShipment";
	public final static String PIECHARTILN_RGB = "RGB";
	public final static String PIECHARTILN_NoOfShipment = "NoOfShipment";
	public final static String PIECHARTILN_ILNID = "ILNID";
	public final static String PIECHARTILN_Type = "Type";
	public final static String PIECHARTILN_FromDate = "FromDate";
	public final static String PIECHARTILN_ToDate = "ToDate";
	public final static String PIECHARTILN_CarrierName = "CarrierName";
	public final static String PIECHARTILN_IsOrigin = "IsOrigin";

	public static synchronized PieChartILNTableHelper getInstance(Context context) {
		if (instance == null) {
			instance = new PieChartILNTableHelper(context);
		}
		return instance;
	}

	public PieChartILNTableHelper(Context context) {
		this.mContext = context;
		this.db = ((MyShipmentApplication) context.getApplicationContext()).DB_HELPER;
	}

	public void storeGraphOVData(ILN mIln) {
		SQLiteDatabase sqlDB = null;
		try {
			sqlDB = db.getDataBase();
			sqlDB.beginTransaction();
			String sql = "Insert or Replace into " + PIECHARTILN_TABLE + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
			SQLiteStatement insert = sqlDB.compileStatement(sql);
			if (mIln != null) {
				insert.bindString(1, "" + mIln.getIndividual_Percentage());
				insert.bindString(2, mIln.getOverall_Percentage());
				insert.bindString(3, mIln.getILN_Name());
				insert.bindString(4, mIln.getNo_Of_Shipment());
				insert.bindString(5, mIln.getRGB());
				insert.bindString(6, mIln.getNo_Of_Shipment());
				insert.bindString(7, mIln.getILNID());

				insert.bindString(8, mIln.getType());
				insert.bindString(9, mIln.getFromDate());
				insert.bindString(10, mIln.getToDate());
				insert.bindString(11, mIln.getCarrierName());
				insert.bindString(12, mIln.getIsOrigin());

				insert.execute();
			}
			sqlDB.setTransactionSuccessful();
		} catch (Exception ex) {
			Log.w("error", ex.getMessage());
		} finally {
			sqlDB.endTransaction();
		}
	}

	public ArrayList<ILN> getListOfCarrier() {

		ArrayList<ILN> data = null;
		Cursor mCursor = null;
		try {
			SQLiteDatabase sqlDB = db.getDataBase();
			String qr = "Select * from " + PIECHARTILN_TABLE;
			mCursor = sqlDB.rawQuery(qr, null);

			if (mCursor != null && mCursor.getCount() >= 1) {
				data = new ArrayList<ILN>();

				while (mCursor.moveToNext()) {
					ILN mILN = new ILN();

					mILN.setIndividual_Percentage(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_IndividualPercentage)));
					mILN.setOverall_Percentage(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_OverallPercentage)));
					mILN.setILN_Name(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_ILNName)));
					mILN.setNo_Of__Shipment(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_NoOfTypeShipment)));
					mILN.setRGB(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_RGB)));
					mILN.setNo_Of_Shipment(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_NoOfShipment)));
					mILN.setILNID(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_ILNID)));
					mILN.setType(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_Type)));
					mILN.setFromDate(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_FromDate)));
					mILN.setToDate(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_ToDate)));
					mILN.setCarrierName(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_CarrierName)));
					mILN.setIsOrigin(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTILN_IsOrigin)));

					data.add(mILN);
				}
			}
			closeCursor(mCursor);
		} catch (Exception e) {
			closeCursor(mCursor);
			e.printStackTrace();
		}

		return data;
	}

	private boolean deleteDataFromTable() {
		try {
			SQLiteDatabase sqlDB = db.getDataBase();
			long success = sqlDB.delete(PIECHARTILN_TABLE, null, null);
			if (success != -1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private void closeCursor(Cursor cursor) {
		try {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
				cursor = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
