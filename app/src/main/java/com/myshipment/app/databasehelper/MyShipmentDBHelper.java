package com.myshipment.app.databasehelper;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.myshipment.R;

public class MyShipmentDBHelper extends SQLiteOpenHelper {

	private static String DATABASENAME = "";
	private static int DATABASEVERSION = 0;
	private static String DB_PATH = "";
	private SQLiteDatabase db = null;

	public MyShipmentDBHelper(Context context) {
		super(context, context.getResources().getString(R.string.database_name), null, Integer.parseInt(context.getResources().getString(R.string.database_version)));
		this.DATABASENAME = getDatabaseName();
		this.DATABASEVERSION = Integer.parseInt(context.getResources().getString(R.string.database_version));
		this.DB_PATH = context.getDatabasePath(DATABASENAME).getPath();

		context.openOrCreateDatabase(DATABASENAME, SQLiteDatabase.OPEN_READWRITE, null);
	}

	public SQLiteDatabase getDataBase() {
		return db;
	}

	public void openDataBase() {
		try {
			db = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			//db.endTransaction();
		}
	}

	@Override
	public synchronized void close() {
		if (getDataBase() != null)
			getDataBase().close();

		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			if (db != null) {
				db.execSQL(sqlQRForCreateGraphOverView());
				db.execSQL(sqlQRForPieChartCarrierDetails());
				db.execSQL(sqlQRForPieChartCarrier());
				db.execSQL(sqlQRForPieChartContainers());
				db.execSQL(sqlQRForPieChartFlow());
				db.execSQL(sqlQRForPieChartILN());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			assert db != null;
			db.endTransaction();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	
		try {
			if (newVersion > oldVersion) {
				db.execSQL("DROP TABLE IF EXIST " + GraphOverViewTableHelper.GRAPHOVERVIEW_TABLE);
				db.execSQL("DROP TABLE IF EXIST " + PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_TABLE);
				db.execSQL("DROP TABLE IF EXIST " + PieChartCarrierTableHelper.PIECHARTCARRIER_TABLE);
				db.execSQL("DROP TABLE IF EXIST " + PieChartContainersTableHelper.PIECHARTCONTAINERS_TABLE );
				db.execSQL("DROP TABLE IF EXIST " + PieChartFlowTableHelper.PIECHARTFLOW_TABLE );
				db.execSQL("DROP TABLE IF EXIST " + PieChartILNTableHelper.PIECHARTILN_TABLE);
				onCreate(db);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}

	/**
	 * Table Create Query
	 */	
	
	private String sqlQRForCreateGraphOverView() {
		String sql = "CREATE TABLE " + GraphOverViewTableHelper.GRAPHOVERVIEW_TABLE 
				+ " (" + GraphOverViewTableHelper.GRAPHOVERVIEW_GraphID + " TEXT PRIMARY KEY  NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_FromDate + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_ToDate + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_PotentialDelay + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_TotalShipment + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_TotalReachedOnTime + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_ILNname + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_TotalIntransit + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_DelayRate + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_TotalDelayed + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_MaxContainerCount + " TEXT NOT NULL," 
				+ GraphOverViewTableHelper.GRAPHOVERVIEW_IsOrigin + " TEXT NOT NULL);";
		return sql;
	}
	
	private String sqlQRForPieChartContainers() {
		String sql = "CREATE TABLE " + PieChartContainersTableHelper.PIECHARTCONTAINERS_TABLE 
				+ " (" + PieChartContainersTableHelper.PIECHARTCONTAINERS_TotalPotentialDelay + " TEXT NOT NULL," 
				+ PieChartContainersTableHelper.PIECHARTCONTAINERS_TotalShipment + " TEXT NOT NULL," 
				+ PieChartContainersTableHelper.PIECHARTCONTAINERS_TotalIntransit + " TEXT NOT NULL," 
				+ PieChartContainersTableHelper.PIECHARTCONTAINERS_TotalDelayed + " TEXT NOT NULL," 
				+ PieChartContainersTableHelper.PIECHARTCONTAINERS_MaxContainerCount + " TEXT NOT NULL," 
				+ PieChartContainersTableHelper.PIECHARTCONTAINERS_FromDate + " TEXT NOT NULL," 
				+ PieChartContainersTableHelper.PIECHARTCONTAINERS_ToDate + " TEXT NOT NULL," 
				+ PieChartContainersTableHelper.PIECHARTCONTAINERS_IsOrigin + " TEXT NOT NULL);"; 
		return sql;
	}
	
	private String sqlQRForPieChartCarrier() {
		String sql = "CREATE TABLE " + PieChartCarrierTableHelper.PIECHARTCARRIER_TABLE 
				+ " (" + PieChartCarrierTableHelper.PIECHARTCARRIER_CarrierID + " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_CarrierName + " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_IndividualPercentage + " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_OverallPercentage + " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_RGB+ " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_NoOfShipment + " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_Type + " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_FromDate + " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_ToDate + " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_IsOrigin + " TEXT NOT NULL," 
				+ PieChartCarrierTableHelper.PIECHARTCARRIER_NoOfTypeShipment + " TEXT NOT NULL);";
		return sql;
	}
		
	private String sqlQRForPieChartILN() {
		String sql = "CREATE TABLE " + PieChartILNTableHelper.PIECHARTILN_TABLE 
				+ " (" + PieChartILNTableHelper.PIECHARTILN_IndividualPercentage + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_OverallPercentage + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_ILNName + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_NoOfTypeShipment + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_RGB + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_NoOfShipment + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_ILNID + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_Type + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_FromDate + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_ToDate + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_CarrierName + " TEXT NOT NULL," 
				+ PieChartILNTableHelper.PIECHARTILN_IsOrigin + " TEXT NOT NULL);"; 
		return sql;
	}
	
	private String sqlQRForPieChartFlow() {
		String sql = "CREATE TABLE " + PieChartFlowTableHelper.PIECHARTFLOW_TABLE 
				+ " (" + PieChartFlowTableHelper.PIECHARTFLOW_FlowID + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_FlowName + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_IndividualPercentage + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_OverallPercentage + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_NoOfTypeShipment + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_RGB + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_NoOfShipment + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_Type + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_FromDate + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_ToDate + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_CarrierName + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_ILNID + " TEXT NOT NULL," 
				+ PieChartFlowTableHelper.PIECHARTFLOW_IsOrigin + " TEXT NOT NULL);"; 
		return sql;
	}
	
	private String sqlQRForPieChartCarrierDetails() {
		String sql = "CREATE TABLE " + PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_TABLE 
				+ " (" + PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_Real + " TEXT NOT NULL," 
				+ PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_DelayDays + " TEXT NOT NULL," 
				+ PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_Initial + " TEXT NOT NULL," 
				+ PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_ContainerNumber + " TEXT NOT NULL," 
				+ PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_CarrierName + " TEXT NOT NULL," 
				+ PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_PolPod + " TEXT NOT NULL," 
				+ PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_Type + " TEXT NOT NULL," 
				+ PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_FromDate + " TEXT NOT NULL," 
				+ PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_ToDate + " TEXT NOT NULL," 
				+ PieChartCarrierDetailsTableHelper.PIECHARTCARRIERDETAILS_IsOrigin + " TEXT NOT NULL);";
		return sql;
	}
	

}
