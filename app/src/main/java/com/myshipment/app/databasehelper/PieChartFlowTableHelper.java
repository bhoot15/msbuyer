package com.myshipment.app.databasehelper;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.myshipment.app.MyShipmentApplication;
import com.myshipment.app.model.Flow;

public class PieChartFlowTableHelper {

	private Context mContext;
	private static PieChartFlowTableHelper instance = null;
	private MyShipmentDBHelper db = null;

	/**
	 * Table Column Name
	 */

	public final static String PIECHARTFLOW_TABLE = "PieChartFlow";

	public final static String PIECHARTFLOW_FlowID = "FlowID";
	public final static String PIECHARTFLOW_FlowName = "FlowName";
	public final static String PIECHARTFLOW_IndividualPercentage = "IndividualPercentage";
	public final static String PIECHARTFLOW_OverallPercentage = "OverallPercentage";
	public final static String PIECHARTFLOW_NoOfTypeShipment = "NoOfTypeShipment";
	public final static String PIECHARTFLOW_RGB = "RGB";
	public final static String PIECHARTFLOW_NoOfShipment = "NoOfShipment";
	public final static String PIECHARTFLOW_Type = "Type";
	public final static String PIECHARTFLOW_FromDate = "FromDate";
	public final static String PIECHARTFLOW_ToDate = "ToDate";
	public final static String PIECHARTFLOW_CarrierName = "CarrierName";
	public final static String PIECHARTFLOW_ILNID = "ILNID";
	public final static String PIECHARTFLOW_IsOrigin = "IsOrigin";

	public static synchronized PieChartFlowTableHelper getInstance(Context context) {
		if (instance == null) {
			instance = new PieChartFlowTableHelper(context);
		}
		return instance;
	}

	public PieChartFlowTableHelper(Context context) {
		this.mContext = context;
		this.db = ((MyShipmentApplication) context.getApplicationContext()).DB_HELPER;
	}

	public void storeGraphOVData(Flow mFlow) {
		SQLiteDatabase sqlDB = null;
		try {
			sqlDB = db.getDataBase();
			sqlDB.beginTransaction();
			String sql = "Insert or Replace into " + PIECHARTFLOW_TABLE + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
			SQLiteStatement insert = sqlDB.compileStatement(sql);
			if (mFlow != null) {
				insert.bindString(1, "" + mFlow.getFlowID());
				insert.bindString(2, mFlow.getFlow_Name());
				insert.bindString(3, mFlow.getIndividual_Percentage());
				insert.bindString(4, mFlow.getOverall_Percentage());
				insert.bindString(5, mFlow.getNo_Of__Shipment());
				insert.bindString(6, mFlow.getRGB());
				insert.bindString(7, mFlow.getNo_Of_Shipment());

				insert.bindString(8, mFlow.getType());
				insert.bindString(9, mFlow.getFromdate());
				insert.bindString(10, mFlow.getTodate());
				insert.bindString(11, mFlow.getCarriername());
				insert.bindString(12, mFlow.getILNCode());
				insert.bindString(13, mFlow.getIsOrigin());
				insert.execute();
			}
			sqlDB.setTransactionSuccessful();
		} catch (Exception ex) {
			Log.w("error", ex.getMessage());
		} finally {
			sqlDB.endTransaction();
		}
	}

	public ArrayList<Flow> getListOfCarrier() {

		ArrayList<Flow> data = null;
		Cursor mCursor = null;
		try {
			SQLiteDatabase sqlDB = db.getDataBase();
			String qr = "Select * from " + PIECHARTFLOW_TABLE;
			mCursor = sqlDB.rawQuery(qr, null);

			if (mCursor != null && mCursor.getCount() >= 1) {
				data = new ArrayList<Flow>();

				while (mCursor.moveToNext()) {
					Flow mFlow = new Flow();

					mFlow.setFlowID(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_FlowID)));
					mFlow.setFlow_Name(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_FlowName)));
					mFlow.setIndividual_Percentage(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_IndividualPercentage)));
					mFlow.setOverall_Percentage(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_OverallPercentage)));
					mFlow.setNo_Of__Shipment(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_NoOfTypeShipment)));
					mFlow.setRGB(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_RGB)));
					mFlow.setNo_Of_Shipment(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_NoOfShipment)));
					mFlow.setType(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_Type)));
					mFlow.setFromdate(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_FromDate)));
					mFlow.setTodate(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_ToDate)));
					mFlow.setCarriername(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_CarrierName)));
					mFlow.setILNCode(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_ILNID)));
					mFlow.setIsOrigin(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTFLOW_IsOrigin)));

					data.add(mFlow);
				}
			}
			closeCursor(mCursor);
		} catch (Exception e) {
			closeCursor(mCursor);
			e.printStackTrace();
		}

		return data;
	}

	private boolean deleteDataFromTable() {
		try {
			SQLiteDatabase sqlDB = db.getDataBase();
			long success = sqlDB.delete(PIECHARTFLOW_TABLE, null, null);
			if (success != -1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private void closeCursor(Cursor cursor) {
		try {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
				cursor = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
