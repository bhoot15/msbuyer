package com.myshipment.app.databasehelper;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.myshipment.app.MyShipmentApplication;
import com.myshipment.app.model.CarrierData;

public class PieChartCarrierTableHelper {

	private Context mContext;
	private static PieChartCarrierTableHelper instance = null;
	private MyShipmentDBHelper db = null;

	/**
	 * Table Column Name
	 */

	public final static String PIECHARTCARRIER_TABLE = "PieChartCarrier";

	public final static String PIECHARTCARRIER_CarrierID = "CarrierID";
	public final static String PIECHARTCARRIER_CarrierName = "CarrierName";
	public final static String PIECHARTCARRIER_IndividualPercentage = "IndividualPercentage";
	public final static String PIECHARTCARRIER_OverallPercentage = "OverallPercentage";
	public final static String PIECHARTCARRIER_RGB = "RGB";
	public final static String PIECHARTCARRIER_NoOfShipment = "NoOfShipment";
	public final static String PIECHARTCARRIER_Type = "Type";
	public final static String PIECHARTCARRIER_FromDate = "FromDate";
	public final static String PIECHARTCARRIER_ToDate = "ToDate";
	public final static String PIECHARTCARRIER_IsOrigin = "IsOrigin";
	public final static String PIECHARTCARRIER_NoOfTypeShipment = "NoOfTypeShipment";

	public static synchronized PieChartCarrierTableHelper getInstance(Context context) {
		if (instance == null) {
			instance = new PieChartCarrierTableHelper(context);
		}
		return instance;
	}

	public PieChartCarrierTableHelper(Context context) {
		this.mContext = context;
		this.db = ((MyShipmentApplication) context.getApplicationContext()).DB_HELPER;
	}

	public void storeGraphOVData(CarrierData mCarrierData) {
		SQLiteDatabase sqlDB = null;
		try {
			sqlDB = db.getDataBase();
			sqlDB.beginTransaction();
			String sql = "Insert or Replace into " + PIECHARTCARRIER_TABLE + " VALUES (?,?,?,?,?,?,?,?,?,?,?);";
			SQLiteStatement insert = sqlDB.compileStatement(sql);
			if (mCarrierData != null) {
				insert.bindString(1, "" + mCarrierData.getCarrierID());
				insert.bindString(2, mCarrierData.getCarrier_Name());
				insert.bindString(3, mCarrierData.getIndividual_Percentage());
				insert.bindString(4, mCarrierData.getOverall_Percentage());
				insert.bindString(5, mCarrierData.getRGB());
				insert.bindString(6, mCarrierData.getNo_Of_Shipment());
				insert.bindString(7, mCarrierData.getType());

				insert.bindString(8, mCarrierData.getFromDate());
				insert.bindString(9, mCarrierData.getToDate());
				insert.bindString(10, mCarrierData.getIsOrigin());
				insert.bindString(11, mCarrierData.getNo_Of_delay_Shipment());

				insert.execute();
			}
			sqlDB.setTransactionSuccessful();
		} catch (Exception ex) {
			Log.w("error", ex.getMessage());
		} finally {
			sqlDB.endTransaction();
		}
	}

	public ArrayList<CarrierData> getListOfCarrier() {

		ArrayList<CarrierData> data = null;
		Cursor mCursor = null;
		try {
			SQLiteDatabase sqlDB = db.getDataBase();
			String qr = "Select * from " + PIECHARTCARRIER_TABLE;
			mCursor = sqlDB.rawQuery(qr, null);

			if (mCursor != null && mCursor.getCount() >= 1) {
				data = new ArrayList<CarrierData>();

				while (mCursor.moveToNext()) {
					CarrierData mCarrierData = new CarrierData();
					mCarrierData.setCarrierID(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_CarrierID)));
					mCarrierData.setCarrier_Name(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_CarrierName)));
					mCarrierData.setIndividual_Percentage(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_IndividualPercentage)));
					mCarrierData.setOverall_Percentage(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_OverallPercentage)));
					mCarrierData.setRGB(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_RGB)));
					mCarrierData.setNo_Of_Shipment(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_NoOfShipment)));
					mCarrierData.setType(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_Type)));
					mCarrierData.setFromDate(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_FromDate)));
					mCarrierData.setToDate(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_ToDate)));
					mCarrierData.setIsOrigin(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_IsOrigin)));
					mCarrierData.setNo_Of_delay_Shipment(mCursor.getColumnName(mCursor.getColumnIndex(PIECHARTCARRIER_NoOfTypeShipment)));
					data.add(mCarrierData);
				}
			}
			closeCursor(mCursor);
		} catch (Exception e) {
			closeCursor(mCursor);
			e.printStackTrace();
		}

		return data;
	}

	private boolean deleteDataFromTable() {
		try {
			SQLiteDatabase sqlDB = db.getDataBase();
			long success = sqlDB.delete(PIECHARTCARRIER_TABLE, null, null);
			if (success != -1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private void closeCursor(Cursor cursor) {
		try {
			if (cursor != null && !cursor.isClosed()) {
				cursor.close();
				cursor = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
