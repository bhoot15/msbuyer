package com.myshipment.app.databasehelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.myshipment.app.MyShipmentApplication;
import com.myshipment.app.model.INLOverViewData;

public class PieChartContainersTableHelper {
	private Context mContext;
	private static PieChartContainersTableHelper instance = null;
	private MyShipmentDBHelper db = null;

	/**
	 * Table Column Name
	 */

	public final static String PIECHARTCONTAINERS_TABLE = "PieChartContainers";

	public final static String PIECHARTCONTAINERS_TotalPotentialDelay = "TotalPotentialDelay";
	public final static String PIECHARTCONTAINERS_TotalShipment = "TotalShipment";
	public final static String PIECHARTCONTAINERS_TotalIntransit = "TotalIntransit";
	public final static String PIECHARTCONTAINERS_TotalDelayed = "TotalDelayed";
	public final static String PIECHARTCONTAINERS_MaxContainerCount = "MaxContainerCount";
	public final static String PIECHARTCONTAINERS_FromDate = "FromDate";
	public final static String PIECHARTCONTAINERS_ToDate = "ToDate";
	public final static String PIECHARTCONTAINERS_IsOrigin = "IsOrigin";

	public static synchronized PieChartContainersTableHelper getInstance(Context context) {
		if (instance == null) {
			instance = new PieChartContainersTableHelper(context);
		}
		return instance;
	}

	public PieChartContainersTableHelper(Context context) {
		this.mContext = context;
		this.db = ((MyShipmentApplication) context.getApplicationContext()).DB_HELPER;
	}

	public void storeGraphOVData(INLOverViewData mINLOverViewData) {
		SQLiteDatabase sqlDB = null;
		try {
			sqlDB = db.getDataBase();
			sqlDB.beginTransaction();
			String sql = "Insert or Replace into " + PIECHARTCONTAINERS_TABLE + " VALUES (?,?,?,?,?,?,?,?);";
			SQLiteStatement insert = sqlDB.compileStatement(sql);
			if (mINLOverViewData != null) {
				insert.bindString(1, mINLOverViewData.getTotalpotentialdelay());
				insert.bindString(2, mINLOverViewData.getTotalshipment());
				insert.bindString(3, mINLOverViewData.getTotalintransit());
				insert.bindString(4, mINLOverViewData.getTotaldelayed());
				insert.bindString(5, mINLOverViewData.getMaxcontainercount());
				insert.bindString(6, mINLOverViewData.getFromDate());
				insert.bindString(7, mINLOverViewData.getToDate());
				insert.bindString(8, mINLOverViewData.getIsOrigin());
				insert.execute();
			}
			sqlDB.setTransactionSuccessful();
		} catch (Exception ex) {
			Log.w("error", ex.getMessage());
		} finally {
			sqlDB.endTransaction();
		}
	}
}
