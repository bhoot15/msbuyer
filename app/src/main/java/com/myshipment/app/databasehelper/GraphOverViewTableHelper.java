package com.myshipment.app.databasehelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.myshipment.app.MyShipmentApplication;
import com.myshipment.app.model.INLData;

public class GraphOverViewTableHelper {

	private Context mContext;
	private static GraphOverViewTableHelper instance = null;
	private MyShipmentDBHelper db = null;

	/**
	 * Table Column Name
	 */

	public final static String GRAPHOVERVIEW_TABLE = "GraphOverView";

	public final static String GRAPHOVERVIEW_GraphID = "GraphID";
	public final static String GRAPHOVERVIEW_FromDate = "FromDate";
	public final static String GRAPHOVERVIEW_ToDate = "ToDate";
	public final static String GRAPHOVERVIEW_PotentialDelay = "PotentialDelay";
	public final static String GRAPHOVERVIEW_TotalShipment = "TotalShipment";
	public final static String GRAPHOVERVIEW_TotalReachedOnTime = "TotalReachedOnTime";
	public final static String GRAPHOVERVIEW_ILNname = "ILNname";
	public final static String GRAPHOVERVIEW_TotalIntransit = "TotalIntransit";
	public final static String GRAPHOVERVIEW_DelayRate = "DelayRate";
	public final static String GRAPHOVERVIEW_TotalDelayed = "TotalDelayed";
	public final static String GRAPHOVERVIEW_MaxContainerCount = "MaxContainerCount";
	public final static String GRAPHOVERVIEW_IsOrigin = "IsOrigin";

	public static synchronized GraphOverViewTableHelper getInstance(Context context) {
		if (instance == null) {
			instance = new GraphOverViewTableHelper(context);
		}
		return instance;
	}

	public GraphOverViewTableHelper(Context context) {
		this.mContext = context;
		this.db = ((MyShipmentApplication) context.getApplicationContext()).DB_HELPER;
	}

	public void storeGraphOVData(INLData mINLData) {
		SQLiteDatabase sqlDB = null;
		try {
			sqlDB = db.getDataBase();
			sqlDB.beginTransaction();
			String sql = "Insert or Replace into " + GRAPHOVERVIEW_TABLE + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
			SQLiteStatement insert = sqlDB.compileStatement(sql);
			if (mINLData != null) {
				insert.bindString(1, mINLData.getId());
				insert.bindString(2, mINLData.getFromDate());
				insert.bindString(3, mINLData.getToDate());
				insert.bindString(4, "0");
				insert.bindString(5, mINLData.getTotalShipment());
				insert.bindString(6, mINLData.getTotalReachedOnTime());
				insert.bindString(7, mINLData.getILNname());
				insert.bindString(8, mINLData.getTotalIntransit());
				insert.bindString(9, mINLData.getDelayRate());
				insert.bindString(10, mINLData.getTotalDelayed());
				insert.bindString(11, mINLData.getMaxcontainercount());
				insert.bindString(12, mINLData.getIsOrigin());

				insert.execute();
			}
			sqlDB.setTransactionSuccessful();
		} catch (Exception ex) {
			Log.w("error", ex.getMessage());
		} finally {
			sqlDB.endTransaction();
		}
	}

}
