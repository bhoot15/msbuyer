package com.myshipment.app.Utility;

/**
 * Created by Akshay Thapliyal on 16-08-2016.
 */
public class AppConstants {

    public static final String TOTAL_SHIPMENT = "totalShipment";
    public static final String OPEN_ORDER = "openOrder";
    public static final String GOODS_RECEIVED = "goodsReceived";
    public static final String STUFFING_DONE = "stuffingDone";
    public static final String IN_TRANSIT = "inTransit";
    public static final String DELIVERED = "delivered";
    public static final String TOTAL_CBM = "totalCbm";
    public static final String TOTAL_GWT = "totalGwt";
}
