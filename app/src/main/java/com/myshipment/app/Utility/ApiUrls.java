package com.myshipment.app.Utility;

/**
 * Created by Akshay Thapliyal on 24-06-2016.
 */
public class ApiUrls {
    //public static String MAIN_URL = "http://202.65.11.27:8080/MyShipmentBackend/";
    //public static String MAIN_URL = "http://121.200.242.141:8080/MyShipmentBackend/"; //production
    //public static String MAIN_URL = "http://121.200.242.147:8080/MyShipmentBackend/"; //Development
      public static String MAIN_URL = "http://apps.myshipment.com/";
    //public static String MAIN_URL = "http://192.168.0.119:8081/MyShipmentBackend/";
    //public static String MAIN_URL = "http://121.200.242.147:8080/MyShipmentBackend/";

    public static String LOGIN_URL = MAIN_URL + "getLoginDetail";
    public static String DASHBOARD_URL = MAIN_URL +"sowiseshipsummarybuyer";
    public static String DASHBOARD_INFO_URL = MAIN_URL +"dashboardInfo";
    public static String LAST_SHIPMENTS_URL = MAIN_URL + "getLastNShipmentsDetail";
    public static String TRACKING_URL = MAIN_URL +"getMyshipmentTrackDetails";
    public static String DASHBOARD_TRACKING_URL = MAIN_URL +"getMyshipmentTrackDetailsDashboard";
    public static String TOP_5_SHIPMENTS = MAIN_URL + "top5shipmentbuyer";
    public static String PIE_CHART_DETAILS_URL = MAIN_URL + "suppwiseshipdetailsbyblstatus";
    public static String BAR_CHART_DETAILS_URL = MAIN_URL + "suppwiseshipdetailsbuyer"; //{"salesOrg":"HN2","dashboardShipperParams":[{"shipperNo":"1000001078","division":"SE","distChannel":"EX","bookingDateFrom":"20110210","bookingDateTo":"20160802"}]
    public static String TOTAL_SHIPMENTS_URL = MAIN_URL + "suppwiseshipdetails";
    public static String TOTAL_CBM_URL = MAIN_URL + "suppwisecbmdetails";
    public static String TOTAL_GWT_URL = MAIN_URL + "suppwisegwtdetails";
    public static String SHIPMENT_DETAILS = MAIN_URL + "suppwiseshipdetailsbypoo";
    public static String COMM_INVOICE_URL = MAIN_URL + "commInvTrackingInfo"; //"http://121.200.242.147:8080/MyShipmentBackend/commInvTrackingInfo"
    public static String PO_DETAIL_URL = MAIN_URL + "poTrackingInfo";//"http://121.200.242.147:8080/MyShipmentBackend/poTrackingInfo"
}
