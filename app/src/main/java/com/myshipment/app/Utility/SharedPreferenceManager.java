package com.myshipment.app.Utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by admin on 29-02-2016.
 */
public class SharedPreferenceManager {

    private static final String MY_PREFS_NAME = "info";
    private static final String SHARED_PREF_NAME = "FCMSharedPref";
    private static final String TAG_TOKEN = "tagtoken";

    public static void saveUserCode(String userCode, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("userCode", userCode);
        editor.apply();
    }

    public static void saveAccGroup(String accGroup, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("accGroup", accGroup);
        editor.apply();
    }

    public static String getAccGroup(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("accGroup", "");
    }

    public static void saveUserName(String userName, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("userName", userName);
        editor.apply();
    }

    public static String getUserName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("userName", "");
    }

    /*public static void saveTotalShipment(String totalShipment, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("totalShipment", totalShipment);
        editor.apply();
    }

    public static String getTotalShipment(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("totalShipment", "");
    }

    public static void saveOpenBookings(String openBookings, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("openBookings", openBookings);
        editor.apply();
    }

    public static String getOpenBookings(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("openBookings", "");
    }

    public static void saveGoodsHandover(String goodsHandover, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("goodsHandover", goodsHandover);
        editor.apply();
    }

    public static String getGoodsHandover(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("goodsHandover", "");
    }

    public static void saveStuffingDone(String stuffingDone, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("stuffingDone", stuffingDone);
        editor.apply();
    }

    public static String getStuffingDone(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("stuffingDone", "");
    }

    public static void saveInTransit(String inTransit, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("inTransit", inTransit);
        editor.apply();
    }

    public static String getInTransit(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("inTransit", "");
    }

    public static void saveArrived(String arrived, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("arrived", arrived);
        editor.apply();
    }

    public static String getArrived(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("arrived", "");
    }*/

    public static void savePol(String pol, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("pol", pol);
        editor.apply();
    }

    public static String getPol(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("pol", "");
    }

    public static void savePod(String pod, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("pod", pod);
        editor.apply();
    }

    public static String getPod(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("pod", "");
    }

    public static void saveEta(String eta, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("eta", eta);
        editor.apply();
    }

    public static String getEta(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("eta", "");
    }

    public static void saveEtd(String etd, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("etd", etd);
        editor.apply();
    }

    public static String getEtd(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("etd", "");
    }

    public static void saveHeaderListString(String myShipmentTrackHeaderList, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("myShipmentTrackHeaderList", myShipmentTrackHeaderList);
        editor.apply();
    }

    public static String getHeaderListString(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("myShipmentTrackHeaderList", "");
    }

    public static void saveLoginRespone(String saveLoginRespone, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("loginResponse", saveLoginRespone);
        editor.apply();
    }

    public static String getLoginRespone(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("loginResponse", "");
    }

    public static void saveTrackRespone(String saveTrackRespone, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("trackRespone", saveTrackRespone);
        editor.apply();
    }

    public static String getTrackRespone(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("trackRespone", "");
    }

    public static String getUserCode(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("userCode", "");
    }

    public static void setUserLoggedIn(boolean response, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putBoolean("userlogin", response);
        editor.apply();
    }

    public static boolean isUserLoggedIn(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getBoolean("userlogin", false);
    }

    public static void setToDate(String response, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("toDate", response);
        editor.apply();
    }

    public static String getToDate(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("toDate", "");
    }

    public static void setFromDate(String response, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("fromDate", response);
        editor.apply();
    }

    public static String getFromDate(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("fromDate", "");
    }

    public static void saveSalesOrg(String response, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("salesOrg", response);
        editor.apply();
    }

    public static String getSalesOrg(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("salesOrg", "");
    }


    public static void saveSalesOrgCode(String response, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("salesOrgCode", response);
        editor.apply();
    }

    public static String getSalesOrgCode(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("salesOrgCode", "");
    }

    public static void saveDistChannel(String response, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("distChannel", response);
        editor.apply();
    }

    public static String getDistChannel(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("distChannel", "");
    }

    public static void saveDivision(String response, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
        editor.putString("division", response);
        editor.apply();
    }

    public static String getDivision(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
        return prefs.getString("division", "");
    }

    public static void clearCredentials(Context context) {
        SharedPreferences preferences = (SharedPreferences) context.getSharedPreferences(MY_PREFS_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    //this method will save the device token to shared preferences
    public static void saveDeviceToken(String token, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TAG_TOKEN, token);
        editor.apply();
    }

    //this method will fetch the device token from shared preferences
    public String getDeviceToken(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return  sharedPreferences.getString(TAG_TOKEN, null);
    }

}
