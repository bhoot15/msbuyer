package com.myshipment.app.beans;

/**
 * Created by Zobair Alam on 15-10-2017.
 */
public class ItemData {
    private String po_no;
    private String commodity;
    private String style;
    private String size;
    private String sku;
    private String art_no;
    private String color;
    private String item_pcs;
    private String cart_sno;
    private String item_vol;

    public String getPo_no() {
        return po_no;
    }

    public void setPo_no(String po_no) {
        this.po_no = po_no;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getArt_no() {
        return art_no;
    }

    public void setArt_no(String art_no) {
        this.art_no = art_no;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getItem_pcs() {
        return item_pcs;
    }

    public void setItem_pcs(String item_pcs) {
        this.item_pcs = item_pcs;
    }

    public String getCart_sno() {
        return cart_sno;
    }

    public void setCart_sno(String cart_sno) {
        this.cart_sno = cart_sno;
    }

    public String getItem_vol() {
        return item_vol;
    }

    public void setItem_vol(String item_vol) {
        this.item_vol = item_vol;
    }

    public String getItem_grwt() {
        return item_grwt;
    }

    public void setItem_grwt(String item_grwt) {
        this.item_grwt = item_grwt;
    }

    private String item_grwt;


}
