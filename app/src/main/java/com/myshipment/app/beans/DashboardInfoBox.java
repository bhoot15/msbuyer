/**
*
*@author Ahmad Naquib
*/
package com.myshipment.app.beans;

import java.util.List;

public class DashboardInfoBox {

	private String status;
	
	private double totalGWT;
	private double totalCBM;
	private double totalCRGWT;
	private Integer totalShipment;
	private Integer inTransit;
	private Integer openOrder;
	private Integer goodsReceived;
	private Integer stuffingDone;
	private Integer delivered;
	private List<TrackingDataNew> totalShipmentList;
	private List<TrackingDataNew> inTransitList;
	private List<TrackingDataNew> openOrderList;
	private List<TrackingDataNew> goodsReceivedList;
	private List<TrackingDataNew> stuffingDoneList;
	private List<TrackingDataNew> deliveredList;
	
	public Integer getGoodsReceived() {
		return goodsReceived;
	}
	public void setGoodsReceived(Integer goodsReceived) {
		this.goodsReceived = goodsReceived;
	}
	public Integer getStuffingDone() {
		return stuffingDone;
	}
	public void setStuffingDone(Integer stuffingDone) {
		this.stuffingDone = stuffingDone;
	}
	public Integer getDelivered() {
		return delivered;
	}
	public void setDelivered(Integer delivered) {
		this.delivered = delivered;
	}
	public List<TrackingDataNew> getGoodsReceivedList() {
		return goodsReceivedList;
	}
	public void setGoodsReceivedList(List<TrackingDataNew> goodsReceivedList) {
		this.goodsReceivedList = goodsReceivedList;
	}
	public List<TrackingDataNew> getStuffingDoneList() {
		return stuffingDoneList;
	}
	public void setStuffingDoneList(List<TrackingDataNew> stuffingDoneList) {
		this.stuffingDoneList = stuffingDoneList;
	}
	public List<TrackingDataNew> getDeliveredList() {
		return deliveredList;
	}
	public void setDeliveredList(List<TrackingDataNew> deliveredList) {
		this.deliveredList = deliveredList;
	}
	public double getTotalGWT() {
		return totalGWT;
	}
	public void setTotalGWT(double totalGWT) {
		this.totalGWT = totalGWT;
	}
	public double getTotalCBM() {
		return totalCBM;
	}
	public void setTotalCBM(double totalCBM) {
		this.totalCBM = totalCBM;
	}
	public double getTotalCRGWT() {
		return totalCRGWT;
	}
	public void setTotalCRGWT(double totalCRGWT) {
		this.totalCRGWT = totalCRGWT;
	}
	public Integer getTotalShipment() {
		return totalShipment;
	}
	public void setTotalShipment(Integer totalShipment) {
		this.totalShipment = totalShipment;
	}
	public List<TrackingDataNew> getInTransitList() {
		return inTransitList;
	}
	public void setInTransitList(List<TrackingDataNew> inTransitList) {
		this.inTransitList = inTransitList;
	}
	public Integer getInTransit() {
		return inTransit;
	}
	public void setInTransit(Integer inTransit) {
		this.inTransit = inTransit;
	}
	public Integer getOpenOrder() {
		return openOrder;
	}
	public void setOpenOrder(Integer openOrder) {
		this.openOrder = openOrder;
	}
	public List<TrackingDataNew> getOpenOrderList() {
		return openOrderList;
	}
	public void setOpenOrderList(List<TrackingDataNew> openOrderList) {
		this.openOrderList = openOrderList;
	}
	public List<TrackingDataNew> getTotalShipmentList() {
		return totalShipmentList;
	}
	public void setTotalShipmentList(List<TrackingDataNew> totalShipmentList) {
		this.totalShipmentList = totalShipmentList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
