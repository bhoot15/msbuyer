package com.myshipment.app.beans;

import java.util.List;

/**
 * Created by shoum on 18/10/2017.
 */

public class DashboardTrackListData {

    List<TrackingDataNew> myshipmentTrackHeader;

    public List<TrackingDataNew> getMyshipmentTrackHeader() {
        return myshipmentTrackHeader;
    }

    public void setMyshipmentTrackHeader(List<TrackingDataNew> myshipmentTrackHeader) {
        this.myshipmentTrackHeader = myshipmentTrackHeader;
    }

}
