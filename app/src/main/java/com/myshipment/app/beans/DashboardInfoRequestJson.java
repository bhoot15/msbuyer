/**
*
*@author Ahmad Naquib
*/
package com.myshipment.app.beans;

import java.util.List;

public class DashboardInfoRequestJson {

	private String loggedInUserName;
	private String salesOrgSelected;
	private String divisionSelected;
	private String disChnlSelected;
	private String accgroup;
	private List<TrackingDataNew> myshipmentTrackHeader;
	
	public String getLoggedInUserName() {
		return loggedInUserName;
	}
	public void setLoggedInUserName(String loggedInUserName) {
		this.loggedInUserName = loggedInUserName;
	}
	public String getSalesOrgSelected() {
		return salesOrgSelected;
	}
	public void setSalesOrgSelected(String salesOrgSelected) {
		this.salesOrgSelected = salesOrgSelected;
	}
	public String getDivisionSelected() {
		return divisionSelected;
	}
	public void setDivisionSelected(String divisionSelected) {
		this.divisionSelected = divisionSelected;
	}
	public String getDisChnlSelected() {
		return disChnlSelected;
	}
	public void setDisChnlSelected(String disChnlSelected) {
		this.disChnlSelected = disChnlSelected;
	}
	public String getAccgroup() {
		return accgroup;
	}
	public void setAccgroup(String accgroup) {
		this.accgroup = accgroup;
	}
	public List<TrackingDataNew> getMyshipmentTrackHeader() {
		return myshipmentTrackHeader;
	}
	public void setMyshipmentTrackHeader(List<TrackingDataNew> myshipmentTrackHeader) {
		this.myshipmentTrackHeader = myshipmentTrackHeader;
	}
	
}
