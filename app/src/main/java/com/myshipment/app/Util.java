package com.myshipment.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myshipment.R;

public class Util {

	private final static String HTTP_CONTENT_TYPE = "text/xml";
	private final static String HTTP_HEADER_CONTENT_TYPE = HTTP_CONTENT_TYPE + "; charset=utf-8";
	public static int splashTime = 1300;

	public static int connectTimeout = 1000 * 120;
	public static int socketTimeout = 1000 * 120;

	// public static int connectTimeout = 1000 * 10;
	// public static int socketTimeout = 1000 * 10;

	public static enum DateFor {
		DefaultValue, StartPriod, StartPriodTo
	}

	public static enum KPIDatePickerFor {
		Service, monthly, comparison
	}

	public static enum ShipmentClickState {
		Shipments, Delayed, InTransit, PotentialDelay
	}

	/**
	 * User Credential
	 *
	 * --> Mandiola/luis (ILN) --> Pozuelo/cesar OR renault/123456 (admin)-->
	 * --> Ahn/dongho (Plant)
	 * 
	 **/

	/* INT Live server */
	// private static String baseUrl = "http://10.0.2.93:8080/myshipment/";

	/* INT Live server */
	// private static String baseUrl = " http://52.91.119.237:8080/myshipment/";

	/* Client Server */
	// private static String baseUrl =
	// "http://121.200.242.142:8080/myshipment/";
	// private static String baseUrl =
	// "http://121.200.242.148/renaultws/myshipment/";
	// private static String baseUrl = "http://121.200.242.148/renaultws/";
	private static String baseUrl = "http://rws.myshipment.com/";
	private static String AppKey = "20myshipment14";

	public static String getLoginUrl(String Username, String password) {
		return baseUrl + "Login?api_key=" + AppKey + "&username=" + Username + "&password=" + password;
	}

	public static String getTotalShipmentUrl() {
		return baseUrl + "TotalShipment";
	}

	public static String[] getMonths() {
		String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
		return months;
	}

	public static String[] TotalShipment = { "TotalShipment", "TotalShipmentIln", "TotalShipmentFlow", "TotalShipmentCarrierDetails" };
	public static String[] DelayShipment = { "DelayShipment", "DelayShipmentIln", "DelayShipmentFlow", "DelayShipmentCarrierDetails" };
	public static String[] InTransitShipment = { "InTransitShipment", "InTransitShipmentIln", "InTransitShipmentFlow", "InTransitShipmentCarrierDetails" };
	public static String[] PotentialDelay = { "PotentialDelay", "PotentialDelayIln", "PotentialDelayFlow", "PotentialDelayCarrierDetails" };

	/**
	 * Over View Data.
	 */

	public static String getBaseUrl() {
		return baseUrl;
	}

	public static String getForgotpasswUrl_(String MethodeKey, String username) {
		return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&userid=" + username;
	}

	public static String getGraphOverviewUrl_(String MethodeKey, String userid, String UserType, String Fromdate, String ToDate, String IsOrigin, String UserSubtype) {
		return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&userid=" + userid + "&UserType=" + UserType + "&Fromdate=" + Util.getEncodeData(Fromdate) + "&ToDate=" + Util.getEncodeData(ToDate) + "&IsOrigin=" + IsOrigin + "&UserSubtype=" + UserSubtype;
	}

	/**
	 * Shipment
	 */
	public static String getShipmentUrl(String MethodeKey, String userid, String UserType, String Fromdate, String ToDate, String IsOrigin, String UserSubtype) {
		return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&userid=" + userid + "&UserType=" + UserType + "&Fromdate=" + Util.getEncodeData(Fromdate) + "&ToDate=" + Util.getEncodeData(ToDate) + "&IsOrigin=" + IsOrigin + "&UserSubtype=" + UserSubtype;
	}

	/**
	 * Shipment ILN
	 */
	public static String getShipmentILNUrl(String MethodeKey, String userid, String UserType, String Fromdate, String ToDate, String IsOrigin, String carrierName, String UserSubtype) {
		return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&userid=" + userid + "&UserType=" + UserType + "&Fromdate=" + Util.getEncodeData(Fromdate) + "&ToDate=" + Util.getEncodeData(ToDate) + "&IsOrigin=" + IsOrigin + "&carrierName=" + carrierName + "&UserSubtype=" + UserSubtype;
	}

	/**
	 * Shipment Flow
	 */
	public static String getShipmentFlowUrl(String MethodeKey, String userid, String UserType, String Fromdate, String ToDate, String IsOrigin, String carrierName, String ilnCode, String UserSubtype) {
		return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&userid=" + userid + "&UserType=" + UserType + "&Fromdate=" + Util.getEncodeData(Fromdate) + "&ToDate=" + Util.getEncodeData(ToDate) + "&IsOrigin=" + IsOrigin + "&carrierName=" + carrierName + "&ilnCode=" + ilnCode + "&UserSubtype="
				+ UserSubtype;
	}

	/**
	 * Shipment Carrier Details
	 */
	public static String getShipmentCarrierDetailsUrl(String MethodeKey, String userid, String UserType, String Fromdate, String ToDate, String IsOrigin, String carrierName, String ilnCode, String podCode, String polCode, String UserSubtype) {
		return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&userid=" + userid + "&UserType=" + UserType + "&Fromdate=" + Util.getEncodeData(Fromdate) + "&ToDate=" + Util.getEncodeData(ToDate) + "&IsOrigin=" + IsOrigin + "&carrierName=" + carrierName + "&ilnCode=" + ilnCode + "&podCode="
				+ podCode + "&polCode=" + polCode + "&UserSubtype=" + UserSubtype;
	}

	public static String getIncidentReportDropDownUrl(String MethodeKey, String userid, String UserType, String UserSubtype) {
		return baseUrl + MethodeKey + "?&APIKey=" + AppKey + "&UserID=" + userid + "&UserType=" + UserType + "&UserSubType=" + UserSubtype;
	}

	public static String getRequestForDataDropDownUrl(String MethodeKey, String userid, String UserType, String UserSubtype) {
		return baseUrl + MethodeKey + "?api_key=" + AppKey + "&userid=" + userid + "&UserType=" + UserType + "&UserSubtype=" + UserSubtype;
	}

	public static String getRequestForDataSubmissionURL(String MethodeKey, String userid, String UserType, String UserSubtype, String requestType, String fromDate, String toDate) {
		return baseUrl + MethodeKey + "?api_key=" + AppKey + "&userid=" + userid + "&UserType=" + UserType + "&UserSubtype=" + UserSubtype + "&requestType=" + requestType + "&fromDate=" + fromDate + "&toDate=" + toDate;
	}

	public static String getIncidentReportUrl(String MethodeKey, String userid, String UserType, String UserSubtype, String IncidentType, String IncidentLevel, String RiskType, String FromDate, String ToDate) {
		return baseUrl + MethodeKey + "?&APIKey=" + AppKey + "&UserID=" + userid + "&UserType=" + UserType + "&UserSubType=" + UserSubtype + "&IncidentType=" + IncidentType + "&IncidentLevel=" + IncidentLevel + "&RiskType=" + RiskType + "&FromDate=" + FromDate + "&ToDate=" + ToDate;
	}

	public static String getIncidentReportUrl(String MethodeKey, String userid, String keyword) {
		return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&userid=" + userid + "&keyword=" + keyword;
	}

	/**
	 * Service KPI Report.
	 */
	public static String getServiceKPIReportUrl(String MethodeKey, String userid, String UserType, String UserSubtype, String month, String Year, String shipingILN, String POL) {
		return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&userid=" + userid + "&UserType=" + UserType + "&UserSubtype=" + UserSubtype + "&ShippingLineName=" + shipingILN + "&month=" + month + "&year=" + Year + "&polName=" + POL;
	}

	/**
	 * Search & Track --> SearchAPI.
	 */
	// http://10.0.2.70:8080/myshipment/RegularSearch?APIKey=20myshipment14&%20&UserID=renault&UserType=L&%20UserSubType=H&ContainerNo=SUDU8727320&BookingNo=&FromDate=2014-07-19&ToDate=2014-10-19
	public static String getRegulerUrl_(String MethodeKey, String ContainerNo, String BookingNo, String Fromdate, String ToDate, String userid, String UserType, String UserSubtype) {
		return baseUrl + MethodeKey + "?&APIKey=" + AppKey + "&ContainerNo=" + ContainerNo + "&BookingNo=" + BookingNo + "&UserID=" + userid + "&UserSubType=" + UserSubtype + "&UserType=" + UserType + "&FromDate=" + Fromdate + "&ToDate=" + ToDate;
	}

	public static String getRefineSearchUrl_(String MethodeKey, String userid, String UserType, String UserSubtype, String text_from, String text_to, String ContainerNo, String BookingNo, String spLine, String pol, String pod, String iln, String Fromdate, String ToDate) {
		return baseUrl + MethodeKey + "?APIKey=" + AppKey + "&UserId=" + userid + "&UserType=" + UserType + "&UserSubType=" + UserSubtype + "&BookingNo=" + BookingNo + "&ContainerNo=" + ContainerNo + "&ShippingLine=" + spLine + "&POL=" + pol + "&POD=" + pod + "&ILN=" + iln + "&FromDate=" + Fromdate
				+ "&ToDate=" + ToDate;
	}

	/**
	 * Search & Track --> TrackAPI.
	 */
	// public static String getTrackShipmentUrl(String MethodeKey, String
	// ContainerNumber, String userid, String UserSubtype, String UserType,
	// String MBLNo, String BookingNo) {
	// return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&ContainerNumber="
	// + ContainerNumber + "&userid=" + userid + "&UserSubtype=" + UserSubtype +
	// "&UserType=" + UserType + "&MBLNo=" + MBLNo + "&BookingNo=" + BookingNo;
	// }

	public static String getRefineSearchInputDataUrl(String MethodeKey, String userid, String UserSubtype, String UserType) {
		return baseUrl + MethodeKey + "?APIKey=" + AppKey + "&UserId=" + userid + "&UserSubType=" + UserSubtype + "&UsetType=" + UserType;
	}

	public static String getTrackShipmentUrl(String MethodeKey, String referenceNo, String userid, String UserSubtype, String UserType, String searchBy) {
		return baseUrl + MethodeKey + "?&api_key=" + AppKey + "&referenceNo=" + referenceNo + "&userid=" + userid + "&UserSubtype=" + UserSubtype + "&UserType=" + UserType + "&searchBy=" + searchBy;
	}

	/**
	 * KpiShippingLinePolListURL.
	 */
	public static String getKpiShippingLinePolListURL(String MethodeKey, String userid, String UserType, String UserSubtype) {
		return baseUrl + MethodeKey + "?APIKey=" + AppKey + "&UserId=" + userid + "&UserType=" + UserType + "&UserSubType=" + UserSubtype;
	}

	/**
	 * KPIILNPlantFlowListURL.
	 */
	public static String getKPIILNPlantFlowListURL(String MethodeKey, String userid, String UserType, String UserSubtype) {
		return baseUrl + MethodeKey + "?APIKey=" + AppKey + "&UserId=" + userid + "&UserType=" + UserType + "&UserSubType=" + UserSubtype;
	}

	/** Ocean Scheduling **/
	public static String getVesselListURL(String MethodeKey, String userid, String UserType, String UserSubtype, String searchText) {
		return baseUrl + MethodeKey + "?APIkey=" + AppKey + "&UserId=" + userid + "&UserType=" + UserType + "&UserSubType=" + UserSubtype + "&Text=" + searchText;
	}

	public static String getOrigAndDesListURL(String MethodeKey, String searchText) {
		return baseUrl + MethodeKey + "?APIKey=" + AppKey + "&Text=" + searchText;
	}

	public static String getOceanSchedulingSearchByLocationURl(String MethodeKey, String userid, String UserType, String UserSubtype, String Origin, String Destination, String DepertOrArivvedBy, String date, String week) {
		return baseUrl + MethodeKey + "?api_key=" + AppKey + "&UserId=" + userid + "&UserType=" + UserType + "&UserSubType=" + UserSubtype + "&origin=" + Origin + "&destination=" + Destination + "&DepertOrArivvedBy=" + DepertOrArivvedBy + "&date=" + date + "&week=" + week;
	}

	public static String getOceanSchedulingSearchByVesselURl(String MethodeKey, String userid, String UserType, String UserSubtype, String VesselName, String DepertOrArivvedBy, String date, String week) {
		return baseUrl + MethodeKey + "?api_key=" + AppKey + "&UserId=" + userid + "&UserType=" + UserType + "&UserSubType=" + UserSubtype + "&VesselName=" + VesselName + "&DepertOrArivvedBy=" + DepertOrArivvedBy + "&date=" + date + "&week=" + week;
	}

	// public static String getOceanSchedulingSearchByVesselURl(String
	// MethodeKey, String userid, String UserType, String UserSubtype, String
	// VesselName, String DepertOrArivvedBy, String date,
	// String week) {
	// return baseUrl + MethodeKey + "?api_key=" + AppKey + "&UserId=" + userid
	// + "&UserType=" + UserType + "&UserSubType=" + UserSubtype +
	// "&DepertOrArivvedBy=" + DepertOrArivvedBy + "&date="
	// + date + "&week=" + week;
	// }

	/**
	 * Dash board Four API
	 */

	public static String getDashboardPiechartURL(String MethodeKey, String userid, String UserType, String Fromdate, String ToDate, String IsOrigin, String UserSubtype) {
		return baseUrl + MethodeKey + "?api_key=" + AppKey + "&UserId=" + userid + "&UserType=" + UserType + "&Fromdate=" + Fromdate + "&ToDate=" + ToDate + "&IsOrigin=" + IsOrigin + "&UserSubtype=" + UserSubtype;
	}

	/**
	 * Hide Key Board.
	 */

	public static void HideKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public static SharedPreferences getPrefs(Context context) {
		return context.getSharedPreferences("MyShipment", Activity.MODE_PRIVATE);
	}

	/**
	 * Internetn Checking.
	 * 
	 * @param context
	 * @return
	 */
	public static boolean hasConnection(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		}
		NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobileNetwork != null && mobileNetwork.isConnected()) {
			return true;
		}
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true;
		}
		return false;
	}

	public static String getJSONKeyvalue(JSONObject jsObj, String key) {
		String value = "";
		try {
			if (jsObj.has(key)) {
				value = jsObj.getString(key);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}

	public static String readXMLinString(String fileName, Context c) {
		try {
			InputStream is = c.getAssets().open(fileName);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			String text = new String(buffer);

			return text;

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static int[] SpliteRGBData(String RGB) {
		String[] spldata = RGB.split(",");
		int[] rgb = new int[spldata.length];
		for (int i = 0; i < rgb.length; i++) {
			rgb[i] = Integer.parseInt(spldata[i]);
		}
		return rgb;

	}

	public static void ShowToast(Context mContext, String msg) {
		Toast mToast = Toast.makeText(mContext, msg, 500);
		mToast.setGravity(Gravity.CENTER, 0, 0);
		mToast.show();
	}

	/* For Incident Report section */
	public static String getSubUserType(Context context) {
		SharedPreferences mPrefs = Util.getPrefs(context);
		String SubUserType = mPrefs.getString("SubUserType", "");
		return SubUserType;
	}

	public static void saveSubUserType(Context context, String SubUserType) {
		SharedPreferences.Editor prefsEditor = Util.getPrefs(context).edit();
		prefsEditor.putString("SubUserType", SubUserType);
		prefsEditor.commit();
	}

	// HTTP GET request
	public static String[] sendGet(String url) throws Exception {
		String Response = "";

		String[] responData = { "", "" };

		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setConnectTimeout(connectTimeout); // set timeout to 5 seconds
			con.setReadTimeout(socketTimeout);

			// optional default is GET
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			// print result
			Response = response.toString();
			System.out.println(response.toString());
			responData[0] = "200";
			responData[1] = Response;
		} catch (java.net.SocketTimeoutException e) {
			e.printStackTrace();
			responData[0] = "205";
		} catch (Exception e) {
			e.printStackTrace();
			responData[0] = "205";
		}

		return responData;
	}

	public static String getJsonValue(JSONObject jsObject, String Key) {
		String value = "";
		try {
			if (jsObject.has(Key)) {
				value = jsObject.getString(Key);
				if (value.contains("null")) {
					value = value.replace("null", "");
				}

			} else {
				value = "";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}

	public static boolean isValid(String startDate, String toDate) {
		SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");
		long diffInDays = 0;
		Date stD = null;
		Date enD = null;
		try {
			stD = dfDate.parse(startDate);
			enD = dfDate.parse(toDate);
			diffInDays = (enD.getTime() - stD.getTime());
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return (diffInDays < 0) ? true : false;
	}

	public static enum DateSate {
		CURRENT, MONTHBF
	}

	public static String getThreeMonthBackDate(DateSate mSate) {
		Date referenceDate = new Date();
		Calendar c = Calendar.getInstance();
		if (mSate == DateSate.MONTHBF) {
			c.setTime(referenceDate);
			c.add(Calendar.MONTH, -3);
		} else {
			c.setTime(referenceDate);
			// c.add(Calendar.MONTH, 1);
		}

		Date newdate = c.getTime();
		String inputPattern = "EEE MMM d HH:mm:ss zzz yyyy";
		String outputPattern = "dd/MM/yyyy";

		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
		SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

		Date date = null;
		String str = "";

		try {
			date = inputFormat.parse(newdate.toString());
			str = outputFormat.format(date);
			Log.i("mini", "Converted Date Today:" + str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return str;
	}

	public static String CharacterReplacement(String date) {
		return date.replaceAll("/", "-");
	}

	public static int getMaxValue(int intVal) {
		// count the number of digits
		int digitCounter = 0;
		int _tmpVal = intVal;
		while (_tmpVal > 0) {
			digitCounter++;
			_tmpVal /= 10;
		}
		if (digitCounter == 1) {
			intVal = 10;
		} else {
			int xValue = (intVal / (int) Math.pow(10, (digitCounter - 1)));
			if ((intVal % (int) Math.pow(10, (digitCounter - 1))) != 0) {
				intVal = (xValue + 1) * (int) Math.pow(10, (digitCounter - 1));
			}
		}
		return intVal;
	}

	public static void showProgress(Context mContext_, String msg) {
		try {
			RelativeLayout rel_pg = (RelativeLayout) ((Activity) mContext_).findViewById(R.id.rel_progress);
			rel_pg.getBackground().setAlpha(120);
			TextView txt_ = (TextView) ((Activity) mContext_).findViewById(R.id.google_progress_text);
			txt_.setText(msg);

			rel_pg.clearAnimation();
			Animation anim = AnimationUtils.loadAnimation(mContext_, android.R.anim.fade_in);
			rel_pg.setAnimation(anim);
			rel_pg.setVisibility(View.VISIBLE);

			if (rel_pg != null) {
				rel_pg.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

					}
				});
			}

		} catch (Exception e) {
			Log.e("showProgress", e.getMessage().toString());
			e.printStackTrace();
		}
	}

	public static void hideProgress(Context mContext_) {
		try {
			final RelativeLayout rel_pg = (RelativeLayout) ((Activity) mContext_).findViewById(R.id.rel_progress);
			if (rel_pg.getVisibility() == View.VISIBLE) {

				rel_pg.clearAnimation();
				Animation anim = AnimationUtils.loadAnimation(mContext_, android.R.anim.fade_out);
				rel_pg.setAnimation(anim);
				anim.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation arg0) {

					}

					@Override
					public void onAnimationRepeat(Animation arg0) {

					}

					@Override
					public void onAnimationEnd(Animation arg0) {
						rel_pg.setVisibility(View.GONE);
					}
				});

			}
		} catch (Exception e) {
			Log.e("hideProgress", e.getMessage().toString());
			e.printStackTrace();
		}
	}

	public static String getAfterWeekDiff(String date, String dateFormat, int days) {
		String date_ = "";
		try {
			SimpleDateFormat formate = new SimpleDateFormat(dateFormat);
			Date myDate = formate.parse(date);
			Date newDate = new Date(myDate.getTime() + days * 7 * 24 * 60 * 60 * 1000L);
			date_ = formate.format(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date_;
	}

	public static String getFormatedDate(String date) {
		String date_ = "";
		SimpleDateFormat input = new SimpleDateFormat("dd/MM/yy");
		SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy", Locale.US);
		try {
			Date inputDate = input.parse(date);
			date_ = output.format(inputDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date_;
	}

	public static String getEncodeData(String date) {
		try {
			return URLEncoder.encode(date, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static String getOceanSearchData(String oldDate) {

		String newdate = "";
		String[] splText = oldDate.split(" ");
		if (splText.length >= 6) {
			newdate = splText[1] + " " + splText[2] + " " + splText[5];
		}
		return newdate;
	}

	public static float convertDpToPixel(Context context, float dp) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	public static float convertPixelsToDp(Context context, float px) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}

}
