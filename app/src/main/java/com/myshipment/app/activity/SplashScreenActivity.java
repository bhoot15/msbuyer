package com.myshipment.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.myshipment.app.Util;
import com.myshipment.R;
import com.myshipment.app.model.UserDetails;

public class SplashScreenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splashscreen);

		gotoNext();

	}

	/**
	 * Delay for Go to next Screen.
	 */
	private void gotoNext() {
		new Handler().postDelayed(new Runnable() {
			public void run() {
				UserDetails mDetails = UserDetails.getLoggedInUser(SplashScreenActivity.this);
				if (mDetails != null && mDetails.getUsername() != null && !mDetails.getUsername().equals("")) {
//					startActivity(new Intent(SplashScreenActivity.this, DashBoardOldActivity.class));
					startActivity(new Intent(SplashScreenActivity.this, NewDashboardActivity.class));
				} else {
					startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
				}
				overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				finish();
			}
		}, Util.splashTime);
	}

}
