package com.myshipment.app.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.Utility.ApiUrls;
import com.myshipment.app.Utility.SharedPreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//xubi
public class LoginActivity extends AppCompatActivity implements OnClickListener, OnKeyListener {

    private EditText edit_username, edit_passw;
    private ProgressDialog dialog;
    Snackbar snackbar;
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        initiliz();
        checkIfAlreadyLoggedIn();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.slide_out_right);
    }

    private void checkIfAlreadyLoggedIn() {
        if (!(SharedPreferenceManager.getUserCode(this).equals(""))) {
            //Intent intent = new Intent(this, HomeDashBoardActivity.class);
            Intent intent = new Intent(this, NewDashboardActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.activitylogin_btn_login:
                SubmitButtonEvent();
                HideKeyboard(LoginActivity.this, v);
                break;

            case R.id.activitylogin_img_forpass:
                ForgotPasswordButtonEvent();
                HideKeyboard(LoginActivity.this, v);
                break;

            default:
                break;
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_ENTER:
                    SubmitButtonEvent();
                    return true;
                default:
                    break;
            }
        }
        return false;
    }

    private void initiliz() {
        edit_passw = (EditText) findViewById(R.id.activitylogin_edit_password);
        edit_username = (EditText) findViewById(R.id.activitylogin_edit_user_name);
        ((TextView) findViewById(R.id.activitylogin_btn_login)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.activitylogin_img_forpass)).setOnClickListener(this);
        edit_passw.setOnKeyListener(this);
    }

    private void SubmitButtonEvent() {

        String username = edit_username.getText().toString();
        String password = edit_passw.getText().toString();

        if (username != null && username.length() == 0) {
            //Util.ShowToast(LoginActivity.this, getResources().getString(R.string.login_screen_enterusername));
            snackbar = Snackbar
                    .make(edit_passw, "Please, enter username", Snackbar.LENGTH_LONG);

            snackbar.show();
            return;
        }
        if (password != null && password.length() == 0) {
            //Util.ShowToast(LoginActivity.this, getResources().getString(R.string.login_screen_enterpass));
            snackbar = Snackbar
                    .make(edit_passw, "Please, enter password", Snackbar.LENGTH_LONG);

            snackbar.show();
            return;
        }

        if (!Util.hasConnection(LoginActivity.this)) {
            //Util.ShowToast(LoginActivity.this, getResources().getString(R.string.no_internet_connection));
            snackbar = Snackbar
                    .make(edit_passw, "Please, Check Internet Connection", Snackbar.LENGTH_LONG);

            snackbar.show();
            return;
        }
        callLoginApi(username, password);
    /*	LoginParser mLoginParser = new LoginParser();
        mLoginParser.setLogininterface(new LoginInterface() {

			@Override
			public void OnError() {
				Util.ShowToast(LoginActivity.this, getResources().getString(R.string.Servererror));
			}

			@Override
			public void OnSuccess(UserDetails mDetails) {
				startActivity(new Intent(LoginActivity.this, HomeDashBoardActivity.class));
				overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				finish();
			}
		});
		mLoginParser.parse(LoginActivity.this, username, password);
*/
    }

    private void callLoginApi(final String userCode, String password) {

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userCode", userCode);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();

        System.out.println("Login json:" + jsonObject.toString());
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.LOGIN_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {
                    // Parsing json object response
                    System.out.println("respose--> " + response);

                    JSONObject jsonObject = response.getJSONObject("bapiReturn2");
                    String accGroup = response.getString("accgroup");
                    //String userName = response.getString("loggedInUserName");
                    JSONArray salesAreaArray = response.getJSONArray("salesAreas");

                    //System.out.println("accgrp --> "+accGroup);
                    String userName = salesAreaArray.getJSONObject(0).getString("customer");

                    //System.out.println("uname ----> "+userName);


                    String message = jsonObject.getString("message");
                    JSONObject jsonObjectPeAddress = response.getJSONObject("peAddress");
                    String name = jsonObjectPeAddress.getString("name");
                    if (message.equalsIgnoreCase("Login successfull!!")) {

                        Intent intent = new Intent(LoginActivity.this, CompanySelectionActivity.class);
                        //intent.putExtra("loginResponse", response.toString());
                        //intent.putExtra("usercode", userCode);
                        //intent.putExtra("accGroup",accGroup);
                        //intent.putExtra("userName",userName);
                        SharedPreferenceManager.saveLoginRespone(response.toString(), getApplicationContext());
                        SharedPreferenceManager.saveUserCode(userCode, getApplicationContext());
                        SharedPreferenceManager.saveAccGroup(accGroup, getApplicationContext());
                        SharedPreferenceManager.saveUserName(userName, getApplicationContext());
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_left, R.anim.slide_right);
                        if (!name.equals("")) {
                            //System.out.println("---name---> "+name);
                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("name", name);
                            editor.apply();
                        }

                        finish();
                    } else {
                        /*Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.valid_usercode_pass_text),
                                Toast.LENGTH_LONG).show();*/
                        snackbar = Snackbar
                                .make(edit_passw, "Enter Valid Usercode & Password", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.error_authenticating_text),
                            Toast.LENGTH_LONG).show();*/
                    snackbar = Snackbar
                            .make(edit_passw, "Error while authenticating user.", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /*Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.error_authenticating_text), Toast.LENGTH_SHORT).show();*/
                snackbar = Snackbar
                        .make(edit_passw, "Error while authenticating user.", Snackbar.LENGTH_LONG);

                snackbar.show();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void ForgotPasswordButtonEvent() {
        String username = edit_username.getText().toString();
        if (username != null && username.length() == 0) {
            //Util.ShowToast(LoginActivity.this, getResources().getString(R.string.login_screen_enterusername));
            snackbar = Snackbar
                    .make(edit_passw, "Enter Username.", Snackbar.LENGTH_LONG);

            snackbar.show();
            return;
        }
        String url = Util.getForgotpasswUrl_("ForgotPassword", username);
        forgotPasswordEvent(url);
    }

    public Dialog onCreateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage(getResources().getString(R.string.do_you_want_to_exit)).setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        return builder.show();

    }

    /**
     * Hide Key Board.
     */

    public static void HideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void forgotPasswordEvent(final String url) {

        if (!Util.hasConnection(LoginActivity.this)) {
            //Util.ShowToast(LoginActivity.this, getResources().getString(R.string.no_internet_connection));
            snackbar = Snackbar
                    .make(edit_passw, "Check your internet connection", Snackbar.LENGTH_LONG);

            snackbar.show();
            return;
        }

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            private String authenticationJson;
            private String url_;
            private boolean isTimeOut = false;
            private boolean isSuccess = false;

            private String RESPONSECODE = "ResponseCode";
            private String RESPONSEDETAIL = "ResponseDetail";
            /* Response JSON key value */
            private String responsecode = "";
            private String responsedetails = "";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Util.showProgress(LoginActivity.this, "");
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    String[] responsedata = Util.sendGet(url);
                    authenticationJson = responsedata[1];
                    isTimeOut = (responsedata[0].equals("205")) ? true : false;
                    if (authenticationJson != null && !authenticationJson.equals("")) {
                        JSONObject jsObj = new JSONObject(authenticationJson);
                        responsecode = Util.getJsonValue(jsObj, RESPONSECODE);
                        if (responsecode != null && responsecode.equals("200")) {
                            isSuccess = true;
                            responsedetails = Util.getJsonValue(jsObj, RESPONSEDETAIL);
                        } else {
                            isSuccess = true;
                            responsedetails = Util.getJsonValue(jsObj, RESPONSEDETAIL);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                Util.hideProgress(LoginActivity.this);
                //Util.ShowToast(LoginActivity.this, responsedetails);
            }

        };

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null, null);
        } else {
            task.execute(null, null, null);
        }

    }
}
