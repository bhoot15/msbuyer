package com.myshipment.app.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.myshipment.R;
import com.myshipment.app.RegisterActivities;
import com.myshipment.app.Utility.ApiUrls;
import com.myshipment.app.Utility.GeneralUtils;
import com.myshipment.app.Utility.SharedPreferenceManager;
import com.myshipment.app.beans.ArrayListManagerModel;
import com.myshipment.app.beans.CommInvData;
import com.myshipment.app.beans.ItemData;
import com.myshipment.app.beans.POData;
import com.myshipment.app.beans.TrackingDataNew;
import com.myshipment.app.model.UserDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

public class TrackingActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    /*private RecyclerView trackingIdRecyclerView;*/
    private Button trackButton;
    private MaterialSpinner referenceSpinner;
    private MaterialSpinner statusSpinner;
    private ProgressDialog dialog;
    private EditText valueEditText, toDateEditText, fromDateEditText;
    private TextView userNameTextViewNav;
    private LinearLayout toDateLayout, fromDateLayout, statusLayout;
    private DateFormat dateFormatter;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private String status = "";
    private String referenceNo = "HBL";
    Snackbar snackbar;
    String eta, etd, pol, pod;
    FirebaseAnalytics mFirebaseAnalytics;


    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Track");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        userNameTextViewNav = (TextView) header.findViewById(R.id.userNameTextViewNav);
        TextView divisionTextViewNav = (TextView) header.findViewById(R.id.divisionTextViewNav);
        TextView distributionChannelTextViewNav = (TextView) header.findViewById(R.id.distributionChannelTextViewNav);
        TextView salesOrgTextViewNav = (TextView) header.findViewById(R.id.salesOrgTextViewNav);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name = preferences.getString("name", "");
        if (!name.equalsIgnoreCase("")) {
            userNameTextViewNav.setText(name);
        }
        String division = SharedPreferenceManager.getDivision(this);
        if (Objects.equals(division, "AR")) {
            divisionTextViewNav.setText("AIR");
        } else
            divisionTextViewNav.setText("SEA");

        String channel = SharedPreferenceManager.getDistChannel(this);
        if (Objects.equals(channel, "EX")) {
            distributionChannelTextViewNav.setText("EXPORT");

        }
        salesOrgTextViewNav.setText(SharedPreferenceManager.getSalesOrg(this));

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
        int randomColor = generator.getRandomColor();
// reuse the builder specs to create multiple drawables
        TextDrawable textDrawable = TextDrawable.builder()
                .buildRound(name.substring(0, 1), randomColor);

        ImageView image = (ImageView) header.findViewById(R.id.image_view);
        image.setImageDrawable(textDrawable);

        init();
        setListeners();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        setDatePickerDialogs();
        setSpinner();
    }

    private void init() {
        trackButton = (Button) findViewById(R.id.trackButton);
        referenceSpinner = (MaterialSpinner) findViewById(R.id.referenceSpinner);
        valueEditText = (EditText) findViewById(R.id.valueEditText);
        toDateLayout = (LinearLayout) findViewById(R.id.toDateLayout);
        fromDateLayout = (LinearLayout) findViewById(R.id.fromDateLayout);
        statusLayout = (LinearLayout) findViewById(R.id.statusLayout);
        statusSpinner = (MaterialSpinner) findViewById(R.id.statusSpinner);
        fromDateEditText = (EditText) findViewById(R.id.fromDateEditText);
        toDateEditText = (EditText) findViewById(R.id.toDateEditText);
    }

    /*@Override
    public void onResume() {
        super.onResume();

        setTabinitilize(TrackingActivity.this);
        setTabSelection(tabSelection.TrackAndSearch.ordinal());
        setCurrentSelection(tabSelection.TrackAndSearch.ordinal());
    }*/


    private void setSpinner() {
        String str[] = {"HBL No.", "HBL No."};
        referenceSpinner.setItems(str);

        String str2[] = {"None", "Order Booked", "Cargo Received", "Stuffing Done"};
        statusSpinner.setItems(str2);
    }

    private void setDatePickerDialogs() {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEditText.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEditText.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private void setListeners() {
        trackButton.setOnClickListener(this);
        //menuIcon.setOnClickListener(this);
        fromDateEditText.setOnClickListener(this);
        toDateEditText.setOnClickListener(this);

        referenceSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if (position == 2) {
                    toDateLayout.setVisibility(View.VISIBLE);
                    fromDateLayout.setVisibility(View.VISIBLE);
                    statusLayout.setVisibility(View.VISIBLE);
                } else {
                    toDateLayout.setVisibility(View.GONE);
                    fromDateLayout.setVisibility(View.GONE);
                    statusLayout.setVisibility(View.GONE);
                }

                if (position == 0) {
                    referenceNo = "HBL";
                } else if (position == 1) {
                    //referenceNo = "PO";
                    referenceNo = "HBL";
                } else if (position == 2) {
                    referenceNo = "COMM";
                }
            }
        });

        statusSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                //  Toast.makeText(getActivity(), position + "", Toast.LENGTH_SHORT).show();
                if (position == 0) {
                    status = "";
                } else if (position == 1) {
                    status = "Order Booked";
                } else if (position == 2) {
                    status = "Cargo Received";
                } else if (position == 3) {
                    status = "Stuffing Done";
                }
            }
        });
    }

    private void showDialog() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.spin_kit);
        TextView textView = (TextView) findViewById(R.id.progressBarinsideText);
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        DoubleBounce doubleBounce = new DoubleBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);
    }

    private void dismissDialog() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.spin_kit);
        progressBar.setVisibility(View.GONE);
        TextView textView = (TextView) findViewById(R.id.progressBarinsideText);
        textView.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == trackButton.getId()) {
            //showAddTrackingDialog();
            GeneralUtils.ScaleAnimate(trackButton, this);

            if (referenceNo.equalsIgnoreCase("HBL")) {
                // hide virtual keyboard
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                checkForValidations();
            } else if (referenceNo.equalsIgnoreCase("PO") || referenceNo.equalsIgnoreCase("COMM")) {
                if (GeneralUtils.isNetworkAvailable(this)) {
                    checkPOValidations();
                } else {
                    //Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                    snackbar = Snackbar
                            .make(userNameTextViewNav, "Please check internet connection", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }
            }


        } /*else if (v.getId() == menuIcon.getId()) {
           // ((TrackingActivity) this).clickEventSlide();
        } */ else if (v.getId() == fromDateEditText.getId()) {
            //Toast.makeText(this,"hi",Toast.LENGTH_SHORT).show();
            fromDatePickerDialog.show();
        } else if (v.getId() == toDateEditText.getId()) {
            toDatePickerDialog.show();
        }
    }

    private void checkForValidations() {
        if (GeneralUtils.isNetworkAvailable(this)) {
            String value = valueEditText.getText().toString().toUpperCase();
            if (!value.isEmpty()) {
                callTrackingApi(value);
            } else {
                //Toast.makeText(this, "Please enter value.", Toast.LENGTH_SHORT).show();
                snackbar = Snackbar
                        .make(userNameTextViewNav, "Please enter HBL number", Snackbar.LENGTH_LONG);

                snackbar.show();
            }

        } else {
            //Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
            snackbar = Snackbar
                    .make(userNameTextViewNav, "Please check internet connection", Snackbar.LENGTH_LONG);

            snackbar.show();
        }

    }

    private void checkPOValidations() {
        String fromDate = fromDateEditText.getText().toString();
        String toDate = toDateEditText.getText().toString();
        String value = valueEditText.getText().toString().toUpperCase();

        //All empty check


        //Date checks
        if (!(value.isEmpty())) {
            if (referenceNo.equalsIgnoreCase("PO")) {
                callPOApi(value, fromDate, toDate);
            } else if (referenceNo.equalsIgnoreCase("COMM")) {
                callCommonInvoiceApi(value, fromDate, toDate);
            }
        } else if (!(fromDate.isEmpty()) && !(toDate.isEmpty())) {
            if (referenceNo.equalsIgnoreCase("PO")) {
                callPOApi(value, fromDate, toDate);
            } else if (referenceNo.equalsIgnoreCase("COMM")) {
                callCommonInvoiceApi(value, fromDate, toDate);
            }
        } else if (!(fromDate.isEmpty()) && toDate.isEmpty()) {
            /*Toast.makeText(this, "Please fill To Date!", Toast.LENGTH_LONG).show();*/
            snackbar = Snackbar
                    .make(userNameTextViewNav, "Fill To date", Snackbar.LENGTH_LONG);

            snackbar.show();
        } else if (fromDate.isEmpty() && !(toDate.isEmpty())) {
            /*Toast.makeText(this, "Please fill From Date!", Toast.LENGTH_LONG).show();*/
            snackbar = Snackbar
                    .make(userNameTextViewNav, "Fill From date", Snackbar.LENGTH_LONG);

            snackbar.show();
        } else if (value.isEmpty() && !(status.equals(""))) {//PO & status check
            //Toast.makeText(this, "Please fill reference Value!!", Toast.LENGTH_LONG).show();
            snackbar = Snackbar
                    .make(userNameTextViewNav, "Fill Reference Value", Snackbar.LENGTH_LONG);

            snackbar.show();
        } else if (status.equals("")) {
            //Toast.makeText(this, "Please fill some Value!!", Toast.LENGTH_LONG).show();
            snackbar = Snackbar
                    .make(userNameTextViewNav, "Fill Value!!", Snackbar.LENGTH_LONG);

            snackbar.show();
        } else {
            System.out.println("value" + value);
            if (referenceNo.equalsIgnoreCase("PO")) {
                callPOApi(value, fromDate, toDate);
            } else if (referenceNo.equalsIgnoreCase("COMM")) {
                callCommonInvoiceApi(value, fromDate, toDate);
            }
        }


        //  callPOApi(value, fromDate, toDate);
    }

    private void callPOApi(String value, String fromDate, String toDate) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("buyer_no", SharedPreferenceManager.getUserCode(this));
            jsonObject.put("poNo", value);
            jsonObject.put("fromDate", fromDate);
            jsonObject.put("toDate", toDate);
            jsonObject.put("statusType", status);
            System.out.println("PO::" + jsonObject);
            /*jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();*/
        showDialog();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.PO_DETAIL_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("poTrackingResultData");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<POData> poDataArrayList = new ArrayList<>();
                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            POData poData = new POData();
                            poData.setPo_no(jsonArray.getJSONObject(i).getString("po_no"));
                            poData.setDocument_no(jsonArray.getJSONObject(i).getString("document_no"));
                            poData.setBl_no(jsonArray.getJSONObject(i).getString("bl_no"));
                            poData.setBooking_date(jsonArray.getJSONObject(i).getString("booking_date"));
                            poData.setBl_date(jsonArray.getJSONObject(i).getString("bl_date"));
                            poData.setShipper_name(jsonArray.getJSONObject(i).getString("shipper_name"));
                            poData.setPol_code(jsonArray.getJSONObject(i).getString("pol_code"));
                            poData.setPod_code(jsonArray.getJSONObject(i).getString("pod_code"));
                            poData.setGross_wt(jsonArray.getJSONObject(i).getString("gross_wt"));
                            poData.setCharge_wt(jsonArray.getJSONObject(i).getString("charge_wt"));
                            poData.setTot_volume(jsonArray.getJSONObject(i).getString("tot_volume"));
                            poData.setGr_date(jsonArray.getJSONObject(i).getString("gr_date"));
                            poData.setShipment_date(jsonArray.getJSONObject(i).getString("shipment_date"));
                            poData.setContainer_no(jsonArray.getJSONObject(i).getString("container_no"));
                            poData.setContainer_type(jsonArray.getJSONObject(i).getString("container_type"));
                            poData.setAta(jsonArray.getJSONObject(i).getString("ata"));
                            poData.setAtd(jsonArray.getJSONObject(i).getString("atd"));
                            poData.setEtd(jsonArray.getJSONObject(i).getString("etd"));
                            poData.setEta(jsonArray.getJSONObject(i).getString("eta"));
                            poData.setFeta(jsonArray.getJSONObject(i).getString("feta"));
                            poData.setFetd(jsonArray.getJSONObject(i).getString("fetd"));
                            poDataArrayList.add(poData);

                        }
                        arrayListManagerModel.setPoDataArrayList(poDataArrayList);
                        Intent intent = new Intent(TrackingActivity.this, POActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        /*Toast.makeText(TrackingActivity.this,
                                getResources().getString(R.string.no_data),
                                Toast.LENGTH_LONG).show();*/
                        snackbar = Snackbar
                                .make(userNameTextViewNav, "No Data", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("Error:" + e);
                    snackbar = Snackbar
                            .make(userNameTextViewNav, "Error While Fetching Data", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                snackbar = Snackbar
                        .make(userNameTextViewNav, "Error Fetching", Snackbar.LENGTH_LONG);

                snackbar.show();
                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void callCommonInvoiceApi(String value, String fromDate, String toDate) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("shipper_no", SharedPreferenceManager.getUserCode(this));
            jsonObject.put("commInvNo", value);
            jsonObject.put("fromDate", fromDate);
            jsonObject.put("toDate", toDate);
            jsonObject.put("statusType", status);
            /*jsonObject.put("shipper_no","1000000037");
            jsonObject.put("commInvNo", "TEST-01");
            jsonObject.put("fromDate","");
            jsonObject.put("toDate", "");
            jsonObject.put("statusType", "Order Booked");*/
            System.out.println("CO::" + jsonObject);
            /*jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*dialog = new ProgressDialog(TrackingActivity.this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();*/
        showDialog();

        RequestQueue requestQueue = Volley.newRequestQueue(TrackingActivity.this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.COMM_INVOICE_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("poTrackingResultData");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<CommInvData> commInvDataArrayList = new ArrayList<>();
                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            CommInvData commInvData = new CommInvData();
                            commInvData.setPoNo(jsonArray.getJSONObject(i).getString("po_no"));
                            commInvData.setDocumentNo(jsonArray.getJSONObject(i).getString("document_no"));
                            commInvData.setHblNo(jsonArray.getJSONObject(i).getString("bl_no"));
                            commInvData.setBookingDate(jsonArray.getJSONObject(i).getString("booking_date"));
                            commInvData.setShipperName(jsonArray.getJSONObject(i).getString("shipper_name"));
                            commInvData.setPol(jsonArray.getJSONObject(i).getString("pol_name"));
                            commInvData.setPod(jsonArray.getJSONObject(i).getString("pod_name"));
                            commInvData.setGrossWt(jsonArray.getJSONObject(i).getString("gross_wt"));
                            commInvData.setGoodReceivedDate(jsonArray.getJSONObject(i).getString("gr_date"));
                            commInvData.setShipmentDate(jsonArray.getJSONObject(i).getString("shipment_date"));
                            commInvData.setAta(jsonArray.getJSONObject(i).getString("ata"));
                            commInvData.setAtd(jsonArray.getJSONObject(i).getString("atd"));
                            commInvData.setEtd(jsonArray.getJSONObject(i).getString("etd"));
                            commInvData.setEta(jsonArray.getJSONObject(i).getString("eta"));
                            commInvData.setCommInvNo(jsonArray.getJSONObject(i).getString("comm_invoice_no"));
                            commInvData.setConsignee(jsonArray.getJSONObject(i).getString("buyer_name"));
                            commInvData.setCarrier(GeneralUtils.checkNullValue(jsonArray.getJSONObject(i).getString("carrier_name")));
                            commInvData.setDeliveryAgent(GeneralUtils.checkNullValue(jsonArray.getJSONObject(i).getString("agent_name")));
                            commInvData.setTotalPcs(jsonArray.getJSONObject(i).getString("tot_pcs"));
                            commInvData.setFeta(jsonArray.getJSONObject(i).getString("feta"));
                            commInvData.setFetd(jsonArray.getJSONObject(i).getString("fetd"));
                            commInvDataArrayList.add(commInvData);

                        }
                        arrayListManagerModel.setCommInvDataArrayList(commInvDataArrayList);
                        Intent intent = new Intent(TrackingActivity.this, CommInvTrackingActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                    } else {
                        snackbar = Snackbar
                                .make(userNameTextViewNav, "No Data", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    snackbar = Snackbar
                            .make(userNameTextViewNav, "Error Fetching", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                snackbar = Snackbar
                        .make(userNameTextViewNav, "Error Fetching", Snackbar.LENGTH_LONG);

                snackbar.show();
                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void callTrackingApi(String value) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer", SharedPreferenceManager.getUserName(this));
            jsonObject.put("salesOrg", SharedPreferenceManager.getSalesOrgCode(this));
            jsonObject.put("distChan", SharedPreferenceManager.getDistChannel(this));
            jsonObject.put("division", SharedPreferenceManager.getDivision(this));
            jsonObject.put("erdat1", "");
            jsonObject.put("erdat2", "");
            if (Objects.equals(SharedPreferenceManager.getDivision(this), "SE")) {
                jsonObject.put("action", "01");
            } else {
                jsonObject.put("action", "03");
            }

            jsonObject.put("doc_no", value);

            System.out.println("track--> " + jsonObject);
            System.out.println("URL--> " + ApiUrls.TRACKING_URL);
            /*jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();*/
        showDialog();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.TRACKING_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("myshipmentTrackHeader");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<TrackingDataNew> trackingDataNewArrayList = new ArrayList<>();
                    if (length > 0) {
                        System.out.println("size: --> " + length);
                        for (int i = 0; i < length; i++) {
                            TrackingDataNew trackingDataNew = new TrackingDataNew();
                            trackingDataNew.setBl_no(jsonArray.getJSONObject(i).getString("bl_no"));
                            trackingDataNew.setBl_bt(jsonArray.getJSONObject(i).getString("bl_bt"));
                            trackingDataNew.setShipper(jsonArray.getJSONObject(i).getString("shipper"));
                            trackingDataNew.setBuyer(jsonArray.getJSONObject(i).getString("buyer"));
                            trackingDataNew.setPol(jsonArray.getJSONObject(i).getString("pol"));
                            trackingDataNew.setPod(jsonArray.getJSONObject(i).getString("pod"));
                            trackingDataNew.setPlod(jsonArray.getJSONObject(i).getString("plod"));
                            trackingDataNew.setFrt_mode_ds(jsonArray.getJSONObject(i).getString("frt_mode_ds"));
                            trackingDataNew.setTot_qty(jsonArray.getJSONObject(i).getString("tot_qty"));
                            trackingDataNew.setVolume(jsonArray.getJSONObject(i).getString("volume"));
                            trackingDataNew.setGrs_wt(jsonArray.getJSONObject(i).getString("grs_wt"));
                            trackingDataNew.setComm_inv(jsonArray.getJSONObject(i).getString("comm_inv"));
                            trackingDataNew.setComm_inv_dt(jsonArray.getJSONObject(i).getString("comm_inv_dt"));
                            trackingDataNew.setPor(jsonArray.getJSONObject(i).getString("por"));
                            trackingDataNew.setGr_dt(jsonArray.getJSONObject(i).getString("gr_dt"));
                            trackingDataNew.setMbl_no(jsonArray.getJSONObject(i).getString("mbl_no"));
                            trackingDataNew.setCarrier(jsonArray.getJSONObject(i).getString("carrier"));
                            trackingDataNew.setStuff_dt(jsonArray.getJSONObject(i).getString("stuff_dt"));
                            trackingDataNew.setDep_dt(jsonArray.getJSONObject(i).getString("dep_dt"));
                            trackingDataNew.setLoad_dt(jsonArray.getJSONObject(i).getString("load_dt"));
                            trackingDataNew.setEta_dt(jsonArray.getJSONObject(i).getString("eta_dt"));
                            trackingDataNew.setAgent(jsonArray.getJSONObject(i).getString("agent"));
                            trackingDataNewArrayList.add(trackingDataNew);

                            eta = jsonArray.getJSONObject(i).getString("eta_dt");
                            etd = jsonArray.getJSONObject(i).getString("dep_dt");
                            pol = jsonArray.getJSONObject(i).getString("pol");
                            pod = jsonArray.getJSONObject(i).getString("pod");

                        }

                        SharedPreferenceManager.saveEta(eta, getApplicationContext());
                        SharedPreferenceManager.saveEtd(etd, getApplicationContext());
                        SharedPreferenceManager.savePol(pol, getApplicationContext());
                        SharedPreferenceManager.savePod(pod, getApplicationContext());


                        arrayListManagerModel.setTrackingDataNewArrayList(trackingDataNewArrayList);
                        if (trackingDataNewArrayList.size() > 0) {
                            Intent intent = new Intent(TrackingActivity.this, MilestoneTrackingActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            /*Toast.makeText(TrackingActivity.this, "No data available", Toast.LENGTH_SHORT).show();*/
                            snackbar = Snackbar
                                    .make(userNameTextViewNav, "No data available", Snackbar.LENGTH_LONG);

                            snackbar.show();
                        }
                    } else {
                        /*Toast.makeText(TrackingActivity.this,
                                getResources().getString(R.string.error_fetching),
                                Toast.LENGTH_LONG).show();*/
                        snackbar = Snackbar
                                .make(userNameTextViewNav, "Error While Fetching Data", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    /*Toast.makeText(TrackingActivity.this,
                            "Wrong HBL Number or Error While Fetching Data",
                            Toast.LENGTH_LONG).show();*/
                    snackbar = Snackbar
                            .make(userNameTextViewNav, "Wrong HBL Number or Error While Fetching Data", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }
                try {

                    // Parsing json object response
                    //System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("myshipmentTrackItem");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<ItemData> itemDataArrayList = new ArrayList<>();
                    if (length > 0) {
                        System.out.println("size item: --> " + length);
                        for (int i = 0; i < length; i++) {
                            ItemData itemData = new ItemData();
                            itemData.setPo_no(jsonArray.getJSONObject(i).getString("po_no"));
                            itemData.setCommodity(jsonArray.getJSONObject(i).getString("commodity"));
                            itemData.setStyle(jsonArray.getJSONObject(i).getString("style"));
                            itemData.setSize(jsonArray.getJSONObject(i).getString("size"));
                            itemData.setSku(jsonArray.getJSONObject(i).getString("sku"));
                            itemData.setArt_no(jsonArray.getJSONObject(i).getString("art_no"));
                            itemData.setColor(jsonArray.getJSONObject(i).getString("color"));
                            itemData.setItem_pcs(jsonArray.getJSONObject(i).getString("item_pcs"));
                            itemData.setCart_sno(jsonArray.getJSONObject(i).getString("item_qty"));
                            itemData.setItem_vol(jsonArray.getJSONObject(i).getString("item_vol"));
                            itemData.setItem_grwt(jsonArray.getJSONObject(i).getString("item_grwt"));

                            itemDataArrayList.add(itemData);

                        }

                        arrayListManagerModel.setItemDataArrayList(itemDataArrayList);
                        if (itemDataArrayList.size() > 0) {
                            Intent intent = new Intent(TrackingActivity.this, MilestoneTrackingActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            snackbar = Snackbar
                                    .make(userNameTextViewNav, "No Data", Snackbar.LENGTH_LONG);

                            snackbar.show();
                        }
                    } else {
                        snackbar = Snackbar
                                .make(userNameTextViewNav, "Error Fetching", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                snackbar = Snackbar
                        .make(userNameTextViewNav, "Error Fetching", Snackbar.LENGTH_LONG);

                snackbar.show();
                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            Intent intent = new Intent(TrackingActivity.this, NewDashboardActivity.class);
            startActivity(intent);
            //finish();

        } else if (id == R.id.nav_track) {

            Intent intent = new Intent(TrackingActivity.this, TrackingActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_support) {
            Uri uri = Uri.parse("http://www.myshipment.com/#faq"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);

        } else if (id == R.id.company_select) {

            Intent intent = new Intent(TrackingActivity.this, CompanySelectionActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(TrackingActivity.this, AboutActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            ShowlogoutDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void ShowlogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(TrackingActivity.this);
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setMessage(getResources().getString(R.string.do_you_want_to_logout));
        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferenceManager.clearCredentials(TrackingActivity.this);
                UserDetails.logoutUser(TrackingActivity.this);
                Intent intent = new Intent(TrackingActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                RegisterActivities.removeAllActivities();
            }
        });
        builder.create();
        builder.setCancelable(false);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}