package com.myshipment.app.activity;

/**
 * Created by zobair on 20-Aug-17.
 */

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.myshipment.R;
import com.myshipment.app.RegisterActivities;
import com.myshipment.app.Utility.ApiUrls;
import com.myshipment.app.Utility.AppConstants;
import com.myshipment.app.Utility.CustomDateDialog;
import com.myshipment.app.Utility.DatePickerCallBack;
import com.myshipment.app.Utility.GeneralUtils;
import com.myshipment.app.Utility.SharedPreferenceManager;
import com.myshipment.app.beans.ArrayListManagerModel;
import com.myshipment.app.beans.CommInvData;
import com.myshipment.app.beans.DashboardInfoBox;
import com.myshipment.app.beans.DashboardInfoRequestJson;
import com.myshipment.app.beans.DashboardTrackListData;
import com.myshipment.app.beans.ItemData;
import com.myshipment.app.beans.POData;
import com.myshipment.app.beans.TrackingDataNew;
import com.myshipment.app.model.UserDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

public class NewDashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    String pol, pod, eta, etd;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    private ImageView inTransitIcon;
    private ProgressDialog dialog;
    private RelativeLayout totalShipmentLayout, totalGoodsHandoverLayout, totalOpenBookingsLayout, totalStuffingDoneLayout, totalIntransitLayout, totalArrivedLayout;
    private TextView totalShipmentTextView, openOrderTextView, goodsReceivedTextView, stuffingDoneTextView, inTransitTextView, deliveredTextView, userNameTextView, divisionTextView, distributionChannelTextView, salesOrgTextView, userNameTextViewNav, divisionTextViewNav, distributionChannelTextViewNav, salesOrgTextViewNav;
    private DatePickerCallBack datePickerCallBack;
    Snackbar snackbar;

    BottomSheetDialog bottomSheetDialog;
    RelativeLayout bottomSheetLayout;
    CoordinatorLayout coordinatorLayout;
    ImageView iv_trigger;


    Button trackButton;
    MaterialSpinner referenceSpinner;
    MaterialSpinner statusSpinner;
    EditText valueEditText, toDateEditText, fromDateEditText;
    LinearLayout toDateLayout, fromDateLayout, statusLayout;
    DateFormat dateFormatter;
    DatePickerDialog fromDatePickerDialog;
    DatePickerDialog toDatePickerDialog;
    String status = "";
    String referenceNo = "HBL";

    SwipeRefreshLayout mSwipeRefreshLayout;
    FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_dashboard);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("myshipment");
        setSupportActionBar(toolbar);
        /*getSupportActionBar().setTitle("Xubair Shoumik \n"+SharedPreferenceManager.getDivision(this)+"\n"+SharedPreferenceManager.getDistChannel(this));*/
        final View titleLayout = findViewById(R.id.layout_title);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        userNameTextViewNav = (TextView) header.findViewById(R.id.userNameTextViewNav);
        divisionTextViewNav = (TextView) header.findViewById(R.id.divisionTextViewNav);
        distributionChannelTextViewNav = (TextView) header.findViewById(R.id.distributionChannelTextViewNav);
        salesOrgTextViewNav = (TextView) header.findViewById(R.id.salesOrgTextViewNav);

        userNameTextView = (TextView) findViewById(R.id.userNameTextView);
        divisionTextView = (TextView) findViewById(R.id.divisionTextView);
        distributionChannelTextView = (TextView) findViewById(R.id.distributionChannelTextView);
        salesOrgTextView = (TextView) findViewById(R.id.salesOrgTextView);
        inTransitIcon = (ImageView) findViewById(R.id.inTransitIcon);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        init_persistent_bottomsheet();


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name = preferences.getString("name", "");
        if (!name.equalsIgnoreCase("")) {
            userNameTextView.setText(name);
            userNameTextViewNav.setText(name);
        }
        String division = SharedPreferenceManager.getDivision(this);
        if (Objects.equals(division, "AR")) {
            divisionTextView.setText("AIR");
            divisionTextViewNav.setText("AIR");
            inTransitIcon.setImageResource(R.drawable.ic_airplanemode_active_white_18dp);
        } else {
            divisionTextView.setText("SEA");
            divisionTextViewNav.setText("SEA");
            inTransitIcon.setImageResource(R.drawable.ic_directions_boat_white_18dp);
        }

        String channel = SharedPreferenceManager.getDistChannel(this);
        if (Objects.equals(channel, "EX")) {
            distributionChannelTextView.setText("EXPORT");
            distributionChannelTextViewNav.setText("EXPORT");

        }
        salesOrgTextView.setText(SharedPreferenceManager.getSalesOrg(this));
        salesOrgTextViewNav.setText(SharedPreferenceManager.getSalesOrg(this));

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
        final int randomColor = generator.getRandomColor();
// reuse the builder specs to create multiple drawables
        TextDrawable textDrawable = TextDrawable.builder()
                .buildRound(name.substring(0, 1), randomColor);

        ImageView image = (ImageView) header.findViewById(R.id.image_view);
        image.setImageDrawable(textDrawable);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        if (Objects.equals(SharedPreferenceManager.getDivision(this), "AR")) {
            fab.setImageResource(R.drawable.ic_flight_white_18dp);
        } else if (Objects.equals(SharedPreferenceManager.getDivision(this), "SE")) {
            fab.setImageResource(R.drawable.ic_directions_boat_white_18dp);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewDashboardActivity.this, TrackingActivity.class);
                startActivity(intent);
            }
        });*/

        setTitle(null);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //old items start
        init();
        setListeners();
        //setButtonAnimation();
        //call dashboard api the set pie chart and bar chart
        datePickerCallback();
        setDates();
        if (GeneralUtils.isNetworkAvailable(this)) {
            //callDashBoardApi();
            callTrackingApi();
        } else {
            //Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
            mSwipeRefreshLayout.setRefreshing(false);
            snackbar = Snackbar
                    .make(totalArrivedLayout, "Please Check Your Internet Connection", Snackbar.LENGTH_LONG);

            snackbar.show();

        }

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        //Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout");

                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent),getResources().getColor(R.color.darkorange),getResources().getColor(R.color.textColorPrimary),getResources().getColor(R.color.red));
                        if (GeneralUtils.isNetworkAvailable(getApplicationContext())) {
                            //callDashBoardApi();
                            callTrackingApi();
                        } else {
                            //Toast.makeText(getApplicationContext(), getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                            snackbar = Snackbar
                                    .make(totalArrivedLayout, "Please Check Your Internet Connection", Snackbar.LENGTH_LONG);

                            snackbar.show();
                        }
                    }
                }
        );
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        dismissDialog();
        //dismissDialogDb();
    }

    public void init_persistent_bottomsheet() {

        View persistentbottomSheet = coordinatorLayout.findViewById(R.id.bottomsheet);
        iv_trigger = (ImageView) persistentbottomSheet.findViewById(R.id.iv_fab);
        bottomSheetLayout = (RelativeLayout) persistentbottomSheet.findViewById(R.id.rl);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(persistentbottomSheet);


        bottomSheetLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        if (behavior != null)
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    //showing the different states
                    switch (newState) {
                        case BottomSheetBehavior.STATE_HIDDEN:
                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            break;
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:
                            break;
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    // React to dragging events

                }
            });

        initBottomSheet();
        setListenersBottomSheet();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        setDatePickerDialogs();
        setSpinner();

    }

    private void setSpinner() {
        String str[] = {"HBL No.", "HBL No."};
        referenceSpinner.setItems(str);

        String str2[] = {"None", "Order Booked", "Cargo Received", "Stuffing Done"};
        statusSpinner.setItems(str2);
    }

    private void setListenersBottomSheet() {
        trackButton.setOnClickListener(this);
        //menuIcon.setOnClickListener(this);
        fromDateEditText.setOnClickListener(this);
        toDateEditText.setOnClickListener(this);

        referenceSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if (position == 2) {
                    toDateLayout.setVisibility(View.VISIBLE);
                    fromDateLayout.setVisibility(View.VISIBLE);
                    statusLayout.setVisibility(View.VISIBLE);
                } else {
                    toDateLayout.setVisibility(View.GONE);
                    fromDateLayout.setVisibility(View.GONE);
                    statusLayout.setVisibility(View.GONE);
                }

                if (position == 0) {
                    referenceNo = "HBL";
                } else if (position == 1) {
                    //referenceNo = "PO";
                    referenceNo = "HBL";
                } else if (position == 2) {
                    referenceNo = "COMM";
                }
            }
        });

        statusSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                //  Toast.makeText(getActivity(), position + "", Toast.LENGTH_SHORT).show();
                if (position == 0) {
                    status = "";
                } else if (position == 1) {
                    status = "Order Booked";
                } else if (position == 2) {
                    status = "Cargo Received";
                } else if (position == 3) {
                    status = "Stuffing Done";
                }
            }
        });
    }

    private void setDatePickerDialogs() {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEditText.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEditText.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private void initBottomSheet() {
        trackButton = (Button) findViewById(R.id.trackButton);
        referenceSpinner = (MaterialSpinner) findViewById(R.id.referenceSpinner);
        valueEditText = (EditText) findViewById(R.id.valueEditText);
        toDateLayout = (LinearLayout) findViewById(R.id.toDateLayout);
        fromDateLayout = (LinearLayout) findViewById(R.id.fromDateLayout);
        statusLayout = (LinearLayout) findViewById(R.id.statusLayout);
        statusSpinner = (MaterialSpinner) findViewById(R.id.statusSpinner);
        fromDateEditText = (EditText) findViewById(R.id.fromDateEditText);
        toDateEditText = (EditText) findViewById(R.id.toDateEditText);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                Toast.makeText(getBaseContext(), "Press once again to exit!",
                        Toast.LENGTH_SHORT).show();
            }
            back_pressed = System.currentTimeMillis();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //return true;
            new CustomDateDialog(this, datePickerCallBack).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            Intent intent = new Intent(NewDashboardActivity.this, NewDashboardActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_track) {

            Intent intent = new Intent(NewDashboardActivity.this, TrackingActivity.class);
            startActivity(intent);

        } else if (id == R.id.company_select) {

            Intent intent = new Intent(NewDashboardActivity.this, CompanySelectionActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_support) {
            Uri uri = Uri.parse("http://www.myshipment.com/#faq"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);

        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(NewDashboardActivity.this, AboutActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            ShowlogoutDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void init() {

        //topShipmentsBtn = (Button) findViewById(R.id.topShipmentsBtn);
        userNameTextView = (TextView) findViewById(R.id.userNameTextView);
        divisionTextView = (TextView) findViewById(R.id.divisionTextView);
        distributionChannelTextView = (TextView) findViewById(R.id.distributionChannelTextView);
        salesOrgTextView = (TextView) findViewById(R.id.salesOrgTextView);

        userNameTextViewNav = (TextView) findViewById(R.id.userNameTextViewNav);
        divisionTextViewNav = (TextView) findViewById(R.id.divisionTextViewNav);
        distributionChannelTextViewNav = (TextView) findViewById(R.id.distributionChannelTextViewNav);
        salesOrgTextViewNav = (TextView) findViewById(R.id.salesOrgTextViewNav);

        totalShipmentTextView = (TextView) findViewById(R.id.totalBookingTextView);
        openOrderTextView = (TextView) findViewById(R.id.openBookigTextView);
        goodsReceivedTextView = (TextView) findViewById(R.id.goodsHandoverTextView);
        stuffingDoneTextView = (TextView) findViewById(R.id.stuffingDoneView);
        inTransitTextView = (TextView) findViewById(R.id.inTransitTextView);
        deliveredTextView = (TextView) findViewById(R.id.arrivedTextView);

        //clockIcon = (ImageView) findViewById(R.id.clock_icon);
        inTransitIcon = (ImageView) findViewById(R.id.inTransitIcon);

        totalShipmentLayout = (RelativeLayout) findViewById(R.id.totalShipmentLayout);
        totalGoodsHandoverLayout = (RelativeLayout) findViewById(R.id.totalGoodsHandoverLayout);
        totalOpenBookingsLayout = (RelativeLayout) findViewById(R.id.totalOpenBookingsLayout);
        totalStuffingDoneLayout = (RelativeLayout) findViewById(R.id.totalStuffingDoneLayout);
        totalIntransitLayout = (RelativeLayout) findViewById(R.id.totalInTransitLayout);
        totalArrivedLayout = (RelativeLayout) findViewById(R.id.totalArrivedLayout);
    }

    private void setListeners() {
        //topShipmentsBtn.setOnClickListener(this);
        totalGoodsHandoverLayout.setOnClickListener(this);
        totalOpenBookingsLayout.setOnClickListener(this);
        totalShipmentLayout.setOnClickListener(this);
        totalStuffingDoneLayout.setOnClickListener(this);
        totalIntransitLayout.setOnClickListener(this);
        totalArrivedLayout.setOnClickListener(this);
        //clockIcon.setOnClickListener(this);
    }

    private void setDates() {
        String fromDate = "", toDate = "";
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String Day = "";

        if (c.get(Calendar.DAY_OF_MONTH) < 10) {
            Day = "0" + c.get(Calendar.DAY_OF_MONTH);
        } else {
            Day = "" + c.get(Calendar.DAY_OF_MONTH);
        }

        String Month = "";
        if ((c.get(Calendar.MONTH) - 2) < 10) {
            Month = "0" + (c.get(Calendar.MONTH) - 2);
        } else {
            Month = "" + (c.get(Calendar.MONTH) - 2);
        }

        String year = c.get(Calendar.YEAR) + "";
        fromDate = year + "" + Month + "" + Day;
        if ((c.get(Calendar.MONTH) + 1) < 10) {
            Month = "0" + (c.get(Calendar.MONTH) + 1);
        } else {
            Month = "" + (c.get(Calendar.MONTH) + 1);
        }
        toDate = year + "" + Month + "" + Day;

        SharedPreferenceManager.setFromDate(fromDate, this);
        SharedPreferenceManager.setToDate(toDate, this);
    }

    private void setButtonAnimation() {
    /*    GeneralUtils.ScaleAnimate(topShipmentsBtn);
        GeneralUtils.ScaleAnimate(totalOpenBookingsLayout);
        GeneralUtils.ScaleAnimate(totalGoodsHandoverLayout);
        GeneralUtils.ScaleAnimate(totalShipmentLayout);*/
    }

    private void callTrackingApi() {
        final JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("action", "00");
            jsonObject.put("customer", SharedPreferenceManager.getUserName(this));
            jsonObject.put("distChan", SharedPreferenceManager.getDistChannel(this));
            jsonObject.put("division", SharedPreferenceManager.getDivision(this));
            jsonObject.put("salesOrg", SharedPreferenceManager.getSalesOrgCode(this));
            jsonObject.put("erdat1", "");
            jsonObject.put("erdat2", "");

            System.out.println("track--> " + jsonObject);
            System.out.println("URL--> " + ApiUrls.DASHBOARD_TRACKING_URL);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        showDialog();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.DASHBOARD_TRACKING_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("myshipmentTrackHeader");

                    SharedPreferenceManager.saveHeaderListString(response.toString(), getApplicationContext());


                    int length = jsonArray.length();

                    if (length > 0) {

                        /*for (int i = 0; i < length; i++) {

                            pol = jsonArray.getJSONObject(i).getString("pol");
                            pod = jsonArray.getJSONObject(i).getString("pod");
                            eta = jsonArray.getJSONObject(i).getString("eta_dt");
                            etd = jsonArray.getJSONObject(i).getString("dep_dt");

                        }*/

                        //System.out.println("POL~~~~~=> "+pol);
                        /*SharedPreferenceManager.savePol(pol, getApplicationContext());
                        SharedPreferenceManager.savePod(pod, getApplicationContext());*/

                        callDashBoardApi();


                    } else {
                        snackbar = Snackbar
                                .make(totalShipmentLayout, "No Data Available", Snackbar.LENGTH_LONG);

                        snackbar.show();
                        dismissDialog();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    snackbar = Snackbar
                            .make(totalShipmentLayout, "No Data Available", Snackbar.LENGTH_LONG);

                    snackbar.show();
                    dismissDialog();
                    mSwipeRefreshLayout.setRefreshing(false);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                snackbar = Snackbar
                        .make(totalShipmentLayout, "Error While Fetching Data", Snackbar.LENGTH_LONG);

                snackbar.show();
                dismissDialog();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void callDashBoardApi() {

        final JSONObject jsonObject = new JSONObject();

        String dashboardRequestGson = "";
        try {
            DashboardInfoRequestJson dashboardInfoRequestJson = new DashboardInfoRequestJson();

            String headerListString = SharedPreferenceManager.getHeaderListString(this);

            DashboardTrackListData dashboardTrackListData = new Gson().fromJson(headerListString, DashboardTrackListData.class);

            dashboardInfoRequestJson.setLoggedInUserName(SharedPreferenceManager.getUserCode(this));
            dashboardInfoRequestJson.setDivisionSelected(SharedPreferenceManager.getDivision(this));
            dashboardInfoRequestJson.setDisChnlSelected(SharedPreferenceManager.getDistChannel(this));
            dashboardInfoRequestJson.setAccgroup(SharedPreferenceManager.getAccGroup(this));
            dashboardInfoRequestJson.setSalesOrgSelected(SharedPreferenceManager.getSalesOrgCode(this));
            dashboardInfoRequestJson.setMyshipmentTrackHeader(dashboardTrackListData.getMyshipmentTrackHeader());

            dashboardRequestGson = new Gson().toJson(dashboardInfoRequestJson);
            System.out.println("db GSON ===> " + dashboardRequestGson);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();*/

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.DASHBOARD_INFO_URL, dashboardRequestGson, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                dismissDialog();
                mSwipeRefreshLayout.setRefreshing(false);

                try {
                    System.out.println("DASH Res::" + response);
                    // Parsing json object response
                    SharedPreferenceManager.saveTrackRespone(response.toString(), getApplicationContext());

                    String message = response.getString("status");
                    DashboardInfoBox dashboardInfoBox = new DashboardInfoBox();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<DashboardInfoBox> dashboardDataArrayList = new ArrayList<>();

                    if (message.equalsIgnoreCase("success")) {
                        dashboardInfoBox.setTotalShipment(Integer.valueOf(response.getString("totalShipment")));
                        dashboardInfoBox.setOpenOrder(Integer.valueOf(response.getString("openOrder")));
                        dashboardInfoBox.setGoodsReceived(Integer.valueOf(response.getString("goodsReceived")));
                        dashboardInfoBox.setStuffingDone(Integer.valueOf(response.getString("stuffingDone")));
                        dashboardInfoBox.setInTransit(Integer.valueOf(response.getString("inTransit")));
                        dashboardInfoBox.setDelivered(Integer.valueOf(response.getString("delivered")));
                        dashboardDataArrayList.add(dashboardInfoBox);
                        arrayListManagerModel.setDashboardDataArrayList(dashboardDataArrayList);

                        //System.out.println("`````~~~~ "+response.getJSONArray("myshipmentTrackHeader"));;

                        totalShipmentTextView.setText(response.getString("totalShipment"));
                        openOrderTextView.setText(response.getString("openOrder"));
                        goodsReceivedTextView.setText(response.getString("goodsReceived"));
                        stuffingDoneTextView.setText(response.getString("stuffingDone"));
                        inTransitTextView.setText(response.getString("inTransit"));
                        deliveredTextView.setText(response.getString("delivered"));

                    } else {

                        snackbar = Snackbar
                                .make(totalOpenBookingsLayout, "No Data Available", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    snackbar = Snackbar
                            .make(totalOpenBookingsLayout, "Error While Fetching Data", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                snackbar = Snackbar
                        .make(totalOpenBookingsLayout, "Error While Fetching Data", Snackbar.LENGTH_LONG);

                snackbar.show();
                dismissDialog();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    /*private void showDialog() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }*/
    private void showDialog() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.spin_kit);
        TextView textView = (TextView) findViewById(R.id.progressBarinsideText);
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        DoubleBounce doubleBounce = new DoubleBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);
    }

    /*private void showDialogDb() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
    private  void dismissDialogDb(){
        *//*dialog = new ProgressDialog(this);
        dialog.show();*//*
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }*/

    private void dismissDialog() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.spin_kit);
        progressBar.setVisibility(View.GONE);
        TextView textView = (TextView) findViewById(R.id.progressBarinsideText);
        textView.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == totalOpenBookingsLayout.getId()) {
            if (GeneralUtils.isNetworkAvailable(getApplicationContext())) {
                GeneralUtils.ScaleAnimate(totalOpenBookingsLayout, getApplicationContext());
                if (Objects.equals(openOrderTextView.getText().toString(), "0")) {
                    snackbar = Snackbar
                            .make(totalOpenBookingsLayout, "No Data Available", Snackbar.LENGTH_LONG);

                    snackbar.show();
                } else {
                    showDialog();
                    Intent intent = new Intent(NewDashboardActivity.this, DashboardInfoBoxClickActivity.class);
                    intent.putExtra("type", AppConstants.OPEN_ORDER);
                    startActivity(intent);
                    //finish();
                }
            } else {
                //Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                snackbar = Snackbar
                        .make(totalOpenBookingsLayout, "Please Check Your Internet Connection", Snackbar.LENGTH_LONG);

                snackbar.show();
            }

        } else if (v.getId() == totalGoodsHandoverLayout.getId()) {
            if (GeneralUtils.isNetworkAvailable(getApplicationContext())) {
                GeneralUtils.ScaleAnimate(totalGoodsHandoverLayout, getApplicationContext());
                if (Objects.equals(goodsReceivedTextView.getText().toString(), "0")) {
                    snackbar = Snackbar
                            .make(totalOpenBookingsLayout, "No Data Available", Snackbar.LENGTH_LONG);

                    snackbar.show();
                } else {
                    showDialog();
                    Intent intent = new Intent(NewDashboardActivity.this, DashboardInfoBoxClickActivity.class);
                    intent.putExtra("type", AppConstants.GOODS_RECEIVED);
                    startActivity(intent);
                }
            } else {
                //Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                snackbar = Snackbar
                        .make(totalGoodsHandoverLayout, "Please Check Your Internet Connection", Snackbar.LENGTH_LONG);

                snackbar.show();
            }

        } else if (v.getId() == totalShipmentLayout.getId()) {
            if (GeneralUtils.isNetworkAvailable(getApplicationContext())) {
                GeneralUtils.ScaleAnimate(totalShipmentLayout, getApplicationContext());
                if (Objects.equals(totalShipmentTextView.getText().toString(), "0")) {
                    snackbar = Snackbar
                            .make(totalOpenBookingsLayout, "No Data Available", Snackbar.LENGTH_LONG);

                    snackbar.show();
                } else {
                    showDialog();
                    Intent intent = new Intent(NewDashboardActivity.this, DashboardInfoBoxClickActivity.class);
                    intent.putExtra("type", AppConstants.TOTAL_SHIPMENT);
                    dismissDialog();
                    mSwipeRefreshLayout.setRefreshing(false);
                    startActivity(intent);
                }
            } else {
                //Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                snackbar = Snackbar
                        .make(totalShipmentLayout, "Please Check Your Internet Connection", Snackbar.LENGTH_LONG);

                snackbar.show();
            }

        } else if (v.getId() == totalStuffingDoneLayout.getId()) {
            if (GeneralUtils.isNetworkAvailable(getApplicationContext())) {
                GeneralUtils.ScaleAnimate(totalStuffingDoneLayout, getApplicationContext());
                if (Objects.equals(stuffingDoneTextView.getText().toString(), "0")) {
                    snackbar = Snackbar
                            .make(totalOpenBookingsLayout, "No Data Available", Snackbar.LENGTH_LONG);

                    snackbar.show();
                } else {
                    showDialog();
                    Intent intent = new Intent(NewDashboardActivity.this, DashboardInfoBoxClickActivity.class);
                    intent.putExtra("type", AppConstants.STUFFING_DONE);
                    startActivity(intent);
                }
            } else {
                //Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                snackbar = Snackbar
                        .make(totalStuffingDoneLayout, "Please Check Your Internet Connection", Snackbar.LENGTH_LONG);

                snackbar.show();
            }

        } else if (v.getId() == totalIntransitLayout.getId()) {
            if (GeneralUtils.isNetworkAvailable(getApplicationContext())) {
                GeneralUtils.ScaleAnimate(totalIntransitLayout, getApplicationContext());
                if (Objects.equals(inTransitTextView.getText().toString(), "0")) {
                    snackbar = Snackbar
                            .make(totalOpenBookingsLayout, "No Data Available", Snackbar.LENGTH_LONG);

                    snackbar.show();
                } else {
                    showDialog();
                    Intent intent = new Intent(NewDashboardActivity.this, DashboardInfoBoxClickActivity.class);
                    intent.putExtra("type", AppConstants.IN_TRANSIT);
                    startActivity(intent);
                }
            } else {
                //Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                snackbar = Snackbar
                        .make(totalIntransitLayout, "Please Check Your Internet Connection", Snackbar.LENGTH_LONG);

                snackbar.show();
            }

        } else if (v.getId() == totalArrivedLayout.getId()) {
            if (GeneralUtils.isNetworkAvailable(getApplicationContext())) {
                GeneralUtils.ScaleAnimate(totalArrivedLayout, getApplicationContext());
                if (Objects.equals(deliveredTextView.getText().toString(), "0")) {
                    snackbar = Snackbar
                            .make(totalOpenBookingsLayout, "No Data Available", Snackbar.LENGTH_LONG);

                    snackbar.show();
                } else {
                    showDialog();
                    Intent intent = new Intent(NewDashboardActivity.this, DashboardInfoBoxClickActivity.class);
                    intent.putExtra("type", AppConstants.DELIVERED);
                    startActivity(intent);
                }
            } else {
                //Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                snackbar = Snackbar
                        .make(totalArrivedLayout, "Please Check Your Internet Connection", Snackbar.LENGTH_LONG);

                snackbar.show();
            }

        } else if (v.getId() == trackButton.getId()) {
            //showAddTrackingDialog();
            GeneralUtils.ScaleAnimate(trackButton, this);

            if (referenceNo.equalsIgnoreCase("HBL")) {
                // hide virtual keyboard
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                checkForValidationsBottomSheet();
            } else if (referenceNo.equalsIgnoreCase("PO") || referenceNo.equalsIgnoreCase("COMM")) {
                if (GeneralUtils.isNetworkAvailable(this)) {
                    checkPOValidations();
                } else {
                    //Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
                    snackbar = Snackbar
                            .make(userNameTextViewNav, "Please check internet connection", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }
            }


        } /*else if (v.getId() == menuIcon.getId()) {
           // ((TrackingActivity) this).clickEventSlide();
        } */ else if (v.getId() == fromDateEditText.getId()) {
            //Toast.makeText(this,"hi",Toast.LENGTH_SHORT).show();
            fromDatePickerDialog.show();
        } else if (v.getId() == toDateEditText.getId()) {
            toDatePickerDialog.show();
        }

    }

    private void checkForValidationsBottomSheet() {
        if (GeneralUtils.isNetworkAvailable(this)) {
            String value = valueEditText.getText().toString().toUpperCase();
            if (!value.isEmpty()) {
                callTrackingApi(value);
            } else {
                //Toast.makeText(this, "Please enter value.", Toast.LENGTH_SHORT).show();
                /*snackbar = Snackbar
                        .make(userNameTextViewNav, "Please enter HBL number", Snackbar.LENGTH_LONG);

                snackbar.show();*/
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.custom_toast,
                        (ViewGroup) findViewById(R.id.custom_toast_container));

                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Please Enter Value");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, 160);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }

        } else {
            Toast.makeText(this, getResources().getString(R.string.internet_connection_msg), Toast.LENGTH_SHORT).show();
            /*snackbar = Snackbar
                    .make(userNameTextViewNav, "Please check internet connection", Snackbar.LENGTH_LONG);

            snackbar.show();*/
        }

    }

    private void checkPOValidations() {
        String fromDate = fromDateEditText.getText().toString();
        String toDate = toDateEditText.getText().toString();
        String value = valueEditText.getText().toString().toUpperCase();

        //All empty check


        //Date checks
        if (!(value.isEmpty())) {
            if (referenceNo.equalsIgnoreCase("PO")) {
                callPOApi(value, fromDate, toDate);
            } else if (referenceNo.equalsIgnoreCase("COMM")) {
                callCommonInvoiceApi(value, fromDate, toDate);
            }
        } else if (!(fromDate.isEmpty()) && !(toDate.isEmpty())) {
            if (referenceNo.equalsIgnoreCase("PO")) {
                callPOApi(value, fromDate, toDate);
            } else if (referenceNo.equalsIgnoreCase("COMM")) {
                callCommonInvoiceApi(value, fromDate, toDate);
            }
        } else if (!(fromDate.isEmpty()) && toDate.isEmpty()) {
            /*Toast.makeText(this, "Please fill To Date!", Toast.LENGTH_LONG).show();*/
            snackbar = Snackbar
                    .make(userNameTextViewNav, "Fill To date", Snackbar.LENGTH_LONG);

            snackbar.show();
        } else if (fromDate.isEmpty() && !(toDate.isEmpty())) {
            /*Toast.makeText(this, "Please fill From Date!", Toast.LENGTH_LONG).show();*/
            snackbar = Snackbar
                    .make(userNameTextViewNav, "Fill From date", Snackbar.LENGTH_LONG);

            snackbar.show();
        } else if (value.isEmpty() && !(status.equals(""))) {//PO & status check
            //Toast.makeText(this, "Please fill reference Value!!", Toast.LENGTH_LONG).show();
            snackbar = Snackbar
                    .make(userNameTextViewNav, "Fill Reference Value", Snackbar.LENGTH_LONG);

            snackbar.show();
        } else if (status.equals("")) {
            //Toast.makeText(this, "Please fill some Value!!", Toast.LENGTH_LONG).show();
            snackbar = Snackbar
                    .make(userNameTextViewNav, "Fill Value!!", Snackbar.LENGTH_LONG);

            snackbar.show();
        } else {
            System.out.println("value" + value);
            if (referenceNo.equalsIgnoreCase("PO")) {
                callPOApi(value, fromDate, toDate);
            } else if (referenceNo.equalsIgnoreCase("COMM")) {
                callCommonInvoiceApi(value, fromDate, toDate);
            }
        }


        //  callPOApi(value, fromDate, toDate);
    }

    private void callPOApi(String value, String fromDate, String toDate) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("buyer_no", SharedPreferenceManager.getUserCode(this));
            jsonObject.put("poNo", value);
            jsonObject.put("fromDate", fromDate);
            jsonObject.put("toDate", toDate);
            jsonObject.put("statusType", status);
            System.out.println("PO::" + jsonObject);
            /*jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();*/
        showDialog();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.PO_DETAIL_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();
                mSwipeRefreshLayout.setRefreshing(false);

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("poTrackingResultData");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<POData> poDataArrayList = new ArrayList<>();
                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            POData poData = new POData();
                            poData.setPo_no(jsonArray.getJSONObject(i).getString("po_no"));
                            poData.setDocument_no(jsonArray.getJSONObject(i).getString("document_no"));
                            poData.setBl_no(jsonArray.getJSONObject(i).getString("bl_no"));
                            poData.setBooking_date(jsonArray.getJSONObject(i).getString("booking_date"));
                            poData.setBl_date(jsonArray.getJSONObject(i).getString("bl_date"));
                            poData.setShipper_name(jsonArray.getJSONObject(i).getString("shipper_name"));
                            poData.setPol_code(jsonArray.getJSONObject(i).getString("pol_code"));
                            poData.setPod_code(jsonArray.getJSONObject(i).getString("pod_code"));
                            poData.setGross_wt(jsonArray.getJSONObject(i).getString("gross_wt"));
                            poData.setCharge_wt(jsonArray.getJSONObject(i).getString("charge_wt"));
                            poData.setTot_volume(jsonArray.getJSONObject(i).getString("tot_volume"));
                            poData.setGr_date(jsonArray.getJSONObject(i).getString("gr_date"));
                            poData.setShipment_date(jsonArray.getJSONObject(i).getString("shipment_date"));
                            poData.setContainer_no(jsonArray.getJSONObject(i).getString("container_no"));
                            poData.setContainer_type(jsonArray.getJSONObject(i).getString("container_type"));
                            poData.setAta(jsonArray.getJSONObject(i).getString("ata"));
                            poData.setAtd(jsonArray.getJSONObject(i).getString("atd"));
                            poData.setEtd(jsonArray.getJSONObject(i).getString("etd"));
                            poData.setEta(jsonArray.getJSONObject(i).getString("eta"));
                            poData.setFeta(jsonArray.getJSONObject(i).getString("feta"));
                            poData.setFetd(jsonArray.getJSONObject(i).getString("fetd"));
                            poDataArrayList.add(poData);

                        }
                        arrayListManagerModel.setPoDataArrayList(poDataArrayList);
                        Intent intent = new Intent(NewDashboardActivity.this, POActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        /*Toast.makeText(TrackingActivity.this,
                                getResources().getString(R.string.no_data),
                                Toast.LENGTH_LONG).show();*/
                        snackbar = Snackbar
                                .make(userNameTextViewNav, "No Data", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("Error:" + e);
                    snackbar = Snackbar
                            .make(userNameTextViewNav, "Error While Fetching Data", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                snackbar = Snackbar
                        .make(userNameTextViewNav, "Error Fetching", Snackbar.LENGTH_LONG);

                snackbar.show();
                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void callCommonInvoiceApi(String value, String fromDate, String toDate) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("shipper_no", SharedPreferenceManager.getUserCode(this));
            jsonObject.put("commInvNo", value);
            jsonObject.put("fromDate", fromDate);
            jsonObject.put("toDate", toDate);
            jsonObject.put("statusType", status);
            /*jsonObject.put("shipper_no","1000000037");
            jsonObject.put("commInvNo", "TEST-01");
            jsonObject.put("fromDate","");
            jsonObject.put("toDate", "");
            jsonObject.put("statusType", "Order Booked");*/
            System.out.println("CO::" + jsonObject);
            /*jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*dialog = new ProgressDialog(NewDashboardActivity.this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();*/
        showDialog();

        RequestQueue requestQueue = Volley.newRequestQueue(NewDashboardActivity.this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.COMM_INVOICE_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();
                mSwipeRefreshLayout.setRefreshing(false);

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("poTrackingResultData");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<CommInvData> commInvDataArrayList = new ArrayList<>();
                    if (length > 0) {
                        for (int i = 0; i < length; i++) {
                            CommInvData commInvData = new CommInvData();
                            commInvData.setPoNo(jsonArray.getJSONObject(i).getString("po_no"));
                            commInvData.setDocumentNo(jsonArray.getJSONObject(i).getString("document_no"));
                            commInvData.setHblNo(jsonArray.getJSONObject(i).getString("bl_no"));
                            commInvData.setBookingDate(jsonArray.getJSONObject(i).getString("booking_date"));
                            commInvData.setShipperName(jsonArray.getJSONObject(i).getString("shipper_name"));
                            commInvData.setPol(jsonArray.getJSONObject(i).getString("pol_name"));
                            commInvData.setPod(jsonArray.getJSONObject(i).getString("pod_name"));
                            commInvData.setGrossWt(jsonArray.getJSONObject(i).getString("gross_wt"));
                            commInvData.setGoodReceivedDate(jsonArray.getJSONObject(i).getString("gr_date"));
                            commInvData.setShipmentDate(jsonArray.getJSONObject(i).getString("shipment_date"));
                            commInvData.setAta(jsonArray.getJSONObject(i).getString("ata"));
                            commInvData.setAtd(jsonArray.getJSONObject(i).getString("atd"));
                            commInvData.setEtd(jsonArray.getJSONObject(i).getString("etd"));
                            commInvData.setEta(jsonArray.getJSONObject(i).getString("eta"));
                            commInvData.setCommInvNo(jsonArray.getJSONObject(i).getString("comm_invoice_no"));
                            commInvData.setConsignee(jsonArray.getJSONObject(i).getString("buyer_name"));
                            commInvData.setCarrier(GeneralUtils.checkNullValue(jsonArray.getJSONObject(i).getString("carrier_name")));
                            commInvData.setDeliveryAgent(GeneralUtils.checkNullValue(jsonArray.getJSONObject(i).getString("agent_name")));
                            commInvData.setTotalPcs(jsonArray.getJSONObject(i).getString("tot_pcs"));
                            commInvData.setFeta(jsonArray.getJSONObject(i).getString("feta"));
                            commInvData.setFetd(jsonArray.getJSONObject(i).getString("fetd"));
                            commInvDataArrayList.add(commInvData);

                        }
                        arrayListManagerModel.setCommInvDataArrayList(commInvDataArrayList);
                        Intent intent = new Intent(NewDashboardActivity.this, CommInvTrackingActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                    } else {
                        snackbar = Snackbar
                                .make(userNameTextViewNav, "No Data", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    snackbar = Snackbar
                            .make(userNameTextViewNav, "Error Fetching", Snackbar.LENGTH_LONG);

                    snackbar.show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                snackbar = Snackbar
                        .make(userNameTextViewNav, "Error Fetching", Snackbar.LENGTH_LONG);

                snackbar.show();
                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    private void callTrackingApi(String value) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer", SharedPreferenceManager.getUserName(this));
            jsonObject.put("salesOrg", SharedPreferenceManager.getSalesOrgCode(this));
            jsonObject.put("distChan", SharedPreferenceManager.getDistChannel(this));
            jsonObject.put("division", SharedPreferenceManager.getDivision(this));
            jsonObject.put("erdat1", "");
            jsonObject.put("erdat2", "");
            if (Objects.equals(SharedPreferenceManager.getDivision(this), "SE")) {
                jsonObject.put("action", "01");
            } else {
                jsonObject.put("action", "03");
            }

            jsonObject.put("doc_no", value);

            System.out.println("track--> " + jsonObject);
            System.out.println("URL--> " + ApiUrls.TRACKING_URL);
            /*jsonObject.put("blNo", "BOM000483");
            jsonObject.put("salesOrg", "BOM2");
            jsonObject.put("distributionChannel", "EX");
            jsonObject.put("division", "SE");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.please_wait_text));
        dialog.show();*/
        showDialog();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                ApiUrls.TRACKING_URL, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();
                mSwipeRefreshLayout.setRefreshing(false);

                try {
                    // Parsing json object response
                    System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("myshipmentTrackHeader");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<TrackingDataNew> trackingDataNewArrayList = new ArrayList<>();
                    if (length > 0) {
                        System.out.println("size: --> " + length);
                        for (int i = 0; i < length; i++) {
                            TrackingDataNew trackingDataNew = new TrackingDataNew();
                            trackingDataNew.setBl_no(jsonArray.getJSONObject(i).getString("bl_no"));
                            trackingDataNew.setBl_bt(jsonArray.getJSONObject(i).getString("bl_bt"));
                            trackingDataNew.setShipper(jsonArray.getJSONObject(i).getString("shipper"));
                            trackingDataNew.setBuyer(jsonArray.getJSONObject(i).getString("buyer"));
                            trackingDataNew.setPol(jsonArray.getJSONObject(i).getString("pol"));
                            trackingDataNew.setPod(jsonArray.getJSONObject(i).getString("pod"));
                            trackingDataNew.setPlod(jsonArray.getJSONObject(i).getString("plod"));
                            trackingDataNew.setFrt_mode_ds(jsonArray.getJSONObject(i).getString("frt_mode_ds"));
                            trackingDataNew.setTot_qty(jsonArray.getJSONObject(i).getString("tot_qty"));
                            trackingDataNew.setVolume(jsonArray.getJSONObject(i).getString("volume"));
                            trackingDataNew.setGrs_wt(jsonArray.getJSONObject(i).getString("grs_wt"));
                            trackingDataNew.setComm_inv(jsonArray.getJSONObject(i).getString("comm_inv"));
                            trackingDataNew.setComm_inv_dt(jsonArray.getJSONObject(i).getString("comm_inv_dt"));
                            trackingDataNew.setPor(jsonArray.getJSONObject(i).getString("por"));
                            trackingDataNew.setGr_dt(jsonArray.getJSONObject(i).getString("gr_dt"));
                            trackingDataNew.setMbl_no(jsonArray.getJSONObject(i).getString("mbl_no"));
                            trackingDataNew.setCarrier(jsonArray.getJSONObject(i).getString("carrier"));
                            trackingDataNew.setStuff_dt(jsonArray.getJSONObject(i).getString("stuff_dt"));
                            trackingDataNew.setDep_dt(jsonArray.getJSONObject(i).getString("dep_dt"));
                            trackingDataNew.setLoad_dt(jsonArray.getJSONObject(i).getString("load_dt"));
                            trackingDataNew.setEta_dt(jsonArray.getJSONObject(i).getString("eta_dt"));
                            trackingDataNew.setAgent(jsonArray.getJSONObject(i).getString("agent"));
                            trackingDataNewArrayList.add(trackingDataNew);

                            eta = jsonArray.getJSONObject(i).getString("eta_dt");
                            etd = jsonArray.getJSONObject(i).getString("dep_dt");
                            pol = jsonArray.getJSONObject(i).getString("pol");
                            pod = jsonArray.getJSONObject(i).getString("pod");

                        }

                        SharedPreferenceManager.saveEta(eta, getApplicationContext());
                        SharedPreferenceManager.saveEtd(etd, getApplicationContext());
                        SharedPreferenceManager.savePol(pol, getApplicationContext());
                        SharedPreferenceManager.savePod(pod, getApplicationContext());


                        arrayListManagerModel.setTrackingDataNewArrayList(trackingDataNewArrayList);
                        if (trackingDataNewArrayList.size() > 0) {
                            Intent intent = new Intent(NewDashboardActivity.this, MilestoneTrackingActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(NewDashboardActivity.this, "No data available", Toast.LENGTH_SHORT).show();
                            /*snackbar = Snackbar
                                    .make(userNameTextViewNav, "No data available", Snackbar.LENGTH_LONG);

                            snackbar.show();*/
                        }
                    } else {
                        Toast.makeText(NewDashboardActivity.this,
                                getResources().getString(R.string.error_fetching),
                                Toast.LENGTH_LONG).show();
                        /*snackbar = Snackbar
                                .make(userNameTextViewNav, "Error While Fetching Data", Snackbar.LENGTH_LONG);

                        snackbar.show();*/
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.custom_toast_container));

                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("Wrong HBL Number");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, 160);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

                    /*Toast.makeText(NewDashboardActivity.this,
                            "Wrong HBL Number or Error While Fetching Data",
                            Toast.LENGTH_LONG).show();*/
                    /*snackbar = Snackbar
                            .make(userNameTextViewNav, "Wrong HBL Number or Error While Fetching Data", Snackbar.LENGTH_LONG);

                    snackbar.show();*/
                }
                try {

                    // Parsing json object response
                    //System.out.println("response: " + response);
                    JSONArray jsonArray = response.getJSONArray("myshipmentTrackItem");
                    int length = jsonArray.length();
                    ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
                    ArrayList<ItemData> itemDataArrayList = new ArrayList<>();
                    if (length > 0) {
                        System.out.println("size item: --> " + length);
                        for (int i = 0; i < length; i++) {
                            ItemData itemData = new ItemData();
                            itemData.setPo_no(jsonArray.getJSONObject(i).getString("po_no"));
                            itemData.setCommodity(jsonArray.getJSONObject(i).getString("commodity"));
                            itemData.setStyle(jsonArray.getJSONObject(i).getString("style"));
                            itemData.setSize(jsonArray.getJSONObject(i).getString("size"));
                            itemData.setSku(jsonArray.getJSONObject(i).getString("sku"));
                            itemData.setArt_no(jsonArray.getJSONObject(i).getString("art_no"));
                            itemData.setColor(jsonArray.getJSONObject(i).getString("color"));
                            itemData.setItem_pcs(jsonArray.getJSONObject(i).getString("item_pcs"));
                            itemData.setCart_sno(jsonArray.getJSONObject(i).getString("item_qty"));
                            itemData.setItem_vol(jsonArray.getJSONObject(i).getString("item_vol"));
                            itemData.setItem_grwt(jsonArray.getJSONObject(i).getString("item_grwt"));

                            itemDataArrayList.add(itemData);

                        }

                        arrayListManagerModel.setItemDataArrayList(itemDataArrayList);
                        if (itemDataArrayList.size() > 0) {
                            Intent intent = new Intent(NewDashboardActivity.this, MilestoneTrackingActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            snackbar = Snackbar
                                    .make(userNameTextViewNav, "No Data", Snackbar.LENGTH_LONG);

                            snackbar.show();
                        }
                    } else {
                        snackbar = Snackbar
                                .make(userNameTextViewNav, "Error Fetching", Snackbar.LENGTH_LONG);

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                snackbar = Snackbar
                        .make(valueEditText, "Error Fetching Data", Snackbar.LENGTH_LONG);

                snackbar.show();
                /*if (dialog.isShowing()) {
                    dialog.dismiss();
                }*/
                dismissDialog();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }

    public void datePickerCallback() {
        datePickerCallBack = new DatePickerCallBack() {
            @Override
            public void SelectedDate(String fromDate, String todate) {
                SharedPreferenceManager.setToDate(todate, getApplicationContext());
                SharedPreferenceManager.setFromDate(fromDate, getApplicationContext());
                //callDashBoardApi();
            }
        };
    }

    private void ShowlogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(NewDashboardActivity.this);
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setMessage(getResources().getString(R.string.do_you_want_to_logout));
        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferenceManager.clearCredentials(NewDashboardActivity.this);
                UserDetails.logoutUser(NewDashboardActivity.this);
                Intent intent = new Intent(NewDashboardActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                RegisterActivities.removeAllActivities();
            }
        });
        builder.create();
        builder.setCancelable(false);
        builder.show();
    }
}

