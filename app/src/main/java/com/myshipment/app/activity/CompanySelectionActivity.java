package com.myshipment.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;


import com.google.firebase.analytics.FirebaseAnalytics;
import com.myshipment.R;
import com.myshipment.app.Utility.SharedPreferenceManager;
import com.myshipment.app.adapters.CompanySelectionAdapter;
import com.myshipment.app.beans.ArrayListManagerModel;
import com.myshipment.app.beans.FilteredSalesOrgList;
import com.myshipment.app.beans.LoginData;
import com.myshipment.app.beans.SalesOrgList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Akshay Thapliyal on 23-06-2016.
 */
public class CompanySelectionActivity extends Activity implements View.OnClickListener {

    private Spinner companyListSpinner1;
    private Button continueBtn;
    private String loginResponse;
    private RadioGroup distributionRadioGroup, divisionRadioGroup;
    private RadioButton exportRadioButton, importRadioButton, chaRadioButton, airRadioButton, seaRadioButton;
    ArrayList<String> companyItems = new ArrayList<>();
    private ArrayList<SalesOrgList> salesOrgListArrayList;
    private ArrayList<FilteredSalesOrgList> filteredSalesOrgListArrayList;
    private String distChannel, division, userCode, accGroup, userName;
    Snackbar snackbar;
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_sel);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        init();
        setListeners();
        userCode = SharedPreferenceManager.getUserCode(this);//getIntent().getStringExtra("usercode");
        accGroup = SharedPreferenceManager.getAccGroup(this);//getIntent().getStringExtra("accGroup");
        userName = SharedPreferenceManager.getUserName(this);//getIntent().getStringExtra("userName");
        //System.out.println("------ag---> "+accGroup+" uc--> "+userCode+" un--->"+userName);
        getIntentData();
        parseLoginResponse();
        filterSalesOrganList();
        getCompanyItems();

       /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item_layout, companyItems);
        companyListSpinner1.setAdapter(adapter);
        companyListSpinner1.setSelection(0);
*/
        CompanySelectionAdapter comparisonDataListAdapter = new CompanySelectionAdapter(CompanySelectionActivity.this, companyItems);
        companyListSpinner1.setAdapter(comparisonDataListAdapter);
        companyListSpinner1.setSelection(0, false);

    }

    private void init() {
        companyListSpinner1 = (Spinner) findViewById(R.id.spinner);
        continueBtn = (Button) findViewById(R.id.continueBtn);
        exportRadioButton = (RadioButton) findViewById(R.id.exportRadioButton);
        importRadioButton = (RadioButton) findViewById(R.id.importRadioButton);
        chaRadioButton = (RadioButton) findViewById(R.id.chaRadioButton);
        airRadioButton = (RadioButton) findViewById(R.id.airRadioButton);
        seaRadioButton = (RadioButton) findViewById(R.id.seaRadioButton);
        distributionRadioGroup = (RadioGroup) findViewById(R.id.distributionRadioGroup);
        divisionRadioGroup = (RadioGroup) findViewById(R.id.divisionRadioGroup);
    }

    private void setListeners() {
        continueBtn.setOnClickListener(this);
        distributionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                //distChannel = radioButton.getText().toString();
                if (checkedId == exportRadioButton.getId()) {
                    distChannel = "EX";

                } else if (checkedId == importRadioButton.getId()) {
                    distChannel = "IM";

                } else if (checkedId == chaRadioButton.getId()) {
                    distChannel = "CHA";
                }

            }
        });

        divisionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                //division = radioButton.getText().toString();
                if (checkedId == airRadioButton.getId()) {
                    division = "AR";
                } else if (checkedId == seaRadioButton.getId()) {
                    division = "SE";
                }
            }
        });

        companyListSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    showDistributionData(position - 1);
                    showDivisionData(position - 1);
                } else {
                    exportRadioButton.setVisibility(View.GONE);
                    importRadioButton.setVisibility(View.GONE);
                    chaRadioButton.setVisibility(View.GONE);
                    airRadioButton.setVisibility(View.GONE);
                    seaRadioButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
      /*  companyListSpinner1.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    showDistributionData(position - 1);
                    showDivisionData(position - 1);
                } else {
                    exportRadioButton.setVisibility(View.GONE);
                    importRadioButton.setVisibility(View.GONE);
                    chaRadioButton.setVisibility(View.GONE);
                    airRadioButton.setVisibility(View.GONE);
                    seaRadioButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
    }

    private void showDistributionData(int position) {
        ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
        int noOfDistChannels = arrayListManagerModel.getLoginDataArrayList().get(position).getDistChannel().size();
        exportRadioButton.setVisibility(View.GONE);
        importRadioButton.setVisibility(View.GONE);
        chaRadioButton.setVisibility(View.GONE);
        for (int i = 0; i < noOfDistChannels; i++) {
            if (arrayListManagerModel.getLoginDataArrayList().get(position).getDistChannel().get(i).equalsIgnoreCase("EX")) {
                exportRadioButton.setVisibility(View.VISIBLE);
                exportRadioButton.setChecked(true);
            } else if (arrayListManagerModel.getLoginDataArrayList().get(position).getDistChannel().get(i).equalsIgnoreCase("IM")) {
                importRadioButton.setVisibility(View.VISIBLE);
                importRadioButton.setChecked(true);
            } else if (arrayListManagerModel.getLoginDataArrayList().get(position).getDistChannel().get(i).equalsIgnoreCase("CHA")) {
                chaRadioButton.setVisibility(View.VISIBLE);
                chaRadioButton.setChecked(true);
            }
        }
    }

    private void showDivisionData(int position) {
        ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
        int noOfDivChannels = arrayListManagerModel.getLoginDataArrayList().get(position).getDivision().size();

        airRadioButton.setVisibility(View.GONE);
        seaRadioButton.setVisibility(View.GONE);
        for (int i = 0; i < noOfDivChannels; i++) {
            System.out.println("Div:: " + arrayListManagerModel.getLoginDataArrayList().get(position).getDivision().get(i));
            if (arrayListManagerModel.getLoginDataArrayList().get(position).getDivision().get(i).equalsIgnoreCase("AR")) {
                airRadioButton.setVisibility(View.VISIBLE);
                airRadioButton.setChecked(true);
            } else if (arrayListManagerModel.getLoginDataArrayList().get(position).getDivision().get(i).equalsIgnoreCase("SE")) {
                seaRadioButton.setVisibility(View.VISIBLE);
                seaRadioButton.setChecked(true);
            }
        }
    }

    private void getIntentData() {
        loginResponse = SharedPreferenceManager.getLoginRespone(this);//getIntent().getStringExtra("loginResponse");
        //System.out.println("login res:::: ---> "+loginResponse);
    }

    private void parseLoginResponse() {

        if (loginResponse != null && !loginResponse.isEmpty()) {

            try {
                JSONObject jsonObject = new JSONObject(loginResponse);
                JSONArray jsonArray = jsonObject.getJSONArray("salesorgList");
                salesOrgListArrayList = new ArrayList<SalesOrgList>();
                salesOrgListArrayList.clear();
                int arrayLength = jsonArray.length();
                for (int i = 0; i < arrayLength; i++) {
                    SalesOrgList salesOrgList = new SalesOrgList();
                    salesOrgList.setSalesOrg(jsonArray.getJSONObject(i).getString("salesOrg"));
                    salesOrgList.setSalesOrgName(jsonArray.getJSONObject(i).getString("salesOrgText"));
                    salesOrgList.setDistChannel(jsonArray.getJSONObject(i).getString("distrChan"));
                    salesOrgList.setDivision(jsonArray.getJSONObject(i).getString("division"));
                    salesOrgListArrayList.add(salesOrgList);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void getCompanyItems() {
        //get company names
        companyItems.add("Select Company");
        int length = ArrayListManagerModel.getInstance().getLoginDataArrayList().size();
        for (int i = 0; i < length; i++) {
            companyItems.add(ArrayListManagerModel.getInstance().getLoginDataArrayList().get(i).getSalesOrg());
        }
    }

    private void filterSalesOrganList() {

        int length = salesOrgListArrayList.size();

        if (length > 0) {
            ArrayListManagerModel arrayListManagerModel = ArrayListManagerModel.getInstance();
            LoginData loginData = new LoginData();
            ArrayList<LoginData> loginDataArrayList = new ArrayList<>();
            ArrayList<String> distChannel = new ArrayList<>();
            ArrayList<String> division = new ArrayList<>();

            int count = 0;
            for (int i = 0; i < length; i++) {
                count = 0;
                distChannel = new ArrayList<>();
                division = new ArrayList<>();
                loginData = new LoginData();
                for (int j = i + 1; j < length; j++) {
                    if (salesOrgListArrayList.get(i).getSalesOrgName().equalsIgnoreCase(salesOrgListArrayList.get(j).getSalesOrgName())) {
                        ++count;
                        getAllDivision(division, i, j);
                        getAllDistChannel(distChannel, i, j);
                        salesOrgListArrayList.remove(j);
                        j = i;
                        --length;
                    }
                }
                if (count == 0) {
                    loginData.setSalesOrg(salesOrgListArrayList.get(i).getSalesOrgName());
                    loginData.setSalesOrgCode(salesOrgListArrayList.get(i).getSalesOrg());
                    distChannel.add(salesOrgListArrayList.get(i).getDistChannel());
                    division.add(salesOrgListArrayList.get(i).getDivision());
                    loginData.setDistChannel(distChannel);
                    loginData.setDivision(division);
                    loginDataArrayList.add(loginData);
                } else {
                    //add the above data
                    loginData.setSalesOrg(salesOrgListArrayList.get(i).getSalesOrgName());
                    loginData.setSalesOrgCode(salesOrgListArrayList.get(i).getSalesOrg());
                    loginData.setDivision(division);
                    loginData.setDistChannel(distChannel);
                    loginDataArrayList.add(loginData);
                }
            }
            arrayListManagerModel.setLoginDataArrayList(loginDataArrayList);

        }
    }

    private void getAllDivision(ArrayList<String> division, int i, int j) {

        if (salesOrgListArrayList.get(i).getDivision().equalsIgnoreCase(salesOrgListArrayList.get(j).getDivision())) {
            division.add(salesOrgListArrayList.get(i).getDivision());
        } else {
            division.add(salesOrgListArrayList.get(i).getDivision());
            division.add(salesOrgListArrayList.get(j).getDivision());

        }
    }

    private void getAllDistChannel(ArrayList<String> distChannel, int i, int j) {

        if (salesOrgListArrayList.get(i).getDistChannel().equalsIgnoreCase(salesOrgListArrayList.get(j).getDistChannel())) {
            distChannel.add(salesOrgListArrayList.get(i).getDistChannel());

        } else {
            distChannel.add(salesOrgListArrayList.get(i).getDistChannel());
            distChannel.add(salesOrgListArrayList.get(j).getDistChannel());
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == continueBtn.getId()) {
            saveLoginData();
        }
    }

    private void saveLoginData() {
        String salesOrg = companyListSpinner1.getSelectedItem().toString();
        String salesOrgCode = getSalesOrgCode(salesOrg);
        if (division != null && distChannel != null && salesOrg != null) {
            SharedPreferenceManager.saveSalesOrg(salesOrg, this);
            SharedPreferenceManager.saveSalesOrgCode(salesOrgCode, this);
            SharedPreferenceManager.saveDistChannel(distChannel, this);
            SharedPreferenceManager.saveDivision(division, this);

            //Intent intent = new Intent(this, HomeDashBoardActivity.class);
            Intent intent = new Intent(this, NewDashboardActivity.class);
            SharedPreferenceManager.saveUserCode(userCode, CompanySelectionActivity.this);
            SharedPreferenceManager.saveAccGroup(accGroup, CompanySelectionActivity.this);
            SharedPreferenceManager.saveUserName(userName, CompanySelectionActivity.this);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_left, R.anim.slide_right);
            finish();
        } else {
            //Toast.makeText(getApplicationContext(), "Please select values.", Toast.LENGTH_SHORT).show();
            snackbar = Snackbar
                    .make(divisionRadioGroup, "Please Select a Company", Snackbar.LENGTH_LONG);

            snackbar.show();
        }
    }

    private String getSalesOrgCode(String salesOrgName) {

        int length = salesOrgListArrayList.size();
        String salesOrgCode = "";
        for (int i = 0; i < length; i++) {
            if (salesOrgListArrayList.get(i).getSalesOrgName().equalsIgnoreCase(salesOrgName)) {
                salesOrgCode = salesOrgListArrayList.get(i).getSalesOrg();
            }
        }

        return salesOrgCode;
    }
}
