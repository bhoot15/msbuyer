package com.myshipment.app.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.myshipment.R;
import com.myshipment.app.Utility.SharedPreferenceManager;
import com.myshipment.app.adapters.DeliveredAdapter;
import com.myshipment.app.adapters.GoodsHandoverAdapter;
import com.myshipment.app.adapters.InTransitAdapter;
import com.myshipment.app.adapters.OpenBookingsAdapter;
import com.myshipment.app.adapters.StuffDoneAdapter;
import com.myshipment.app.adapters.TotalShipmentAdapter;
import com.myshipment.app.beans.DashboardInfoBox;
import com.myshipment.app.beans.TrackingDataNew;

import java.util.List;
import java.util.Objects;

public class DashboardInfoBoxClickActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private ImageView backButton;
    Snackbar snackbar;
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_shipment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView tv = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        setSupportActionBar(toolbar);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        if (getIntent().getStringExtra("type").equalsIgnoreCase("totalShipment")) {
            tv.setText("Total Bookings");
        } else if (getIntent().getStringExtra("type").equalsIgnoreCase("openOrder")) {
            tv.setText("Open Bookings");
        } else if (getIntent().getStringExtra("type").equalsIgnoreCase("goodsReceived")) {
            tv.setText("Goods Received");
        } else if (getIntent().getStringExtra("type").equalsIgnoreCase("stuffingDone")) {
            tv.setText("Stuffing Done");
        } else if (getIntent().getStringExtra("type").equalsIgnoreCase("inTransit")) {
            tv.setText("In Transit (*EST)");
        } else if (getIntent().getStringExtra("type").equalsIgnoreCase("delivered")) {
            tv.setText("Arrived (*EST)");
        }


        initViews();
        initRecyclerView();
        initListener();
    }

    public void initViews() {

        recyclerView = (RecyclerView) findViewById(R.id.totalSipmentRecyclerView);
        backButton = (ImageView) findViewById(R.id.menuIcon);

    }

    public void initListener() {
        backButton.setOnClickListener(this);
    }

    public void initRecyclerView() {

        String dataType = getIntent().getStringExtra("type");

        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setNestedScrollingEnabled(false);
        fetchData(dataType);
    }

    private void fetchData(String dataType) {

        String trackingData = SharedPreferenceManager.getTrackRespone(this);
        DashboardInfoBox dashboardInfoBox = new Gson().fromJson(trackingData, DashboardInfoBox.class);


        List<TrackingDataNew> trackingDataNewList;

        if (Objects.equals(dataType, "totalShipment")) {

            trackingDataNewList = dashboardInfoBox.getTotalShipmentList();
            TotalShipmentAdapter totalShipmentAdapter = new TotalShipmentAdapter(this, trackingDataNewList);
            recyclerView.setAdapter(totalShipmentAdapter);
        } else if (Objects.equals(dataType, "openOrder")) {
            trackingDataNewList = dashboardInfoBox.getOpenOrderList();
            OpenBookingsAdapter openBookingsAdapter = new OpenBookingsAdapter(this, trackingDataNewList);
            recyclerView.setAdapter(openBookingsAdapter);
        } else if (Objects.equals(dataType, "goodsReceived")) {
            trackingDataNewList = dashboardInfoBox.getGoodsReceivedList();
            GoodsHandoverAdapter goodsHandoverAdapter = new GoodsHandoverAdapter(this, trackingDataNewList);
            recyclerView.setAdapter(goodsHandoverAdapter);
        } else if (Objects.equals(dataType, "stuffingDone")) {
            trackingDataNewList = dashboardInfoBox.getStuffingDoneList();
            StuffDoneAdapter stuffDoneAdapter = new StuffDoneAdapter(this, trackingDataNewList);
            recyclerView.setAdapter(stuffDoneAdapter);
        } else if (Objects.equals(dataType, "inTransit")) {
            trackingDataNewList = dashboardInfoBox.getInTransitList();
            InTransitAdapter inTransitAdapter = new InTransitAdapter(this, trackingDataNewList);
            recyclerView.setAdapter(inTransitAdapter);
        } else if (Objects.equals(dataType, "delivered")) {
            trackingDataNewList = dashboardInfoBox.getDeliveredList();
            if (trackingDataNewList.size() != 0) {
                DeliveredAdapter deliveredAdapter = new DeliveredAdapter(this, trackingDataNewList);
                recyclerView.setAdapter(deliveredAdapter);
            } else {
                snackbar = Snackbar
                        .make(recyclerView, "No Data Found", Snackbar.LENGTH_LONG);

                snackbar.show();
            }
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menuIcon:
                /*Intent intent = new Intent(DashboardInfoBoxClickActivity.this, NewDashboardActivity.class);
                startActivity(intent);*/
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        /*Intent intent = new Intent(DashboardInfoBoxClickActivity.this, NewDashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/
        finish();
    }
}
