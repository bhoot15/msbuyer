package com.myshipment.app.uicomponent;

import com.myshipment.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


public class ArrowAnimatedImageView extends ImageView {

	private Context context_;

	public ArrowAnimatedImageView(Context context) {
		super(context);
		this.context_ = context;
		zoomein();
	}

	public ArrowAnimatedImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context_ = context;
		zoomein();
	}

	private void zoomein() {
		Animation zoomin = AnimationUtils.loadAnimation(context_, R.anim.shake_anim);
		clearAnimation();
		startAnimation(zoomin);
		zoomin.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation arg0) {
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				zoomein();
			}
		});
	}

}
