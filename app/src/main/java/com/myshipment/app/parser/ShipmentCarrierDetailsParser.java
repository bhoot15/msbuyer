package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.Util.ShipmentClickState;
import com.myshipment.app.databasehelper.PieChartCarrierDetailsTableHelper;
import com.myshipment.app.model.CarrierDetail;

public class ShipmentCarrierDetailsParser {

	private Context mContext;

	private String fromdate = "";
	private String todate = "";
	private String isOrigin = "";
	private String carriername = "";
	private String ilncode = "";
	private String Pol = "";
	private String Pod = "";

	private PieChartCarrierDetailsTableHelper mCarrierDetailsTableHelper = null;

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";

	/* For Total Shipment Flow */
	private String CARRIERDETAILS = "CarrierDetails";
	private String CARRIER_NAME_FORCARRIERDETAILS = "Carrier Name";
	private String CONTAINER_NUMBER = "Container Number";
	private String POL_POD = "POL-POD";
	private String INITIAL = "Initial";
	private String REAL = "Real";
	private String DELAY_DAYS = "Delay Days";

	private String responsecode = "";

	public void parse(Context context, String createUrl, ShipmentClickState mShipmentClickState, String fromdate_, String todate_, String isOrigin_, String carriername_, String ILNCode_, String polCode, String podCode) {
		this.mContext = context;

		this.fromdate = fromdate_;
		this.todate = todate_;
		this.isOrigin = isOrigin_;
		this.carriername = carriername_;
		this.ilncode = ILNCode_;
		this.Pol = polCode;
		this.Pod = podCode;
		this.mCarrierDetailsTableHelper = PieChartCarrierDetailsTableHelper.getInstance(mContext);

		FlowParserAsync mFlowParserAsync = new FlowParserAsync(mContext, createUrl, mShipmentClickState);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mFlowParserAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mFlowParserAsync.execute();
		}
	}

	public String[] getFieldnameOfCarrierdetails() {
		String[] fields = new String[6];
		fields[0] = CARRIER_NAME_FORCARRIERDETAILS;
		fields[1] = CONTAINER_NUMBER;
		fields[2] = POL_POD;
		fields[3] = INITIAL;
		fields[4] = REAL;
		fields[5] = DELAY_DAYS;
		return fields;
	}

	private class FlowParserAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;
		private ArrayList<CarrierDetail> CarrierDetails;
		private String createUrl_;
		private ShipmentClickState State;
		private boolean isTimeOut = false;

		public FlowParserAsync(Context mContext, String createUrl, ShipmentClickState mShipmentClickState) {
			this.context = mContext;
			this.createUrl_ = createUrl;
			this.State = mShipmentClickState;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				String[] responsedata = Util.sendGet(createUrl_);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;

				if (authenticationJson != null && !authenticationJson.equals("")) {
					JSONObject jsObg = new JSONObject(authenticationJson);
					responsecode = jsObg.getString(RESPONSECODE);
					if (responsecode.equals("200")) {
						if (State == ShipmentClickState.Shipments) {
							CarrierDetails = ShipmentCarrierDetailsData(jsObg);
						} else if (State == ShipmentClickState.Delayed) {
							CarrierDetails = DelayCarrierDetailsData(jsObg);
						} else if (State == ShipmentClickState.InTransit) {
							CarrierDetails = InTransitCarrierDetailsData(jsObg);
						} else if (State == ShipmentClickState.PotentialDelay) {
							CarrierDetails = PotentialCarrierDetailsData(jsObg);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);
			if (isTimeOut) {
				if (carierdetailsparserinterface != null) {
					carierdetailsparserinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (carierdetailsparserinterface != null) {
						carierdetailsparserinterface.ONSuccess(CarrierDetails);
					}
				} else if (responsecode.equals("1002")) {
					if (carierdetailsparserinterface != null) {
						carierdetailsparserinterface.NoData();
					}
				} else {
					if (carierdetailsparserinterface != null) {
						carierdetailsparserinterface.OnError();
					}
				}
			}
		}
	}

	/* For Total Shipment CarrierDetail */
	private ArrayList<CarrierDetail> ShipmentCarrierDetailsData(JSONObject jsObg) {
		ArrayList<CarrierDetail> carDetailsList = null;
		try {
			JSONArray carrierJodnarray = jsObg.getJSONArray(CARRIERDETAILS);
			if (carrierJodnarray != null && carrierJodnarray.length() >= 1) {
				carDetailsList = new ArrayList<CarrierDetail>();
				for (int i = 0; i < carrierJodnarray.length(); i++) {

					JSONObject carrierDetailjsonObj = carrierJodnarray.getJSONObject(i);
					CarrierDetail mCarrierDetail = new CarrierDetail();

					mCarrierDetail.setCarrier_Name(Util.getJSONKeyvalue(carrierDetailjsonObj, CARRIER_NAME_FORCARRIERDETAILS));
					mCarrierDetail.setContainer_Number(Util.getJSONKeyvalue(carrierDetailjsonObj, CONTAINER_NUMBER));
					mCarrierDetail.setPOL_POD(Util.getJSONKeyvalue(carrierDetailjsonObj, POL_POD));
					mCarrierDetail.setInitial(Util.getJSONKeyvalue(carrierDetailjsonObj, INITIAL));
					mCarrierDetail.setReal(Util.getJSONKeyvalue(carrierDetailjsonObj, REAL));
					mCarrierDetail.setDelay_Days(Util.getJSONKeyvalue(carrierDetailjsonObj, DELAY_DAYS));

					mCarrierDetail.setFromdate(fromdate);
					mCarrierDetail.setTodate(todate);
					mCarrierDetail.setType("");
					mCarrierDetail.setIsOrigin(isOrigin);
					mCarrierDetail.setCarriername(carriername);
					mCarrierDetail.setILNCode(ilncode);

					/** Database insertion **/
					mCarrierDetailsTableHelper.storeGraphOVData(mCarrierDetail);
					/***************************/

					mCarrierDetailsTableHelper.storeGraphOVData(mCarrierDetail);

					carDetailsList.add(mCarrierDetail);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carDetailsList;
	}

	/* For Total Shipment CarrierDetail */

	/* For Delay Shipment CarrierDetail */
	private ArrayList<CarrierDetail> DelayCarrierDetailsData(JSONObject jsObg) {
		ArrayList<CarrierDetail> carDetailsList = null;
		try {
			JSONArray carrierJodnarray = jsObg.getJSONArray(CARRIERDETAILS);
			if (carrierJodnarray != null && carrierJodnarray.length() >= 1) {
				carDetailsList = new ArrayList<CarrierDetail>();
				for (int i = 0; i < carrierJodnarray.length(); i++) {

					JSONObject carrierDetailjsonObj = carrierJodnarray.getJSONObject(i);
					CarrierDetail mCarrierDetail = new CarrierDetail();

					mCarrierDetail.setCarrier_Name(Util.getJSONKeyvalue(carrierDetailjsonObj, CARRIER_NAME_FORCARRIERDETAILS));
					mCarrierDetail.setContainer_Number(Util.getJSONKeyvalue(carrierDetailjsonObj, CONTAINER_NUMBER));
					mCarrierDetail.setPOL_POD(Util.getJSONKeyvalue(carrierDetailjsonObj, POL_POD));
					mCarrierDetail.setInitial(Util.getJSONKeyvalue(carrierDetailjsonObj, INITIAL));
					mCarrierDetail.setReal(Util.getJSONKeyvalue(carrierDetailjsonObj, REAL));
					mCarrierDetail.setDelay_Days(Util.getJSONKeyvalue(carrierDetailjsonObj, DELAY_DAYS));

					mCarrierDetail.setFromdate(fromdate);
					mCarrierDetail.setTodate(todate);
					mCarrierDetail.setType("");
					mCarrierDetail.setIsOrigin(isOrigin);
					mCarrierDetail.setCarriername(carriername);
					mCarrierDetail.setILNCode(ilncode);

					/** Database insertion **/
					mCarrierDetailsTableHelper.storeGraphOVData(mCarrierDetail);
					/***************************/

					carDetailsList.add(mCarrierDetail);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carDetailsList;
	}

	/* For Delay Shipment CarrierDetail */

	/* For InTransit Shipment CarrierDetail */
	private ArrayList<CarrierDetail> InTransitCarrierDetailsData(JSONObject jsObg) {
		ArrayList<CarrierDetail> carDetailsList = null;
		try {
			JSONArray carrierJodnarray = jsObg.getJSONArray(CARRIERDETAILS);
			if (carrierJodnarray != null && carrierJodnarray.length() >= 1) {
				carDetailsList = new ArrayList<CarrierDetail>();
				for (int i = 0; i < carrierJodnarray.length(); i++) {

					JSONObject carrierDetailjsonObj = carrierJodnarray.getJSONObject(i);
					CarrierDetail mCarrierDetail = new CarrierDetail();

					mCarrierDetail.setCarrier_Name(Util.getJSONKeyvalue(carrierDetailjsonObj, CARRIER_NAME_FORCARRIERDETAILS));
					mCarrierDetail.setContainer_Number(Util.getJSONKeyvalue(carrierDetailjsonObj, CONTAINER_NUMBER));
					mCarrierDetail.setPOL_POD(Util.getJSONKeyvalue(carrierDetailjsonObj, POL_POD));
					mCarrierDetail.setInitial(Util.getJSONKeyvalue(carrierDetailjsonObj, INITIAL));
					mCarrierDetail.setReal(Util.getJSONKeyvalue(carrierDetailjsonObj, REAL));
					mCarrierDetail.setDelay_Days(Util.getJSONKeyvalue(carrierDetailjsonObj, DELAY_DAYS));

					mCarrierDetail.setFromdate(fromdate);
					mCarrierDetail.setTodate(todate);
					mCarrierDetail.setType("");
					mCarrierDetail.setIsOrigin(isOrigin);
					mCarrierDetail.setCarriername(carriername);
					mCarrierDetail.setILNCode(ilncode);

					/** Database insertion **/
					mCarrierDetailsTableHelper.storeGraphOVData(mCarrierDetail);
					/***************************/

					carDetailsList.add(mCarrierDetail);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carDetailsList;
	}

	/* For InTransit Shipment CarrierDetail */

	/* For Potential Shipment CarrierDetail */
	private ArrayList<CarrierDetail> PotentialCarrierDetailsData(JSONObject jsObg) {
		ArrayList<CarrierDetail> carDetailsList = null;
		try {
			JSONArray carrierJodnarray = jsObg.getJSONArray(CARRIERDETAILS);
			if (carrierJodnarray != null && carrierJodnarray.length() >= 1) {
				carDetailsList = new ArrayList<CarrierDetail>();
				for (int i = 0; i < carrierJodnarray.length(); i++) {

					JSONObject carrierDetailjsonObj = carrierJodnarray.getJSONObject(i);
					CarrierDetail mCarrierDetail = new CarrierDetail();

					mCarrierDetail.setCarrier_Name(Util.getJSONKeyvalue(carrierDetailjsonObj, CARRIER_NAME_FORCARRIERDETAILS));
					mCarrierDetail.setContainer_Number(Util.getJSONKeyvalue(carrierDetailjsonObj, CONTAINER_NUMBER));
					mCarrierDetail.setPOL_POD(Util.getJSONKeyvalue(carrierDetailjsonObj, POL_POD));
					mCarrierDetail.setInitial(Util.getJSONKeyvalue(carrierDetailjsonObj, INITIAL));
					mCarrierDetail.setReal(Util.getJSONKeyvalue(carrierDetailjsonObj, REAL));
					mCarrierDetail.setDelay_Days(Util.getJSONKeyvalue(carrierDetailjsonObj, DELAY_DAYS));

					mCarrierDetail.setFromdate(fromdate);
					mCarrierDetail.setTodate(todate);
					mCarrierDetail.setType("");
					mCarrierDetail.setIsOrigin(isOrigin);
					mCarrierDetail.setCarriername(carriername);
					mCarrierDetail.setILNCode(ilncode);

					/** Database insertion **/
					mCarrierDetailsTableHelper.storeGraphOVData(mCarrierDetail);
					/***************************/

					carDetailsList.add(mCarrierDetail);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carDetailsList;
	}

	/* For Potential Shipment CarrierDetail */

	public CarierDetailsParserInterface carierdetailsparserinterface;

	public CarierDetailsParserInterface getCarierdetailsparserinterface() {
		return carierdetailsparserinterface;
	}

	public void setCarierdetailsparserinterface(CarierDetailsParserInterface carierdetailsparserinterface) {
		this.carierdetailsparserinterface = carierdetailsparserinterface;
	}

	public interface CarierDetailsParserInterface {
		public void ONSuccess(ArrayList<CarrierDetail> carrierDetails);

		public void OnError();

		public void NoData();
	}
}
