package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.SpinnerDataSet;

public class IncidentReportDropdownParser {

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";

	private String INCIDENTLEVEL = "IncidentLevel";
	private String RISKTYPE = "RiskType";
	private String INCIDENTTYPE = "IncidentType";

	private String NAME = "name";
	private String CODE = "code";

	private Context mContext;
	private String responsecode = "";

	public void parse(Context context, String createUrl) {
		this.mContext = context;

		IncidentReportParserAsync mIncidentReportParserAsync = new IncidentReportParserAsync(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mIncidentReportParserAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mIncidentReportParserAsync.execute();
		}
	}

	private ArrayList<ArrayList<SpinnerDataSet>> ParseTheData(String responseJSOn) {

		ArrayList<ArrayList<SpinnerDataSet>> incidentDropdownListData = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.optString(RESPONSECODE);
			if (responsecode.equals("200")) {

				incidentDropdownListData = new ArrayList<ArrayList<SpinnerDataSet>>();

				JSONArray incidentlevelJsonArray = jsObg.getJSONArray(INCIDENTLEVEL);
				JSONArray risktypeJsonArray = jsObg.getJSONArray(RISKTYPE);
				JSONArray incidenttypeJsonArray = jsObg.getJSONArray(INCIDENTTYPE);

				ArrayList<SpinnerDataSet> incidentlevel = new ArrayList<SpinnerDataSet>();
				SpinnerDataSet emptySta = new SpinnerDataSet();
				emptySta.setId("");
				emptySta.setName("Select Incident Level");
				incidentlevel.add(emptySta);
				for (int i = 0; i < incidentlevelJsonArray.length(); i++) {
					JSONObject level = incidentlevelJsonArray.getJSONObject(i);
					SpinnerDataSet mDataSet = new SpinnerDataSet();
					mDataSet.setId(Util.getJSONKeyvalue(level, CODE));
					mDataSet.setName(Util.getJSONKeyvalue(level, NAME));
					incidentlevel.add(mDataSet);
				}

				ArrayList<SpinnerDataSet> type = new ArrayList<SpinnerDataSet>();
				emptySta = new SpinnerDataSet();
				emptySta.setId("");
				emptySta.setName("Select Incident Type");
				type.add(emptySta);
				for (int i = 0; i < incidenttypeJsonArray.length(); i++) {
					JSONObject level = incidenttypeJsonArray.getJSONObject(i);
					SpinnerDataSet mDataSet = new SpinnerDataSet();
					mDataSet.setId(Util.getJSONKeyvalue(level, CODE));
					mDataSet.setName(Util.getJSONKeyvalue(level, NAME));
					type.add(mDataSet);
				}

				ArrayList<SpinnerDataSet> risktype = new ArrayList<SpinnerDataSet>();
				emptySta = new SpinnerDataSet();
				emptySta.setId("");
				emptySta.setName("Select Risk Type");
				risktype.add(emptySta);
				for (int i = 0; i < risktypeJsonArray.length(); i++) {
					JSONObject level = risktypeJsonArray.getJSONObject(i);
					SpinnerDataSet mDataSet = new SpinnerDataSet();
					mDataSet.setId(Util.getJSONKeyvalue(level, CODE));
					mDataSet.setName(Util.getJSONKeyvalue(level, NAME));
					risktype.add(mDataSet);
				}
				incidentDropdownListData.add(incidentlevel);
				incidentDropdownListData.add(type);
				incidentDropdownListData.add(risktype);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return incidentDropdownListData;
	}

	private class IncidentReportParserAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String inputUrl;
		private String authenticationJson;
		private ArrayList<ArrayList<SpinnerDataSet>> incidentDropdownData;
		private boolean isTimeOut = false;

		public IncidentReportParserAsync(Context mContext, String createUrl) {
			this.context = mContext;
			this.inputUrl = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(inputUrl);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				if (authenticationJson != null && !authenticationJson.equals("")) {
					incidentDropdownData = ParseTheData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);

			if (isTimeOut) {
				if (incidentReportDropdownParserInterface != null) {
					incidentReportDropdownParserInterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (incidentReportDropdownParserInterface != null) {
						incidentReportDropdownParserInterface.ONSuccess(incidentDropdownData);
					}
				} else if (responsecode.equals("1002")) {
					if (incidentReportDropdownParserInterface != null) {
						incidentReportDropdownParserInterface.NoData();
					}
				} else {
					if (incidentReportDropdownParserInterface != null) {
						incidentReportDropdownParserInterface.OnError();
					}
				}
			}
		}
	}

	public IncidentReportDropdownParserInterface incidentReportDropdownParserInterface;

	public IncidentReportDropdownParserInterface getIncidentReportDropdownParserInterface() {
		return incidentReportDropdownParserInterface;
	}

	public void setIncidentReportDropdownParserInterface(IncidentReportDropdownParserInterface incidentReportDropdownParserInterface) {
		this.incidentReportDropdownParserInterface = incidentReportDropdownParserInterface;
	}

	public interface IncidentReportDropdownParserInterface {
		public void ONSuccess(ArrayList<ArrayList<SpinnerDataSet>> incidentDropdownData);

		public void NoData();

		public void OnError();
	}

}
