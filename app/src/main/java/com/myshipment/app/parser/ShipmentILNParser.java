package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.Util.ShipmentClickState;
import com.myshipment.app.databasehelper.PieChartILNTableHelper;
import com.myshipment.app.model.ILN;

public class ShipmentILNParser {

	private Context mContext;

	private String fromdate;
	private String todate;
	private String isOrigin;
	private String carriername;
	private PieChartILNTableHelper mTableHelper = null;

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";

	/* For Total Shipment ILN */
	private String TOTAL_ILN = "ILN";
	private String TOTAL_ILNID = "ILNID";
	private String TOTAL_ILN_NAME = "ILN Name";
	private String TOTAL_NO_OF_SHIPMENT = "No Of Shipment";
	private String TOTAL_OVERALL_PERCENTAGE = "Overall Percentage";
	private String TOTAL_RGB = "RGB";

	/* For Delay Shipment ILN */
	private String DELAY_ILN = "ILN";
	private String DELAY_ILNID = "ILNID";
	private String DELAY_ILN_NAME = "ILN Name";
	private String DELAY_NO_OF_SHIPMENT = "No Of Shipment";
	private String DELAY_NO_OF_DELAY_SHIPMENT = "No Of delay Shipment";
	private String DELAY_OVERALL_PERCENTAGE = "Overall Percentage";
	private String DELAY_INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String DELAY_RGB = "RGB";

	/* For InTransit Shipment ILN */
	private String INTRANSIT_ILN = "ILN";
	private String INTRANSIT_ILNID = "ILNID";
	private String INTRANSIT_ILN_NAME = "ILN Name";
	private String INTRANSIT_NO_OF_SHIPMENT = "No Of Shipment";
	private String INTRANSIT_NO_OF_INTRANSIT_SHIPMENT = "No Of Intransit Shipment";
	private String INTRANSIT_OVERALL_PERCENTAGE = "Overall Percentage";
	private String INTRANSIT_INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String INTRANSIT_RGB = "RGB";

	/* For PotentialDelay Shipment ILN */
	private String POTENTIALDELAY_ILN = "ILN";
	private String POTENTIALDELAY_ILNID = "ILNID";
	private String POTENTIALDELAY_ILN_NAME = "ILN Name";
	private String POTENTIALDELAY_NO_OF_SHIPMENT = "No Of Shipment";
	private String POTENTIALDELAY_NO_OF_ON_TIME_SHIPMENT = "No Of On Time Shipment";
	private String POTENTIALDELAY_OVERALL_PERCENTAGE = "Overall Percentage";
	private String POTENTIALDELAY_INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String POTENTIALDELAY_RGB = "RGB";

	/* Response JSON key value */
	private String responsecode = "";

	public void parse(Context context, String createUrl, ShipmentClickState mShipmentClickState, String fromdate_, String todate_, String isOrigin_, String carriername_) {
		this.mContext = context;

		this.fromdate = fromdate_;
		this.todate = todate_;
		this.isOrigin = isOrigin_;
		this.carriername = carriername_;
		this.mTableHelper = new PieChartILNTableHelper(mContext);

		ShipmentILNAsync mShipmentAsync = new ShipmentILNAsync(mContext, createUrl, mShipmentClickState);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mShipmentAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mShipmentAsync.execute();
		}
	}

	public String[] getListViewFieldName() {
		String[] fields = new String[3];
		fields[0] = TOTAL_ILN_NAME;
		fields[1] = TOTAL_NO_OF_SHIPMENT;
		fields[2] = TOTAL_OVERALL_PERCENTAGE;
		return fields;
	}

	public String[] getListViewFieldNameForDelay() {
		String[] fields = new String[5];
		fields[0] = DELAY_ILN_NAME;
		fields[1] = DELAY_NO_OF_SHIPMENT;
		fields[2] = DELAY_NO_OF_DELAY_SHIPMENT;
		fields[3] = DELAY_OVERALL_PERCENTAGE;
		fields[4] = DELAY_INDIVIDUAL_PERCENTAGE;
		return fields;
	}

	public String[] getListViewFieldNameInTrans() {
		String[] fields = new String[5];
		fields[0] = INTRANSIT_ILN_NAME;
		fields[1] = INTRANSIT_NO_OF_SHIPMENT;
		fields[2] = INTRANSIT_NO_OF_INTRANSIT_SHIPMENT;
		fields[3] = INTRANSIT_OVERALL_PERCENTAGE;
		fields[4] = INTRANSIT_INDIVIDUAL_PERCENTAGE;
		return fields;
	}

	public String[] getListViewFieldNamePotentialDelay() {
		String[] fields = new String[5];
		fields[0] = POTENTIALDELAY_ILN_NAME;
		fields[1] = POTENTIALDELAY_NO_OF_SHIPMENT;
		fields[2] = POTENTIALDELAY_NO_OF_ON_TIME_SHIPMENT;
		fields[3] = POTENTIALDELAY_OVERALL_PERCENTAGE;
		fields[4] = POTENTIALDELAY_INDIVIDUAL_PERCENTAGE;
		return fields;
	}

	private class ShipmentILNAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;
		private ArrayList<ILN> ILNS;
		private String createUrl_;
		private ShipmentClickState State;
		private boolean isTimeOut = false;

		public ShipmentILNAsync(Context mContext, String createUrl, ShipmentClickState mShipmentClickState) {
			this.context = mContext;
			this.createUrl_ = createUrl;
			this.State = mShipmentClickState;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(createUrl_);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				if (authenticationJson != null && !authenticationJson.equals("")) {
					JSONObject jsObg = new JSONObject(authenticationJson);
					responsecode = jsObg.getString(RESPONSECODE);
					if (responsecode.equals("200")) {
						if (State == ShipmentClickState.Shipments) {
							ILNS = ShipmentILNData(jsObg);
						} else if (State == ShipmentClickState.Delayed) {
							ILNS = DelayILNData(jsObg);
						} else if (State == ShipmentClickState.InTransit) {
							ILNS = InTransitILNData(jsObg);
						} else if (State == ShipmentClickState.PotentialDelay) {
							ILNS = PotentialILNData(jsObg);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);
			if (isTimeOut) {
				if (shipmentilnparserinterface != null) {
					shipmentilnparserinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (shipmentilnparserinterface != null) {
						shipmentilnparserinterface.ONSuccess(ILNS);
					}
				} else if (responsecode.equals("1002")) {
					if (shipmentilnparserinterface != null) {
						shipmentilnparserinterface.NoData();
					}
				} else {
					if (shipmentilnparserinterface != null) {
						shipmentilnparserinterface.OnError();
					}
				}
			}
		}
	}

	/* For Total Shipment ILN */
	private ArrayList<ILN> ShipmentILNData(JSONObject jsObg) {
		ArrayList<ILN> ILNs = null;
		try {
			JSONArray ILNjsonArray = jsObg.getJSONArray(TOTAL_ILN);
			if (ILNjsonArray != null && ILNjsonArray.length() >= 1) {
				ILNs = new ArrayList<ILN>();

				for (int i = 0; i < ILNjsonArray.length(); i++) {
					JSONObject ILNjsonObj = ILNjsonArray.getJSONObject(i);
					ILN mIln = new ILN();
					mIln.setILNID(Util.getJSONKeyvalue(ILNjsonObj, TOTAL_ILNID));
					mIln.setILN_Name(Util.getJSONKeyvalue(ILNjsonObj, TOTAL_ILN_NAME));
					mIln.setNo_Of_Shipment(Util.getJSONKeyvalue(ILNjsonObj, TOTAL_NO_OF_SHIPMENT));
					mIln.setOverall_Percentage(Util.getJSONKeyvalue(ILNjsonObj, TOTAL_OVERALL_PERCENTAGE));
					mIln.setRGB(Util.getJSONKeyvalue(ILNjsonObj, TOTAL_RGB));
					mIln.setFlows(null);

					mIln.setFromDate(fromdate);
					mIln.setToDate(todate);
					mIln.setIsOrigin(isOrigin);
					mIln.setCarrierName(carriername);

					/** Database insertion **/
					mTableHelper.storeGraphOVData(mIln);
					/***************************/

					ILNs.add(mIln);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ILNs;
	}

	/* For Total Shipment ILN */

	/* For Delay Shipment ILN */
	private ArrayList<ILN> DelayILNData(JSONObject jsObg) {
		ArrayList<ILN> ILNs = null;
		try {
			JSONArray ILNjsonArray = jsObg.getJSONArray(DELAY_ILN);
			if (ILNjsonArray != null && ILNjsonArray.length() >= 1) {
				ILNs = new ArrayList<ILN>();
				for (int i = 0; i < ILNjsonArray.length(); i++) {
					JSONObject ILNjsonObj = ILNjsonArray.getJSONObject(i);
					ILN mIln = new ILN();
					mIln.setILNID(Util.getJSONKeyvalue(ILNjsonObj, DELAY_ILNID));
					mIln.setILN_Name(Util.getJSONKeyvalue(ILNjsonObj, DELAY_ILN_NAME));
					mIln.setNo_Of_Shipment(Util.getJSONKeyvalue(ILNjsonObj, DELAY_NO_OF_SHIPMENT));
					mIln.setNo_Of__Shipment(Util.getJSONKeyvalue(ILNjsonObj, DELAY_NO_OF_DELAY_SHIPMENT));
					mIln.setOverall_Percentage(Util.getJSONKeyvalue(ILNjsonObj, DELAY_OVERALL_PERCENTAGE));
					mIln.setIndividual_Percentage(Util.getJSONKeyvalue(ILNjsonObj, DELAY_INDIVIDUAL_PERCENTAGE));
					mIln.setRGB(Util.getJSONKeyvalue(ILNjsonObj, DELAY_RGB));
					mIln.setFlows(null);

					mIln.setFromDate(fromdate);
					mIln.setToDate(todate);
					mIln.setIsOrigin(isOrigin);
					mIln.setCarrierName(carriername);

					/** Database insertion **/
					mTableHelper.storeGraphOVData(mIln);
					/***************************/

					ILNs.add(mIln);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ILNs;
	}

	/* For Delay Shipment ILN */

	/* For InTransit Shipment */
	private ArrayList<ILN> InTransitILNData(JSONObject jsObg) {
		ArrayList<ILN> ILNs = null;
		try {
			JSONArray ILNjsonArray = jsObg.getJSONArray(INTRANSIT_ILN);
			if (ILNjsonArray != null && ILNjsonArray.length() >= 1) {
				ILNs = new ArrayList<ILN>();
				for (int i = 0; i < ILNjsonArray.length(); i++) {
					JSONObject ILNjsonObj = ILNjsonArray.getJSONObject(i);
					ILN mIln = new ILN();
					mIln.setILNID(Util.getJSONKeyvalue(ILNjsonObj, INTRANSIT_ILNID));
					mIln.setILN_Name(Util.getJSONKeyvalue(ILNjsonObj, INTRANSIT_ILN_NAME));
					mIln.setNo_Of_Shipment(Util.getJSONKeyvalue(ILNjsonObj, INTRANSIT_NO_OF_SHIPMENT));
					mIln.setNo_Of__Shipment(Util.getJSONKeyvalue(ILNjsonObj, INTRANSIT_NO_OF_INTRANSIT_SHIPMENT));
					mIln.setOverall_Percentage(Util.getJSONKeyvalue(ILNjsonObj, INTRANSIT_OVERALL_PERCENTAGE));
					mIln.setIndividual_Percentage(Util.getJSONKeyvalue(ILNjsonObj, INTRANSIT_INDIVIDUAL_PERCENTAGE));
					mIln.setRGB(Util.getJSONKeyvalue(ILNjsonObj, INTRANSIT_RGB));
					mIln.setFlows(null);

					mIln.setFromDate(fromdate);
					mIln.setToDate(todate);
					mIln.setIsOrigin(isOrigin);
					mIln.setCarrierName(carriername);

					/** Database insertion **/
					mTableHelper.storeGraphOVData(mIln);
					/***************************/

					ILNs.add(mIln);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ILNs;
	}

	/* For InTransit Shipment */

	/* For Potential Shipment */
	private ArrayList<ILN> PotentialILNData(JSONObject jsObg) {
		ArrayList<ILN> ILNs = null;
		try {
			JSONArray ILNjsonArray = jsObg.getJSONArray(POTENTIALDELAY_ILN);
			if (ILNjsonArray != null && ILNjsonArray.length() >= 1) {
				ILNs = new ArrayList<ILN>();
				for (int i = 0; i < ILNjsonArray.length(); i++) {
					JSONObject ILNjsonObj = ILNjsonArray.getJSONObject(i);
					ILN mIln = new ILN();
					mIln.setILNID(Util.getJSONKeyvalue(ILNjsonObj, POTENTIALDELAY_ILNID));
					mIln.setILN_Name(Util.getJSONKeyvalue(ILNjsonObj, POTENTIALDELAY_ILN_NAME));
					mIln.setNo_Of_Shipment(Util.getJSONKeyvalue(ILNjsonObj, POTENTIALDELAY_NO_OF_SHIPMENT));
					mIln.setNo_Of__Shipment(Util.getJSONKeyvalue(ILNjsonObj, POTENTIALDELAY_NO_OF_ON_TIME_SHIPMENT));
					mIln.setOverall_Percentage(Util.getJSONKeyvalue(ILNjsonObj, POTENTIALDELAY_OVERALL_PERCENTAGE));
					mIln.setIndividual_Percentage(Util.getJSONKeyvalue(ILNjsonObj, POTENTIALDELAY_INDIVIDUAL_PERCENTAGE));
					mIln.setRGB(Util.getJSONKeyvalue(ILNjsonObj, POTENTIALDELAY_RGB));
					mIln.setFlows(null);

					mIln.setFromDate(fromdate);
					mIln.setToDate(todate);
					mIln.setIsOrigin(isOrigin);
					mIln.setCarrierName(carriername);

					/** Database insertion **/
					mTableHelper.storeGraphOVData(mIln);
					/***************************/

					ILNs.add(mIln);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ILNs;
	}

	/* For Potential Shipment */

	public ShipmentILNParserInterface shipmentilnparserinterface;

	public ShipmentILNParserInterface getShipmentilnparserinterface() {
		return shipmentilnparserinterface;
	}

	public void setShipmentilnparserinterface(ShipmentILNParserInterface shipmentilnparserinterface) {
		this.shipmentilnparserinterface = shipmentilnparserinterface;
	}

	public interface ShipmentILNParserInterface {
		public void ONSuccess(ArrayList<ILN> iLNS);

		public void OnError();

		public void NoData();
	}
}
