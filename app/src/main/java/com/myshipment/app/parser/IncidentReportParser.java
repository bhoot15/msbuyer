package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.IncidentRepor;

public class IncidentReportParser {

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";
	private String INCIDENTRESULT = "IncidentResult";

	private String BOOKINGNUMBER = "BookingNumber";
	private String INCIDENTNUMBER = "IncidentNumber";
	private String DIFIPSUPPORT = "DIFIPsupport";
	private String ILNNAME = "ILNname";
	private String INITIALVESSELWEEK = "InitialVesselweek";
	private String INCIDENTCLOSUREDATE = "IncidentClosuredate";

	private Context mContext;
	private String responsecode = "";

	public void parse(Context context, String createUrl) {
		this.mContext = context;

		IncidentReportParserAsync mIncidentReportParserAsync = new IncidentReportParserAsync(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mIncidentReportParserAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mIncidentReportParserAsync.execute();
		}
	}

	private ArrayList<IncidentRepor> ParseTheData(String responseJSOn) {

		ArrayList<IncidentRepor> incidentRepors = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.optString(RESPONSECODE);
			if (responsecode.equals("200")) {
				JSONArray incidentJsonArray = jsObg.getJSONArray(INCIDENTRESULT);
				incidentRepors = new ArrayList<IncidentRepor>();
				for (int i = 0; i < incidentJsonArray.length(); i++) {

					IncidentRepor mData = new IncidentRepor();
					JSONObject incidentjsonObj = incidentJsonArray.getJSONObject(i);

					mData.setBookingNumber(Util.getJSONKeyvalue(incidentjsonObj, BOOKINGNUMBER));
					mData.setDIFIPsupport(Util.getJSONKeyvalue(incidentjsonObj, DIFIPSUPPORT));
					mData.setILNname(Util.getJSONKeyvalue(incidentjsonObj, ILNNAME));
					mData.setIncidentClosuredate(Util.getJSONKeyvalue(incidentjsonObj, INCIDENTCLOSUREDATE));
					mData.setIncidentNumber(Util.getJSONKeyvalue(incidentjsonObj, INCIDENTNUMBER));
					mData.setInitialVesselweek(Util.getJSONKeyvalue(incidentjsonObj, INITIALVESSELWEEK));

					incidentRepors.add(mData);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return incidentRepors;
	}

	private class IncidentReportParserAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String inputUrl;
		private String authenticationJson;
		private ArrayList<IncidentRepor> incidentRepors;
		private boolean isTimeOut = false;

		public IncidentReportParserAsync(Context mContext, String createUrl) {
			this.context = mContext;
			this.inputUrl = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(inputUrl);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				if (authenticationJson != null && !authenticationJson.equals("")) {
					incidentRepors = ParseTheData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);

			if (isTimeOut) {
				if (incidentReportParserInterface != null) {
					incidentReportParserInterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (incidentReportParserInterface != null) {
						incidentReportParserInterface.ONSuccess(incidentRepors);
					}
				} else if (responsecode.equals("1002")) {
					if (incidentReportParserInterface != null) {
						incidentReportParserInterface.NoData();
					}
				} else {
					if (incidentReportParserInterface != null) {
						incidentReportParserInterface.OnError();
					}
				}
			}
		}
	}

	public IncidentReportParserInterface incidentReportParserInterface;

	public IncidentReportParserInterface getIncidentReportParserInterface() {
		return incidentReportParserInterface;
	}

	public void setIncidentReportParserInterface(IncidentReportParserInterface incidentReportParserInterface) {
		this.incidentReportParserInterface = incidentReportParserInterface;
	}

	public interface IncidentReportParserInterface {
		public void ONSuccess(ArrayList<IncidentRepor> incidentReporArrayList);

		public void NoData();

		public void OnError();
	}
}
