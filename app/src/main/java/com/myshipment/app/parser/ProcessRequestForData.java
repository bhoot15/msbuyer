package com.myshipment.app.parser;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;

public class ProcessRequestForData {

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";

	private Context mContext;
	private String responsecode = "";

	public void parse(Context context, String createUrl) {
		this.mContext = context;

		IncidentReportParserAsync mIncidentReportParserAsync = new IncidentReportParserAsync(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mIncidentReportParserAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mIncidentReportParserAsync.execute();
		}
	}

	private class IncidentReportParserAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String inputUrl;
		private String authenticationJson;
		private boolean isSuccess = false;
		private boolean isTimeOut = false;

		public IncidentReportParserAsync(Context mContext, String createUrl) {
			this.context = mContext;
			this.inputUrl = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(inputUrl);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				if (authenticationJson != null && !authenticationJson.equals("")) {

					JSONObject mObject = new JSONObject(authenticationJson);
					responsecode = Util.getJSONKeyvalue(mObject, RESPONSECODE);
					isSuccess = (responsecode.equals("200")) ? true : false;

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);

			if (isTimeOut) {
				if (processRequestForDataParserInterface != null) {
					processRequestForDataParserInterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (processRequestForDataParserInterface != null) {
						processRequestForDataParserInterface.ONSuccess(isSuccess);
					}
				} else if (responsecode.equals("1002")) {
					if (processRequestForDataParserInterface != null) {
						processRequestForDataParserInterface.NoData();
					}
				} else {
					if (processRequestForDataParserInterface != null) {
						processRequestForDataParserInterface.OnError();
					}
				}
			}
		}
	}

	public ProcessRequestForDataParserInterface processRequestForDataParserInterface;

	public ProcessRequestForDataParserInterface getProcessRequestForDataParserInterface() {
		return processRequestForDataParserInterface;
	}

	public void setProcessRequestForDataParserInterface(ProcessRequestForDataParserInterface processRequestForDataParserInterface) {
		this.processRequestForDataParserInterface = processRequestForDataParserInterface;
	}

	public interface ProcessRequestForDataParserInterface {
		public void ONSuccess(boolean isSuccess);

		public void NoData();

		public void OnError();
	}

}
