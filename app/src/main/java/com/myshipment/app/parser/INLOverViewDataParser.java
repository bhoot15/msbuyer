package com.myshipment.app.parser;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.databasehelper.GraphOverViewTableHelper;
import com.myshipment.app.databasehelper.PieChartContainersTableHelper;
import com.myshipment.app.model.INLData;
import com.myshipment.app.model.INLOverViewData;

public class INLOverViewDataParser {
	private Context mContext;
	private String fromdate;
	private String todate;
	private String isOrigin;

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";
	// private String STATUS = "Status";
	private String STATUS = "ResponseDetail";

	private String MAXCONTAINERCOUNT = "MaxContainerCount";
	// private String MAXDELAYPERCENTAGERATE = "MaxDelaypercentagerate";
	// private String TOTALSHIPMENT = "TotalShipment";
	// private String TOTALDELAYED = "TotalDelayed";
	// private String TOTALINTRANSIT = "TotalIntransit";
	private String TOTALPOTENTIALDELAY = "TotalPotentialDelay";

	private String OVERVIEWFORGRAPH = "OverViewForGraph";

	private String ID = "id";
	private String ILNNAME = "ILNname";
	private String POTENTIALDELAY = "PotentialDelay";
	private String TOTALSHIPMENT = "TotalShipment";
	private String TOTALDELAYED = "TotalDelayed";
	private String TOTALINTRANSIT = "TotalIntransit";
	// private String TOTALREACHEDONTIME = "TotalReachedOnTime";
	private String DELAYRATE = "DelayRate";

	/* Response JSON key value */

	private String responsecode = "";
	private String responsedetails = "";

	public void parse(Context context, String createUrl, String fromdate_, String todate_, String isOrigin_) {
		this.mContext = context;
		this.fromdate = fromdate_;
		this.todate = todate_;
		this.isOrigin = isOrigin_;

		INLOverViewDataAsync mINLOverViewDataAsync = new INLOverViewDataAsync(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mINLOverViewDataAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mINLOverViewDataAsync.execute();
		}
	}

	private class INLOverViewDataAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;
		private INLOverViewData mViewData_;
		private String CREATEURL;
		private boolean isTimeOut = false;

		public INLOverViewDataAsync(Context mContext, String createUrl) {
			this.context = mContext;
			this.CREATEURL = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(CREATEURL);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;

				if (authenticationJson != null && !authenticationJson.equals("")) {
					mViewData_ = ParseData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);
			if (isTimeOut) {
				if (inloverviewdataparserinterface != null) {
					inloverviewdataparserinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (inloverviewdataparserinterface != null) {
						inloverviewdataparserinterface.ONSuccess(mViewData_);
					}
				} else if (responsecode.equals("1002")) {
					if (inloverviewdataparserinterface != null) {
						inloverviewdataparserinterface.NoData();
					}
				} else {
					if (inloverviewdataparserinterface != null) {
						inloverviewdataparserinterface.OnError();
					}
				}
			}

		}
	}

	private INLOverViewData ParseData(String responseJSOn) {
		INLOverViewData mViewData = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.getString(RESPONSECODE);
			responsedetails = jsObg.getString(STATUS);

			if (responsecode.equals("200")) {

				mViewData = new INLOverViewData();
				mViewData.setResponsecode(responsecode);
				mViewData.setStatus(responsedetails);

				String maxValue = jsObg.getString(MAXCONTAINERCOUNT);
				int RoundMaxValue = Util.getMaxValue(Integer.parseInt(maxValue));

				mViewData.setMaxcontainercount("" + RoundMaxValue);
				// mViewData.setMaxdelaypercentagerate(jsObg.getString(MAXDELAYPERCENTAGERATE));
				// Change as per new API Change 26.11.2014 .
				mViewData.setMaxdelaypercentagerate("100");
				mViewData.setTotalshipment(Util.getJsonValue(jsObg, TOTALSHIPMENT));
				mViewData.setTotaldelayed(Util.getJsonValue(jsObg, TOTALDELAYED));
				mViewData.setTotalintransit(Util.getJsonValue(jsObg, TOTALINTRANSIT));
				mViewData.setTotalpotentialdelay(Util.getJsonValue(jsObg, TOTALPOTENTIALDELAY));

				mViewData.setFromDate(fromdate);
				mViewData.setToDate(todate);
				mViewData.setIsOrigin(isOrigin);

				/** Database insertion **/
				PieChartContainersTableHelper mHelper = PieChartContainersTableHelper.getInstance(mContext);
				mHelper.storeGraphOVData(mViewData);
				/*********************/

				JSONArray array = jsObg.getJSONArray(OVERVIEWFORGRAPH);

				ArrayList<INLData> arrayINLData = null;
				if (array != null && array.length() >= 1) {

					GraphOverViewTableHelper mOverViewTableHelper = GraphOverViewTableHelper.getInstance(mContext);

					arrayINLData = new ArrayList<INLData>();
					for (int i = 0; i < array.length(); i++) {
						JSONObject INLObj = array.getJSONObject(i);
						INLData data = new INLData();
						data.setId(Util.getJsonValue(INLObj, ID));
						data.setILNname(Util.getJsonValue(INLObj, ILNNAME));
						data.setTotalShipment(Util.getJsonValue(INLObj, TOTALSHIPMENT));
						data.setTotalDelayed(Util.getJsonValue(INLObj, TOTALDELAYED));
						data.setTotalIntransit(Util.getJsonValue(INLObj, TOTALINTRANSIT));
						data.setTotalReachedOnTime(Util.getJsonValue(INLObj, POTENTIALDELAY));
						data.setDelayRate(Util.getJsonValue(INLObj, DELAYRATE));

						data.setFromDate(fromdate);
						data.setToDate(todate);
						data.setIsOrigin(isOrigin);

						/** Database insertion **/
						mOverViewTableHelper.storeGraphOVData(data);
						/***************************/
						arrayINLData.add(data);
					}
				}
				mViewData.setArrayINLData(arrayINLData);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return mViewData;
	}

	public List<NameValuePair> getparamBody() {
		List<NameValuePair> pairsofEducation = new ArrayList<NameValuePair>();
		pairsofEducation.add(new BasicNameValuePair("api_key", "20myshipment14"));
		pairsofEducation.add(new BasicNameValuePair("userid ", "Carnio"));
		pairsofEducation.add(new BasicNameValuePair("UserType", "I"));
		pairsofEducation.add(new BasicNameValuePair("Fromdate", "26/8/2014"));
		pairsofEducation.add(new BasicNameValuePair("ToDate", "26/11/2014"));
		pairsofEducation.add(new BasicNameValuePair("IsOrigin", "1"));
		return pairsofEducation;
	}

	public INLOverViewDataParserInterface inloverviewdataparserinterface;

	public INLOverViewDataParserInterface getInloverviewdataparserinterface() {
		return inloverviewdataparserinterface;
	}

	public void setInloverviewdataparserinterface(INLOverViewDataParserInterface inloverviewdataparserinterface) {
		this.inloverviewdataparserinterface = inloverviewdataparserinterface;
	}

	public interface INLOverViewDataParserInterface {
		public void ONSuccess(INLOverViewData mViewData_);

		public void OnError();

		public void NoData();
	}

}
