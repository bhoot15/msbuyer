package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.RegularSearchResult;

public class RefineSearchParser {

	private Context mContext;

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";
	private String STATUS = "ResponseDetail";

	private String SHIPPINGLINE = "ShippingLine";

	private String POL = "POL";
	private String ILN = "ILN";
	private String ATD = "ATD";
	private String ETD = "ETD";
	private String POD = "POD";
	private String InitialVesselWeek = "InitialVesselWeek";
	private String ATA = "ATA";
	private String Booking = "Booking";
	private String ETA = "ETA";
	private String Container = "Container";

	/* Response JSON key value */

	private String responsecode = "";
	private String responsedetails = "";

	public void parse(Context context, String createUrl) {
		this.mContext = context;
		ServiceKPIReportAsync mServiceKPIReport = new ServiceKPIReportAsync(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mServiceKPIReport.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mServiceKPIReport.execute();
		}
	}

	private class ServiceKPIReportAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;
		private ArrayList<RegularSearchResult> listRegularSearchResult;
		private String CREATEURL;
		private boolean isTimeOut = false;

		public ServiceKPIReportAsync(Context mContext, String createUrl) {
			this.context = mContext;
			this.CREATEURL = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(CREATEURL);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;

				if (authenticationJson != null && !authenticationJson.equals("")) {
					listRegularSearchResult = ParseData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);
			if (isTimeOut) {
				if (refineSearchParserInterface != null) {
					refineSearchParserInterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (refineSearchParserInterface != null) {
						refineSearchParserInterface.OnSuccess(listRegularSearchResult);
					}
				} else if (responsecode.equals("1002")) {
					if (refineSearchParserInterface != null) {
						refineSearchParserInterface.NoData();
					}
				} else {
					if (refineSearchParserInterface != null) {
						refineSearchParserInterface.OnError();
					}
				}
			}

		}
	}

	private ArrayList<RegularSearchResult> ParseData(String responseJSOn) {
		ArrayList<RegularSearchResult> listRegularSearchResult = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.getString(RESPONSECODE);
			responsedetails = jsObg.getString(STATUS);

			if (responsecode.equals("200")) {
				JSONArray mJSONArray = jsObg.getJSONArray(SHIPPINGLINE);

				if (mJSONArray != null && mJSONArray.length() >= 1) {
					listRegularSearchResult = new ArrayList<RegularSearchResult>();

					for (int i = 0; i < mJSONArray.length(); i++) {

						JSONObject serviceObj = mJSONArray.getJSONObject(i);
						RegularSearchResult mRegularSearchResult = new RegularSearchResult();

						mRegularSearchResult.setAtd(Util.getJsonValue(serviceObj, ATD));
						mRegularSearchResult.setBooking(Util.getJsonValue(serviceObj, Booking));
						mRegularSearchResult.setContainer(Util.getJsonValue(serviceObj, Container));
						mRegularSearchResult.setEta(Util.getJsonValue(serviceObj, ETA));
						mRegularSearchResult.setEtd(Util.getJsonValue(serviceObj, ETD));
						mRegularSearchResult.setIln(Util.getJsonValue(serviceObj, ILN));
						mRegularSearchResult.setInitialvesselweek(Util.getJsonValue(serviceObj, InitialVesselWeek));
						mRegularSearchResult.setAta(Util.getJsonValue(serviceObj, ATA));
						mRegularSearchResult.setPod(Util.getJsonValue(serviceObj, POD));
						mRegularSearchResult.setPol(Util.getJsonValue(serviceObj, POL));
						listRegularSearchResult.add(mRegularSearchResult);
					}
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return listRegularSearchResult;
	}

	public RefineSearchParserInterface refineSearchParserInterface;

	public RefineSearchParserInterface getRefineSearchParserInterface() {
		return refineSearchParserInterface;
	}

	public void setRefineSearchParserInterface(RefineSearchParserInterface refineSearchParserInterface) {
		this.refineSearchParserInterface = refineSearchParserInterface;
	}

	public interface RefineSearchParserInterface {
		public void OnSuccess(ArrayList<RegularSearchResult> RegularSearchResults);

		public void OnError();

		public void NoData();
	}
}
