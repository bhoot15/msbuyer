package com.myshipment.app.parser;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.UserDetails;

public class LoginParser {

	private Context mContext;
	
	private String USERNAME="Username";
	private String ROLE="Role";
	private String FIRSTNAME="FirstName";
	private String RESPONSEDETAIL="ResponseDetail";
	private String RESPONSECODE="ResponseCode";
	private String USERID="UserId";
	private String LASTNAME="LastName";
	private String VC_USER_TYPE="VC_USER_TYPE";
	private String VC_MASTER_USERNAME="VC_MASTER_USERNAME";
	
	
	/*Response JSON key value*/
	private String responsecode="";
	private String responsedetails="";
	
	
	public void parse(Context context, String username, String password) {
		this.mContext = context;
		loginAsync mLoginAsync = new loginAsync(mContext,Util.getLoginUrl(username, password));
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mLoginAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mLoginAsync.execute();
		}
	}
	
	
	private class loginAsync extends AsyncTask<Void, Void, Void>{
		private Context context;
		private String authenticationJson;
		private String url_ ; 
		private UserDetails mDetails;
		private boolean isTimeOut=false;
		
		public loginAsync(Context mContext, String url) {
			this.context = mContext;
			this.url_ = url; 
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(url_);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205"))?true:false;
				if (authenticationJson != null && !authenticationJson.equals("")) {
					JSONObject jsObj = new JSONObject(authenticationJson);
					responsecode = Util.getJsonValue(jsObj, RESPONSECODE);
					if (responsecode != null && responsecode.equals("200")) {
						mDetails = new UserDetails();

						mDetails.setFirstname(Util.getJsonValue(jsObj,FIRSTNAME));
						mDetails.setLastname(Util.getJsonValue(jsObj, LASTNAME));
						mDetails.setRole(Util.getJsonValue(jsObj, ROLE));
						mDetails.setUserid(Util.getJsonValue(jsObj, USERID));
						mDetails.setUsername(Util.getJsonValue(jsObj, USERNAME));
						mDetails.setVcmasterusername(Util.getJsonValue(jsObj,VC_MASTER_USERNAME));
						mDetails.setVcusertype(Util.getJsonValue(jsObj,VC_USER_TYPE));
						mDetails.saveInPreferense(context);
					} else if (responsecode != null && responsecode.equals("1002")) {
						responsedetails = Util.getJsonValue(jsObj,RESPONSEDETAIL);
					}

				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			try {
				Util.hideProgress(context);
				if (isTimeOut) {
					if (logininterface != null) {
						logininterface.OnError(); 
					}
				}else {
					if (responsecode.equals("200")) {
						if (logininterface!=null) {
							logininterface.OnSuccess(mDetails);
						}
					}else if (responsecode.equals("1002")){
						Util.ShowToast(mContext, responsedetails);
					}else {
						Util.ShowToast(mContext, mContext.getResources().getString(R.string.login_parser_msg_loginerror));
					}
				}
			} catch (NotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	
	public List<NameValuePair> getparamBody(String username2, String password) {
		List<NameValuePair> pairsofEducation = new ArrayList<NameValuePair>();
		pairsofEducation.add(new BasicNameValuePair("api_key", "20myshipment14"));
		pairsofEducation.add(new BasicNameValuePair("username", username2));
		pairsofEducation.add(new BasicNameValuePair("password", password));
		return pairsofEducation;
	}
	
	
	public LoginInterface logininterface;

	public LoginInterface getLogininterface() {
		return logininterface;
	}

	public void setLogininterface(LoginInterface logininterface) {
		this.logininterface = logininterface;
	}


	public interface LoginInterface{
		public void OnSuccess(UserDetails mDetails);
		public void OnError();
	}
	
	
	
	
}
