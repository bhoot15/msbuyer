package com.myshipment.app.parser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.TrackResult;
import com.myshipment.app.model.TrackResultDetail;

public class TrackShipmentParser {

	private Context mContext;

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";
	private String STATUS = "ResponseDetail";

	private String TRACKRESULT = "TrackResult";

	private String CARRIERNAME = "CarrierName";
	private String BOOKINGNO = "BookingNo";
	private String CONTAINERNO = "ContainerNo";
	private String POL = "POL";
	private String POD = "POD";
	private String ESTIMATEDETD = "EstimatedETD";
	private String ESTIMATEDETA = "EstimatedETA";
	private String ACTUALETD = "ActualETD";
	private String ACTUALETA = "ActualETA";

	private String DETAILS = "Details";

	private String VESSELNAME = "VesselName";
	private String DETAILS_DATE = "Date";
	private String VOYAGE = "Voyage";
	private String STATUSDESCRIPTION = "StatusDescription";
	private String LOCATION = "Location";

	/* Response JSON key value */

	private String responsecode = "";
	private String responsedetails = "";

	public void parse(Context context, String createUrl) {
		this.mContext = context;
		ServiceKPIReportAsync mServiceKPIReport = new ServiceKPIReportAsync(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mServiceKPIReport.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mServiceKPIReport.execute();
		}
	}

	private class ServiceKPIReportAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;
		private ArrayList<TrackResult> SearchResults;
		private String CREATEURL;
		private boolean isTimeOut = false;

		public ServiceKPIReportAsync(Context mContext, String createUrl) {
			this.context = mContext;
			this.CREATEURL = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				String[] responsedata = Util.sendGet(CREATEURL);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				// authenticationJson = Util.readXMLinString("trackresult.json",
				// context);

				if (authenticationJson != null && !authenticationJson.equals("")) {
					SearchResults = ParseData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);
			if (isTimeOut) {
				if (trackshipmentparserinterface != null) {
					trackshipmentparserinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (trackshipmentparserinterface != null) {
						trackshipmentparserinterface.OnSuccess(SearchResults);
					}
				} else if (responsecode.equals("1002")) {
					if (trackshipmentparserinterface != null) {
						trackshipmentparserinterface.NoData();
					}
				} else {
					if (trackshipmentparserinterface != null) {
						trackshipmentparserinterface.OnError();
					}
				}
			}

		}
	}

	private ArrayList<TrackResult> ParseData(String responseJSOn) {
		ArrayList<TrackResult> TrackResults = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.getString(RESPONSECODE);
			responsedetails = jsObg.getString(STATUS);

			if (responsecode.equals("200")) {
				JSONArray mJSONArray = jsObg.getJSONArray(TRACKRESULT);

				if (mJSONArray != null && mJSONArray.length() >= 1) {
					TrackResults = new ArrayList<TrackResult>();

					for (int i = 0; i < mJSONArray.length(); i++) {

						JSONObject ResultObj = mJSONArray.getJSONObject(i);
						TrackResult mTrackResult = new TrackResult();

						mTrackResult.setCarrierName(Util.getJsonValue(ResultObj, CARRIERNAME));
						mTrackResult.setBookingNo(Util.getJsonValue(ResultObj, BOOKINGNO));
						mTrackResult.setContainerNo(Util.getJsonValue(ResultObj, CONTAINERNO));

						mTrackResult.setPOL(Util.getJsonValue(ResultObj, POL));
						mTrackResult.setPOD(Util.getJsonValue(ResultObj, POD));
						mTrackResult.setEstimatedETD(Util.getJsonValue(ResultObj, ESTIMATEDETD));
						mTrackResult.setActualETD(Util.getJsonValue(ResultObj, ACTUALETD));
						mTrackResult.setEstimatedETA(Util.getJsonValue(ResultObj, ESTIMATEDETA));
						mTrackResult.setActualETA(Util.getJsonValue(ResultObj, ACTUALETA));

						try {
							JSONArray DetailsARRAY = ResultObj.getJSONArray(DETAILS);
							ArrayList<TrackResultDetail> TrackResultDetails = new ArrayList<TrackResultDetail>();

							if (DetailsARRAY != null && DetailsARRAY.length() >= 1) {
								TrackResultDetails = new ArrayList<TrackResultDetail>();

								for (int k = 0; k < DetailsARRAY.length(); k++) {
									JSONObject DetailObj = DetailsARRAY.getJSONObject(k);
									TrackResultDetail mTrackResultDetail = new TrackResultDetail();

									mTrackResultDetail.setVesselName(Util.getJsonValue(DetailObj, VESSELNAME));
									mTrackResultDetail.setDate(Util.getJsonValue(DetailObj, DETAILS_DATE));
									mTrackResultDetail.setStatusDescription(Util.getJsonValue(DetailObj, STATUSDESCRIPTION));
									mTrackResultDetail.setVoyage(Util.getJsonValue(DetailObj, VOYAGE));
									mTrackResultDetail.setLocation(Util.getJsonValue(DetailObj, LOCATION));

									TrackResultDetails.add(mTrackResultDetail);
								}

								if (TrackResultDetails != null && TrackResultDetails.size() >= 1) {
									Collections.sort(TrackResultDetails, new Comparator<TrackResultDetail>() {

										@Override
										public int compare(TrackResultDetail lhs, TrackResultDetail rhs) {
											return getTimeStamp(lhs.getDate()).compareTo(getTimeStamp(rhs.getDate()));
										}
									});
								}
							}
							mTrackResult.setDetails(TrackResultDetails);
						} catch (Exception e) {
							e.printStackTrace();
						}
						TrackResults.add(mTrackResult);
					}
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return TrackResults;
	}

	private String getTimeStamp(String str_date) {
		String timestamp = str_date;

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
			Date date = (Date) formatter.parse(str_date);
			return String.valueOf(date.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.valueOf(timestamp);
	}

	public TrackShipmentParserInterface trackshipmentparserinterface;

	public TrackShipmentParserInterface getTrackshipmentparserinterface() {
		return trackshipmentparserinterface;
	}

	public void setTrackshipmentparserinterface(TrackShipmentParserInterface trackshipmentparserinterface) {
		this.trackshipmentparserinterface = trackshipmentparserinterface;
	}

	public interface TrackShipmentParserInterface {
		public void OnSuccess(ArrayList<TrackResult> TrackResults);

		public void OnError();

		public void NoData();
	}
}
