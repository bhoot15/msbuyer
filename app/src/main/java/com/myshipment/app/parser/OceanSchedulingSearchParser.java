package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.OceanScheduleSearchDetails;

public class OceanSchedulingSearchParser {

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";

	private Context mContext;
	private String responsecode = "";

	private String SearchResult = "SearchResult";
	private String TransitionResult = "TransitionResult";

	private String NAME = "Name";
	private String IMAGE_LINK = "image_link";
	private String VESSELCODE = "VesselCode";
	private String VOYAGECODE = "VoyageCode";
	private String PORTCALL = "PortCall";
	private String DAYS = "Days";
	private String ADDRESS = "Address";
	private String ARRIVEDATE = "arriveDate";
	private String DEPARTDATE = "departDate";
	private String ROOTORIGIN = "RootOrigin";
	private String ROOTDESTINATION = "RootDestination";

	private String CHILD_DEPARTDATE = "departDate";
	private String CHILD_ARRIVEDATE = "arriveDate";
	private String CHILD_DEPARTING = "Departing";
	private String CHILD_ARRIVING = "Arriving";

	/* Return Data Sets */
	private ArrayList<OceanScheduleSearchDetails> searchList = null;

	private String searchUrl = "";

	public void setSearchUrl(String searchUrl) {
		this.searchUrl = searchUrl;
	}

	public void parse(Context context) {
		this.mContext = context;

		Async mAsync = new Async(mContext);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mAsync.execute();
		}
	}

	private class Async extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;

		private boolean isTimeOut = false;

		public Async(Context mContext) {
			this.context = mContext;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(searchUrl);
				authenticationJson = responsedata[1];
				// isTimeOut = false;
				// authenticationJson =
				// Util.readXMLinString("oceanscheduling.json", context);
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				if (authenticationJson != null && !authenticationJson.equals("")) {
					parseData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);

			if (isTimeOut) {
				if (oceanschedulingsearchparserinterface != null) {
					oceanschedulingsearchparserinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (oceanschedulingsearchparserinterface != null) {
						oceanschedulingsearchparserinterface.ONSuccess(searchList);
					}
				} else if (responsecode.equals("1002")) {
					if (oceanschedulingsearchparserinterface != null) {
						oceanschedulingsearchparserinterface.NoData();
					}
				} else {
					if (oceanschedulingsearchparserinterface != null) {
						oceanschedulingsearchparserinterface.OnError();
					}
				}
			}
		}
	}

	private void parseData(String responseJSOn) {

		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.optString(RESPONSECODE);

			JSONArray searchResult = jsObg.getJSONArray(SearchResult);
			if (searchResult != null && searchResult.length() >= 1) {
				searchList = new ArrayList<OceanScheduleSearchDetails>();
				for (int i = 0; i < searchResult.length(); i++) {
					JSONObject result = searchResult.getJSONObject(i);
					OceanScheduleSearchDetails mOceanScheduleSearchDetails = new OceanScheduleSearchDetails();

					mOceanScheduleSearchDetails.setName(Util.getJsonValue(result, NAME));
					mOceanScheduleSearchDetails.setImage_link(Util.getJsonValue(result, IMAGE_LINK));
					mOceanScheduleSearchDetails.setVesselcode(Util.getJsonValue(result, VESSELCODE));
					mOceanScheduleSearchDetails.setVoyagecode(Util.getJsonValue(result, VOYAGECODE));
					mOceanScheduleSearchDetails.setPortcall(Util.getJsonValue(result, PORTCALL));
					mOceanScheduleSearchDetails.setDays(Util.getJsonValue(result, DAYS));
					mOceanScheduleSearchDetails.setAddress(Util.getJsonValue(result, ADDRESS));
					mOceanScheduleSearchDetails.setArrivedate(Util.getOceanSearchData(Util.getJsonValue(result, ARRIVEDATE)));
					mOceanScheduleSearchDetails.setDepartdate(Util.getOceanSearchData(Util.getJsonValue(result, DEPARTDATE)));
					mOceanScheduleSearchDetails.setRootorigin(Util.getJsonValue(result, ROOTORIGIN));
					mOceanScheduleSearchDetails.setRootdestination(Util.getJsonValue(result, ROOTDESTINATION));

					JSONArray transhipmentResult = result.getJSONArray(TransitionResult);
					if (transhipmentResult != null && transhipmentResult.length() >= 1) {
						ArrayList<OceanScheduleSearchDetails> transhipmentList = new ArrayList<OceanScheduleSearchDetails>();

						for (int j = 0; j < transhipmentResult.length(); j++) {

							JSONObject transhipentObj = transhipmentResult.getJSONObject(j);
							OceanScheduleSearchDetails Details = new OceanScheduleSearchDetails();

							Details.setChild_arrivedate(Util.getOceanSearchData(Util.getJsonValue(transhipentObj, CHILD_ARRIVEDATE)));
							Details.setChild_departdate(Util.getOceanSearchData(Util.getJsonValue(transhipentObj, CHILD_DEPARTDATE)));
							Details.setChild_arriving(Util.getJsonValue(transhipentObj, CHILD_ARRIVING));
							Details.setChild_departing(Util.getJsonValue(transhipentObj, CHILD_DEPARTING));

							transhipmentList.add(Details);
						}
						mOceanScheduleSearchDetails.setTranshipmentResult(transhipmentList);
					}
					searchList.add(mOceanScheduleSearchDetails);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public OceanSchedulingSearchParserInterface oceanschedulingsearchparserinterface;

	public OceanSchedulingSearchParserInterface getOceanschedulingsearchparserinterface() {
		return oceanschedulingsearchparserinterface;
	}

	public void setOceanschedulingsearchparserinterface(OceanSchedulingSearchParserInterface oceanschedulingsearchparserinterface) {
		this.oceanschedulingsearchparserinterface = oceanschedulingsearchparserinterface;
	}

	public interface OceanSchedulingSearchParserInterface {
		public void ONSuccess(ArrayList<OceanScheduleSearchDetails> searchList);

		public void NoData();

		public void OnError();
	}

}
