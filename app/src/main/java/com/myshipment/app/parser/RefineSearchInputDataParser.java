package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.SpinnerDataSet;

public class RefineSearchInputDataParser {

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";

	private String PODLIST = "PODlist";
	private String ILNNAMELIST = "ILNNameList";
	private String SHIPPINGLINE = "ShippingLine";
	private String POLLIST = "POLlist";

	private String POLNAME = "POLName";
	private String SHIPPINGLINENAME = "ShippingLineName";
	private String ILNNAME = "ILNName";
	private String PODNAME = "PODName";
	private String CODE = "Code";

	private Context mContext;
	private String responsecode = "";

	public void parse(Context context, String createUrl) {
		this.mContext = context;

		IncidentReportParserAsync mIncidentReportParserAsync = new IncidentReportParserAsync(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mIncidentReportParserAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mIncidentReportParserAsync.execute();
		}
	}

	private ArrayList<ArrayList<SpinnerDataSet>> ParseTheData(String responseJSOn) {

		ArrayList<ArrayList<SpinnerDataSet>> refineDropdownListData = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.optString(RESPONSECODE);
			if (responsecode.equals("200")) {

				refineDropdownListData = new ArrayList<ArrayList<SpinnerDataSet>>();

				JSONArray shippinglineJsonArray = jsObg.getJSONArray(SHIPPINGLINE);
				JSONArray pollistJsonArray = jsObg.getJSONArray(POLLIST);
				JSONArray podlistJsonArray = jsObg.getJSONArray(PODLIST);
				JSONArray ilnnamelistJsonArray = jsObg.getJSONArray(ILNNAMELIST);

				ArrayList<SpinnerDataSet> shippingline = new ArrayList<SpinnerDataSet>();
				for (int i = 0; i < shippinglineJsonArray.length(); i++) {
					JSONObject level = shippinglineJsonArray.getJSONObject(i);
					SpinnerDataSet mDataSet = new SpinnerDataSet();
					mDataSet.setId(Util.getJSONKeyvalue(level, CODE));
					mDataSet.setName(Util.getJSONKeyvalue(level, SHIPPINGLINENAME));
					shippingline.add(mDataSet);
				}

				ArrayList<SpinnerDataSet> pollist = new ArrayList<SpinnerDataSet>();
				for (int i = 0; i < pollistJsonArray.length(); i++) {
					JSONObject level = pollistJsonArray.getJSONObject(i);
					SpinnerDataSet mDataSet = new SpinnerDataSet();
					mDataSet.setId(Util.getJSONKeyvalue(level, CODE));
					mDataSet.setName(Util.getJSONKeyvalue(level, POLNAME));
					pollist.add(mDataSet);
				}

				ArrayList<SpinnerDataSet> podlist = new ArrayList<SpinnerDataSet>();
				for (int i = 0; i < podlistJsonArray.length(); i++) {
					JSONObject level = podlistJsonArray.getJSONObject(i);
					SpinnerDataSet mDataSet = new SpinnerDataSet();
					mDataSet.setId(Util.getJSONKeyvalue(level, CODE));
					mDataSet.setName(Util.getJSONKeyvalue(level, PODNAME));
					podlist.add(mDataSet);
				}

				ArrayList<SpinnerDataSet> ilnnamelist = new ArrayList<SpinnerDataSet>();
				for (int i = 0; i < ilnnamelistJsonArray.length(); i++) {
					JSONObject level = ilnnamelistJsonArray.getJSONObject(i);
					SpinnerDataSet mDataSet = new SpinnerDataSet();
					mDataSet.setId(Util.getJSONKeyvalue(level, CODE));
					mDataSet.setName(Util.getJSONKeyvalue(level, ILNNAME));
//					mDataSet.setName(Util.getJSONKeyvalue(level, ILNNAME) + " - " + mDataSet.getId());
					ilnnamelist.add(mDataSet);
				}

				refineDropdownListData.add(shippingline);
				refineDropdownListData.add(pollist);
				refineDropdownListData.add(podlist);
				refineDropdownListData.add(ilnnamelist);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return refineDropdownListData;
	}

	private class IncidentReportParserAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String inputUrl;
		private String authenticationJson;
		private ArrayList<ArrayList<SpinnerDataSet>> refineDropdownListData;
		private boolean isTimeOut = false;

		public IncidentReportParserAsync(Context mContext, String createUrl) {
			this.context = mContext;
			this.inputUrl = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(inputUrl);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				if (authenticationJson != null && !authenticationJson.equals("")) {
					refineDropdownListData = ParseTheData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);

			if (isTimeOut) {
				if (refineSearchInputDataParserInterface != null) {
					refineSearchInputDataParserInterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (refineSearchInputDataParserInterface != null) {
						refineSearchInputDataParserInterface.ONSuccess(refineDropdownListData);
					}
				} else if (responsecode.equals("1002")) {
					if (refineSearchInputDataParserInterface != null) {
						refineSearchInputDataParserInterface.NoData();
					}
				} else {
					if (refineSearchInputDataParserInterface != null) {
						refineSearchInputDataParserInterface.OnError();
					}
				}
			}
		}
	}

	public RefineSearchInputDataParserInterface refineSearchInputDataParserInterface;

	public RefineSearchInputDataParserInterface getRefineSearchInputDataParserInterface() {
		return refineSearchInputDataParserInterface;
	}

	public void setRefineSearchInputDataParserInterface(RefineSearchInputDataParserInterface refineSearchInputDataParserInterface) {
		this.refineSearchInputDataParserInterface = refineSearchInputDataParserInterface;
	}

	public interface RefineSearchInputDataParserInterface {
		public void ONSuccess(ArrayList<ArrayList<SpinnerDataSet>> refineDropdownListData);

		public void NoData();

		public void OnError();
	}

}
