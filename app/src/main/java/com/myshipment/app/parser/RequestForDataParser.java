package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.SpinnerDataSet;

public class RequestForDataParser {

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";

	private String REQUESTTYPE = "RequestType";
	private String NAME = "Name";
	private String CODE = "Code";

	private Context mContext;
	private String responsecode = "";

	public void parse(Context context, String createUrl) {
		this.mContext = context;

		IncidentReportParserAsync mIncidentReportParserAsync = new IncidentReportParserAsync(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mIncidentReportParserAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mIncidentReportParserAsync.execute();
		}
	}

	private ArrayList<SpinnerDataSet> ParseTheData(String responseJSOn) {

		ArrayList<SpinnerDataSet> requestDropdownListData = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.optString(RESPONSECODE);
			if (responsecode.equals("200")) {

				requestDropdownListData = new ArrayList<SpinnerDataSet>();

				JSONArray requesttypeJsonArray = jsObg.getJSONArray(REQUESTTYPE);

				SpinnerDataSet emptySta = new SpinnerDataSet();
				emptySta.setId("-1");
				emptySta.setName("Select request type");
				requestDropdownListData.add(emptySta);
				for (int i = 0; i < requesttypeJsonArray.length(); i++) {
					JSONObject level = requesttypeJsonArray.getJSONObject(i);
					SpinnerDataSet mDataSet = new SpinnerDataSet();
					mDataSet.setId(Util.getJSONKeyvalue(level, CODE));
					mDataSet.setName(Util.getJSONKeyvalue(level, NAME));
					requestDropdownListData.add(mDataSet);
				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return requestDropdownListData;
	}

	private class IncidentReportParserAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String inputUrl;
		private String authenticationJson;
		private ArrayList<SpinnerDataSet> dropdownData;
		private boolean isTimeOut = false;

		public IncidentReportParserAsync(Context mContext, String createUrl) {
			this.context = mContext;
			this.inputUrl = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(inputUrl);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				if (authenticationJson != null && !authenticationJson.equals("")) {
					dropdownData = ParseTheData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);

			if (isTimeOut) {
				if (requestTypeDropdownParserInterface != null) {
					requestTypeDropdownParserInterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (requestTypeDropdownParserInterface != null) {
						requestTypeDropdownParserInterface.ONSuccess(dropdownData);
					}
				} else if (responsecode.equals("1002")) {
					if (requestTypeDropdownParserInterface != null) {
						requestTypeDropdownParserInterface.NoData();
					}
				} else {
					if (requestTypeDropdownParserInterface != null) {
						requestTypeDropdownParserInterface.OnError();
					}
				}
			}
		}
	}

	public RequestTypeDropdownParserInterface requestTypeDropdownParserInterface;

	public RequestTypeDropdownParserInterface getRequestTypeDropdownParserInterface() {
		return requestTypeDropdownParserInterface;
	}

	public void setRequestTypeDropdownParserInterface(RequestTypeDropdownParserInterface requestTypeDropdownParserInterface) {
		this.requestTypeDropdownParserInterface = requestTypeDropdownParserInterface;
	}

	public interface RequestTypeDropdownParserInterface {
		public void ONSuccess(ArrayList<SpinnerDataSet> incidentDropdownData);

		public void NoData();

		public void OnError();
	}

}
