package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.Util.ShipmentClickState;
import com.myshipment.app.databasehelper.PieChartFlowTableHelper;
import com.myshipment.app.model.Flow;

public class ShipmentFlowParser {

	private Context mContext;

	private String fromdate = "";
	private String todate = "";
	private String isOrigin = "";
	private String carriername = "";
	private String ilncode = "";
	private PieChartFlowTableHelper mFlowTableHelper;

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";

	/* For Total Shipment Flow */
	private String TOTAL_FLOW = "Flow";
	private String TOTAL_FLOWID = "FlowID";
	private String TOTAL_FLOW_NAME = "Flow Name";
	private String TOTAL_NO_OF_SHIPMENT = "No Of Shipment";
	private String TOTAL_OVERALL_PERCENTAGE = "Overall Percentage";
	private String TOTAL_RGB = "RGB";

	/* For Delay Shipment Flow */
	private String DELAY_FLOW = "Flow";
	private String DELAY_FLOWID = "FlowID";
	private String DELAY_FLOW_NAME = "Flow Name";
	private String DELAY_NO_OF_SHIPMENT = "No Of Shipment";
	private String DELAY_NO_OF_DELAY_SHIPMENT = "No Of delay Shipment";
	private String DELAY_OVERALL_PERCENTAGE = "Overall Percentage";
	private String DELAY_INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String DELAY_RGB = "RGB";

	/* For InTransit Shipment Flow */
	private String INTRANSIT_FLOW = "Flow";
	private String INTRANSIT_FLOWID = "FlowID";
	private String INTRANSIT_FLOW_NAME = "Flow Name";
	private String INTRANSIT_NO_OF_SHIPMENT = "No Of Shipment";
	private String INTRANSIT_NO_OF_INTRANSIT_SHIPMENT = "No Of Intransit Shipment";
	private String INTRANSIT_OVERALL_PERCENTAGE = "Overall Percentage";
	private String INTRANSIT_INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String INTRANSIT_RGB = "RGB";

	/* For PotentialDelay Shipment Flow */
	private String POTENTIALDELAY_FLOW = "Flow";
	private String POTENTIALDELAY_FLOWID = "FlowID";
	private String POTENTIALDELAY_FLOW_NAME = "Flow Name";
	private String POTENTIALDELAY_NO_OF_SHIPMENT = "No Of Shipment";
	private String POTENTIALDELAY_NO_OF_ON_TIME_SHIPMENT = "No Of On Time Shipment";
	private String POTENTIALDELAY_OVERALL_PERCENTAGE = "Overall Percentage";
	private String POTENTIALDELAY_INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String POTENTIALDELAY_RGB = "RGB";
	/* Response JSON key value */
	private String responsecode = "";

	public void parse(Context context, String createUrl, ShipmentClickState mShipmentClickState, String fromdate_, String todate_, String isOrigin_, String carriername_, String ILNCode_) {
		this.mContext = context;

		this.fromdate = fromdate_;
		this.todate = todate_;
		this.isOrigin = isOrigin_;
		this.carriername = carriername_;
		this.ilncode = ILNCode_;
		this.mFlowTableHelper = PieChartFlowTableHelper.getInstance(mContext);

		FlowParserAsync mFlowParserAsync = new FlowParserAsync(mContext, createUrl, mShipmentClickState);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mFlowParserAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mFlowParserAsync.execute();
		}
	}

	public String[] getListViewFieldName() {
		String[] fields = new String[3];
		fields[0] = TOTAL_FLOW_NAME;
		fields[1] = TOTAL_NO_OF_SHIPMENT;
		fields[2] = TOTAL_OVERALL_PERCENTAGE;
		return fields;
	}

	public String[] getListViewFieldNameForDelay() {
		String[] fields = new String[5];
		fields[0] = DELAY_FLOW_NAME;
		fields[1] = DELAY_NO_OF_SHIPMENT;
		fields[2] = DELAY_NO_OF_DELAY_SHIPMENT;
		fields[3] = DELAY_OVERALL_PERCENTAGE;
		fields[4] = DELAY_INDIVIDUAL_PERCENTAGE;
		return fields;
	}

	public String[] getListViewFieldNameInTrans() {
		String[] fields = new String[5];
		fields[0] = INTRANSIT_FLOW_NAME;
		fields[1] = INTRANSIT_NO_OF_SHIPMENT;
		fields[2] = INTRANSIT_NO_OF_INTRANSIT_SHIPMENT;
		fields[3] = INTRANSIT_OVERALL_PERCENTAGE;
		fields[4] = INTRANSIT_INDIVIDUAL_PERCENTAGE;
		return fields;
	}

	public String[] getListViewFieldNamePotentialDelay() {
		String[] fields = new String[5];
		fields[0] = POTENTIALDELAY_FLOW_NAME;
		fields[1] = POTENTIALDELAY_NO_OF_SHIPMENT;
		fields[2] = POTENTIALDELAY_NO_OF_ON_TIME_SHIPMENT;
		fields[3] = POTENTIALDELAY_OVERALL_PERCENTAGE;
		fields[4] = POTENTIALDELAY_INDIVIDUAL_PERCENTAGE;
		return fields;
	}

	private class FlowParserAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;
		private ArrayList<Flow> FlowS;
		private String createUrl_;
		private ShipmentClickState State;
		private boolean isTimeOut = false;

		public FlowParserAsync(Context mContext, String createUrl, ShipmentClickState mShipmentClickState) {
			this.context = mContext;
			this.createUrl_ = createUrl;
			this.State = mShipmentClickState;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				String[] responsedata = Util.sendGet(createUrl_);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				if (authenticationJson != null && !authenticationJson.equals("")) {
					JSONObject jsObg = new JSONObject(authenticationJson);
					responsecode = jsObg.getString(RESPONSECODE);
					if (responsecode.equals("200")) {
						if (State == ShipmentClickState.Shipments) {
							FlowS = ShipmentFlowData(jsObg);
						} else if (State == ShipmentClickState.Delayed) {
							FlowS = DelayFlowData(jsObg);
						} else if (State == ShipmentClickState.InTransit) {
							FlowS = InTransitFlowData(jsObg);
						} else if (State == ShipmentClickState.PotentialDelay) {
							FlowS = PotentialFlowData(jsObg);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);
			if (isTimeOut) {
				if (flowparserinterface != null) {
					flowparserinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (flowparserinterface != null) {
						flowparserinterface.ONSuccess(FlowS);
					}
				} else if (responsecode.equals("1002")) {
					if (flowparserinterface != null) {
						flowparserinterface.NoData();
					}
				} else {
					if (flowparserinterface != null) {
						flowparserinterface.OnError();
					}
				}
			}
		}
	}

	/* For Total Shipment Flow */
	private ArrayList<Flow> ShipmentFlowData(JSONObject jsObg) {
		ArrayList<Flow> Flows = null;
		try {
			JSONArray FlowsjsonArray = jsObg.getJSONArray(TOTAL_FLOW);
			if (FlowsjsonArray != null && FlowsjsonArray.length() >= 1) {
				Flows = new ArrayList<Flow>();
				for (int i = 0; i < FlowsjsonArray.length(); i++) {
					JSONObject flowjsonObg = FlowsjsonArray.getJSONObject(i);
					Flow mFlow = new Flow();
					mFlow.setFlowID(Util.getJSONKeyvalue(flowjsonObg, TOTAL_FLOWID));
					mFlow.setFlow_Name(Util.getJSONKeyvalue(flowjsonObg, TOTAL_FLOW_NAME));
					mFlow.setNo_Of_Shipment(Util.getJSONKeyvalue(flowjsonObg, TOTAL_NO_OF_SHIPMENT));
					mFlow.setOverall_Percentage(Util.getJSONKeyvalue(flowjsonObg, TOTAL_OVERALL_PERCENTAGE));
					mFlow.setRGB(Util.getJSONKeyvalue(flowjsonObg, TOTAL_RGB));
					mFlow.setCarrierDetails(null);

					mFlow.setFromdate(fromdate);
					mFlow.setTodate(todate);
					mFlow.setType("");
					mFlow.setIsOrigin(isOrigin);
					mFlow.setCarriername(carriername);
					mFlow.setILNCode(ilncode);

					/** Database insertion **/
					mFlowTableHelper.storeGraphOVData(mFlow);
					/***************************/

					Flows.add(mFlow);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return Flows;
	}

	/* For Total Shipment Flow */

	/* For Delay Shipment Flow */
	private ArrayList<Flow> DelayFlowData(JSONObject jsObg) {

		ArrayList<Flow> Flows = null;
		try {
			JSONArray FlowsjsonArray = jsObg.getJSONArray(DELAY_FLOW);
			if (FlowsjsonArray != null && FlowsjsonArray.length() >= 1) {
				Flows = new ArrayList<Flow>();
				for (int i = 0; i < FlowsjsonArray.length(); i++) {
					JSONObject flowjsonObg = FlowsjsonArray.getJSONObject(i);
					Flow mFlow = new Flow();
					mFlow.setFlowID(Util.getJSONKeyvalue(flowjsonObg, DELAY_FLOWID));
					mFlow.setFlow_Name(Util.getJSONKeyvalue(flowjsonObg, DELAY_FLOW_NAME));
					mFlow.setNo_Of_Shipment(Util.getJSONKeyvalue(flowjsonObg, DELAY_NO_OF_SHIPMENT));
					mFlow.setNo_Of__Shipment(Util.getJSONKeyvalue(flowjsonObg, DELAY_NO_OF_DELAY_SHIPMENT));
					mFlow.setOverall_Percentage(Util.getJSONKeyvalue(flowjsonObg, DELAY_OVERALL_PERCENTAGE));
					mFlow.setIndividual_Percentage(Util.getJSONKeyvalue(flowjsonObg, DELAY_INDIVIDUAL_PERCENTAGE));
					mFlow.setRGB(Util.getJSONKeyvalue(flowjsonObg, DELAY_RGB));
					mFlow.setCarrierDetails(null);

					mFlow.setFromdate(fromdate);
					mFlow.setTodate(todate);
					mFlow.setType("");
					mFlow.setIsOrigin(isOrigin);
					mFlow.setCarriername(carriername);
					mFlow.setILNCode(ilncode);

					/** Database insertion **/
					mFlowTableHelper.storeGraphOVData(mFlow);
					/***************************/

					Flows.add(mFlow);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return Flows;

	}

	/* For Delay Shipment Flow */

	/* For InTransit Shipment Flow */
	private ArrayList<Flow> InTransitFlowData(JSONObject jsObg) {

		ArrayList<Flow> Flows = null;
		try {
			JSONArray FlowsjsonArray = jsObg.getJSONArray(INTRANSIT_FLOW);
			if (FlowsjsonArray != null && FlowsjsonArray.length() >= 1) {
				Flows = new ArrayList<Flow>();
				for (int i = 0; i < FlowsjsonArray.length(); i++) {
					JSONObject flowjsonObg = FlowsjsonArray.getJSONObject(i);
					Flow mFlow = new Flow();
					mFlow.setFlowID(Util.getJSONKeyvalue(flowjsonObg, INTRANSIT_FLOWID));
					mFlow.setFlow_Name(Util.getJSONKeyvalue(flowjsonObg, INTRANSIT_FLOW_NAME));
					mFlow.setNo_Of_Shipment(Util.getJSONKeyvalue(flowjsonObg, INTRANSIT_NO_OF_SHIPMENT));
					mFlow.setNo_Of__Shipment(Util.getJSONKeyvalue(flowjsonObg, INTRANSIT_NO_OF_INTRANSIT_SHIPMENT));
					mFlow.setOverall_Percentage(Util.getJSONKeyvalue(flowjsonObg, INTRANSIT_OVERALL_PERCENTAGE));
					mFlow.setIndividual_Percentage(Util.getJSONKeyvalue(flowjsonObg, INTRANSIT_INDIVIDUAL_PERCENTAGE));
					mFlow.setRGB(Util.getJSONKeyvalue(flowjsonObg, INTRANSIT_RGB));
					mFlow.setCarrierDetails(null);

					mFlow.setFromdate(fromdate);
					mFlow.setTodate(todate);
					mFlow.setType("");
					mFlow.setIsOrigin(isOrigin);
					mFlow.setCarriername(carriername);
					mFlow.setILNCode(ilncode);

					/** Database insertion **/
					mFlowTableHelper.storeGraphOVData(mFlow);
					/***************************/

					Flows.add(mFlow);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return Flows;

	}

	/* For InTransit Shipment Flow */

	/* For Potential Shipment Flow */
	private ArrayList<Flow> PotentialFlowData(JSONObject jsObg) {
		ArrayList<Flow> Flows = null;
		try {
			JSONArray FlowsjsonArray = jsObg.getJSONArray(POTENTIALDELAY_FLOW);
			if (FlowsjsonArray != null && FlowsjsonArray.length() >= 1) {
				Flows = new ArrayList<Flow>();
				for (int i = 0; i < FlowsjsonArray.length(); i++) {
					JSONObject flowjsonObg = FlowsjsonArray.getJSONObject(i);
					Flow mFlow = new Flow();
					mFlow.setFlowID(Util.getJSONKeyvalue(flowjsonObg, POTENTIALDELAY_FLOWID));
					mFlow.setFlow_Name(Util.getJSONKeyvalue(flowjsonObg, POTENTIALDELAY_FLOW_NAME));
					mFlow.setNo_Of_Shipment(Util.getJSONKeyvalue(flowjsonObg, POTENTIALDELAY_NO_OF_SHIPMENT));
					mFlow.setNo_Of__Shipment(Util.getJSONKeyvalue(flowjsonObg, POTENTIALDELAY_NO_OF_ON_TIME_SHIPMENT));
					mFlow.setOverall_Percentage(Util.getJSONKeyvalue(flowjsonObg, POTENTIALDELAY_OVERALL_PERCENTAGE));
					mFlow.setIndividual_Percentage(Util.getJSONKeyvalue(flowjsonObg, POTENTIALDELAY_INDIVIDUAL_PERCENTAGE));
					mFlow.setRGB(Util.getJSONKeyvalue(flowjsonObg, POTENTIALDELAY_RGB));
					mFlow.setCarrierDetails(null);

					mFlow.setFromdate(fromdate);
					mFlow.setTodate(todate);
					mFlow.setType("");
					mFlow.setIsOrigin(isOrigin);
					mFlow.setCarriername(carriername);
					mFlow.setILNCode(ilncode);

					/** Database insertion **/
					mFlowTableHelper.storeGraphOVData(mFlow);
					/***************************/

					Flows.add(mFlow);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return Flows;
	}

	/* For Potential Shipment Flow */

	public FlowParserInterface flowparserinterface;

	public FlowParserInterface getFlowparserinterface() {
		return flowparserinterface;
	}

	public void setFlowparserinterface(FlowParserInterface flowparserinterface) {
		this.flowparserinterface = flowparserinterface;
	}

	public interface FlowParserInterface {
		public void ONSuccess(ArrayList<Flow> FlowS);

		public void OnError();

		public void NoData();
	}
}
