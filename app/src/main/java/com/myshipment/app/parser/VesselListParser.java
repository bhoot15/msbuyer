package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.app.Util;
import com.myshipment.app.model.SpinnerDataSet;

public class VesselListParser {
	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";
	private String VESSELLIST = "VesselList";
	private String VESSELNAME = "VesselName";

	private Context mContext;
	private String responsecode = "";

	public void parse(Context context, String createUrl) {
		this.mContext = context;

		Async mAsync = new Async(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mAsync.execute();
		}
	}

	private class Async extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String inputUrl;
		private String authenticationJson;
		private ArrayList<SpinnerDataSet> vessellist;
		private boolean isTimeOut = false;

		public Async(Context mContext, String createUrl) {
			this.context = mContext;
			this.inputUrl = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
//			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(inputUrl);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;
				if (authenticationJson != null && !authenticationJson.equals("")) {
					vessellist = ParseTheData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);

			if (isTimeOut) {
				if (vessellistinterface != null) {
					vessellistinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (vessellistinterface != null) {
						vessellistinterface.ONSuccess(vessellist);
					}
				} else if (responsecode.equals("1002")) {
					if (vessellistinterface != null) {
						vessellistinterface.NoData();
					}
				} else {
					if (vessellistinterface != null) {
						vessellistinterface.OnError();
					}
				}
			}
		}
	}

	private ArrayList<SpinnerDataSet> ParseTheData(String responseJSOn) {

		ArrayList<SpinnerDataSet> incidentRepors = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.optString(RESPONSECODE);
			if (responsecode.equals("200")) {
				JSONArray origAnddestiJsonArray = jsObg.getJSONArray(VESSELLIST);
				incidentRepors = new ArrayList<SpinnerDataSet>();
				for (int i = 0; i < origAnddestiJsonArray.length(); i++) {
					SpinnerDataSet mData = new SpinnerDataSet();
					JSONObject incidentjsonObj = origAnddestiJsonArray.getJSONObject(i);
					mData.setName(Util.getJSONKeyvalue(incidentjsonObj, VESSELNAME));
					incidentRepors.add(mData);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return incidentRepors;
	}

	public VesselListInterface vessellistinterface;

	public VesselListInterface getVessellistinterface() {
		return vessellistinterface;
	}

	public void setVessellistinterface(VesselListInterface vessellistinterface) {
		this.vessellistinterface = vessellistinterface;
	}

	public interface VesselListInterface {
		public void ONSuccess(ArrayList<SpinnerDataSet> vessellist);

		public void NoData();

		public void OnError();
	}
}
