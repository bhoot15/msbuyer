package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.Util.ShipmentClickState;
import com.myshipment.app.databasehelper.PieChartCarrierTableHelper;
import com.myshipment.app.model.CarrierData;

public class ShipmentParser {

	private Context mContext;

	private String fromdate;
	private String todate;
	private String isOrigin;
	private PieChartCarrierTableHelper mPieChartCarrierTableHelper = null;

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";

	/* For Total Shipment */
	private String TOTAL_NOOFSHIPMENT = "No Of Shipment";
	private String TOTAL_CARRIER = "Carrier";
	private String TOTAL_CARRIERID = "CarrierID";
	private String TOTAL_CARRIER_NAME = "Carrier Name";
	private String TOTAL_NO_OF_SHIPMENT = "No Of Shipment";
	private String TOTAL_OVERALL_PERCENTAGE = "Overall Percentage";
	private String TOTAL_RGB = "RGB";

	/* For Delay Shipment */
	private String DELAY_DELAYSHIPMENT = "DelayShipment";
	private String DELAY_CARRIER = "Carrier";
	private String DELAY_CARRIERID = "CarrierID";
	private String DELAY_CARRIER_NAME = "Carrier Name";
	private String DELAY_NO_OF_SHIPMENT = "No Of Shipment";
	private String DELAY_NO_OF_DELAY_SHIPMENT = "No Of delay Shipment";
	private String DELAY_OVERALL_PERCENTAGE = "Overall Percentage";
	private String DELAY_INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String DELAY_RGB = "RGB";

	/* For InTransit Shipment */
	private String INTRANSIT_INTRANSITSHIPMENT = "InTransitShipment";
	private String INTRANSIT_CARRIER = "Carrier";
	private String INTRANSIT_CARRIERID = "CarrierID";
	private String INTRANSIT_CARRIER_NAME = "Carrier Name";
	private String INTRANSIT_NO_OF_SHIPMENT = "No Of Shipment";
	private String INTRANSIT_NO_OF_INTRANSIT_SHIPMENT = "No Of Intransit Shipment";
	private String INTRANSIT_OVERALL_PERCENTAGE = "Overall Percentage";
	private String INTRANSIT_INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String INTRANSIT_RGB = "RGB";

	/* For PotentialDelay Shipment */
	private String POTENTIALDELAY_POTENTIALDELAYSHIPMENT = "PotentialDelayShipment";
	private String POTENTIALDELAY_CARRIER = "Carrier";
	private String POTENTIALDELAY_CARRIERID = "CarrierID";
	private String POTENTIALDELAY_CARRIER_NAME = "Carrier Name";
	private String POTENTIALDELAY_NO_OF_SHIPMENT = "No Of Shipment";
	private String POTENTIALDELAY_NO_OF_ON_TIME_SHIPMENT = "No Of On Time Shipment";
	private String POTENTIALDELAY_OVERALL_PERCENTAGE = "Overall Percentage";
	private String POTENTIALDELAY_INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String POTENTIALDELAY_RGB = "RGB";

	/* Response JSON key value */
	private String responsecode = "";

	public void parse(Context context, String createUrl, ShipmentClickState mShipmentClickState, String fromdate_, String todate_, String isOrigin_) {
		this.mContext = context;

		this.fromdate = fromdate_;
		this.todate = todate_;
		this.isOrigin = isOrigin_;
		this.mPieChartCarrierTableHelper = PieChartCarrierTableHelper.getInstance(mContext);

		ShipmentAsync mShipmentAsync = new ShipmentAsync(mContext, createUrl, mShipmentClickState);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mShipmentAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mShipmentAsync.execute();
		}
	}

	public String[] getListViewFieldName() {
		String[] fields = new String[3];
		fields[0] = TOTAL_CARRIER_NAME;
		fields[1] = TOTAL_NO_OF_SHIPMENT;
		fields[2] = TOTAL_OVERALL_PERCENTAGE;
		return fields;
	}

	public String[] getListViewFieldNameForDelay() {
		String[] fields = new String[5];
		fields[0] = DELAY_CARRIER_NAME;
		fields[1] = DELAY_NO_OF_SHIPMENT;
		fields[2] = DELAY_NO_OF_DELAY_SHIPMENT;
		fields[3] = DELAY_OVERALL_PERCENTAGE;
		fields[4] = DELAY_INDIVIDUAL_PERCENTAGE;
		return fields;
	}

	public String[] getListViewFieldNameInTrans() {
		String[] fields = new String[5];
		fields[0] = INTRANSIT_CARRIER_NAME;
		fields[1] = INTRANSIT_NO_OF_SHIPMENT;
		fields[2] = INTRANSIT_NO_OF_INTRANSIT_SHIPMENT;
		fields[3] = INTRANSIT_OVERALL_PERCENTAGE;
		fields[4] = INTRANSIT_INDIVIDUAL_PERCENTAGE;
		return fields;
	}

	public String[] getListViewFieldNamePotentialDelay() {
		String[] fields = new String[5];
		fields[0] = POTENTIALDELAY_CARRIER_NAME;
		fields[1] = POTENTIALDELAY_NO_OF_SHIPMENT;
		fields[2] = POTENTIALDELAY_NO_OF_ON_TIME_SHIPMENT;
		fields[3] = POTENTIALDELAY_OVERALL_PERCENTAGE;
		fields[4] = POTENTIALDELAY_INDIVIDUAL_PERCENTAGE;
		return fields;
	}

	private class ShipmentAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;
		private ArrayList<CarrierData> CarrierDatas;
		private String createUrl_;
		private ShipmentClickState State;
		private boolean isTimeOut = false;

		public ShipmentAsync(Context mContext, String createUrl, ShipmentClickState mShipmentClickState) {
			this.context = mContext;
			this.createUrl_ = createUrl;
			this.State = mShipmentClickState;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				String[] responsedata = Util.sendGet(createUrl_);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;

				if (authenticationJson != null && !authenticationJson.equals("")) {
					JSONObject jsObg = new JSONObject(authenticationJson);
					responsecode = jsObg.getString(RESPONSECODE);
					if (responsecode.equals("200")) {
						if (State == ShipmentClickState.Shipments) {
							CarrierDatas = ParseTheData(jsObg);
						} else if (State == ShipmentClickState.Delayed) {
							CarrierDatas = ParseTheDataDelay(jsObg);
						} else if (State == ShipmentClickState.InTransit) {
							CarrierDatas = ParseTheDataInTransit(jsObg);
						} else if (State == ShipmentClickState.PotentialDelay) {
							CarrierDatas = ParseTheDataPotential(jsObg);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);

			if (isTimeOut) {
				if (shipmentparserinterface != null) {
					shipmentparserinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (shipmentparserinterface != null) {
						shipmentparserinterface.ONSuccess(CarrierDatas);
					}
				} else if (responsecode.equals("1002")) {
					if (shipmentparserinterface != null) {
						shipmentparserinterface.NoData();
					}
				} else {
					if (shipmentparserinterface != null) {
						shipmentparserinterface.OnError();
					}
				}
			}
		}
	}

	/* For Total Shipment */
	private ArrayList<CarrierData> ParseTheData(JSONObject jsObg) {
		ArrayList<CarrierData> carrierDatas = null;
		try {
			JSONObject noofShipmentObj = jsObg.getJSONObject(TOTAL_NOOFSHIPMENT);
			JSONArray CarrierjsonArray = noofShipmentObj.getJSONArray(TOTAL_CARRIER);
			carrierDatas = new ArrayList<CarrierData>();

			if (CarrierjsonArray != null && CarrierjsonArray.length() >= 1) {

				for (int i = 0; i < CarrierjsonArray.length(); i++) {

					JSONObject carrierjsonObj = CarrierjsonArray.getJSONObject(i);
					CarrierData mData = new CarrierData();
					mData.setCarrierID(Util.getJSONKeyvalue(carrierjsonObj, TOTAL_CARRIERID));
					mData.setCarrier_Name(Util.getJSONKeyvalue(carrierjsonObj, TOTAL_CARRIER_NAME));
					mData.setNo_Of_Shipment(Util.getJSONKeyvalue(carrierjsonObj, TOTAL_NO_OF_SHIPMENT));
					mData.setOverall_Percentage(Util.getJSONKeyvalue(carrierjsonObj, TOTAL_OVERALL_PERCENTAGE));
					mData.setRGB(Util.getJSONKeyvalue(carrierjsonObj, TOTAL_RGB));

					mData.setFromDate(fromdate);
					mData.setToDate(todate);
					mData.setIsOrigin(isOrigin);

					/** Database insertion **/
					mPieChartCarrierTableHelper.storeGraphOVData(mData);
					/***************************/

					mData.setILNs(null);
					carrierDatas.add(mData);
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carrierDatas;
	}

	/* For Total Shipment */

	/* For Delay Shipment */
	private ArrayList<CarrierData> ParseTheDataDelay(JSONObject jsObg) {
		ArrayList<CarrierData> carrierDatas = null;
		try {
			JSONObject noofShipmentObj = jsObg.getJSONObject(DELAY_DELAYSHIPMENT);
			JSONArray CarrierjsonArray = noofShipmentObj.getJSONArray(DELAY_CARRIER);
			carrierDatas = new ArrayList<CarrierData>();

			if (CarrierjsonArray != null && CarrierjsonArray.length() >= 1) {

				for (int i = 0; i < CarrierjsonArray.length(); i++) {
					CarrierData mData = new CarrierData();
					JSONObject carrierjsonObj = CarrierjsonArray.getJSONObject(i);

					mData.setCarrierID(Util.getJSONKeyvalue(carrierjsonObj, DELAY_CARRIERID));
					mData.setCarrier_Name(Util.getJSONKeyvalue(carrierjsonObj, DELAY_CARRIER_NAME));
					mData.setNo_Of_Shipment(Util.getJSONKeyvalue(carrierjsonObj, DELAY_NO_OF_SHIPMENT));
					mData.setNo_Of_delay_Shipment(Util.getJSONKeyvalue(carrierjsonObj, DELAY_NO_OF_DELAY_SHIPMENT));
					mData.setOverall_Percentage(Util.getJSONKeyvalue(carrierjsonObj, DELAY_OVERALL_PERCENTAGE));
					mData.setIndividual_Percentage(Util.getJSONKeyvalue(carrierjsonObj, DELAY_INDIVIDUAL_PERCENTAGE));
					mData.setRGB(Util.getJSONKeyvalue(carrierjsonObj, DELAY_RGB));
					mData.setILNs(null);

					mData.setFromDate(fromdate);
					mData.setToDate(todate);
					mData.setIsOrigin(isOrigin);

					/** Database insertion **/
					mPieChartCarrierTableHelper.storeGraphOVData(mData);
					/***************************/

					carrierDatas.add(mData);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carrierDatas;
	}

	/* For Delay Shipment */

	/* For InTransit Shipment */
	private ArrayList<CarrierData> ParseTheDataInTransit(JSONObject jsObg) {
		ArrayList<CarrierData> carrierDatas = null;
		try {
			JSONObject noofShipmentObj = jsObg.getJSONObject(INTRANSIT_INTRANSITSHIPMENT);
			JSONArray CarrierjsonArray = noofShipmentObj.getJSONArray(INTRANSIT_CARRIER);
			carrierDatas = new ArrayList<CarrierData>();

			if (CarrierjsonArray != null && CarrierjsonArray.length() >= 1) {

				for (int i = 0; i < CarrierjsonArray.length(); i++) {
					CarrierData mData = new CarrierData();
					JSONObject carrierjsonObj = CarrierjsonArray.getJSONObject(i);

					mData.setCarrierID(Util.getJSONKeyvalue(carrierjsonObj, INTRANSIT_CARRIERID));
					mData.setCarrier_Name(Util.getJSONKeyvalue(carrierjsonObj, INTRANSIT_CARRIER_NAME));
					mData.setNo_Of_Shipment(Util.getJSONKeyvalue(carrierjsonObj, INTRANSIT_NO_OF_SHIPMENT));
					mData.setNo_Of_delay_Shipment(Util.getJSONKeyvalue(carrierjsonObj, INTRANSIT_NO_OF_INTRANSIT_SHIPMENT));
					mData.setOverall_Percentage(Util.getJSONKeyvalue(carrierjsonObj, INTRANSIT_OVERALL_PERCENTAGE));
					mData.setIndividual_Percentage(Util.getJSONKeyvalue(carrierjsonObj, INTRANSIT_INDIVIDUAL_PERCENTAGE));
					mData.setRGB(Util.getJSONKeyvalue(carrierjsonObj, INTRANSIT_RGB));
					mData.setILNs(null);

					mData.setFromDate(fromdate);
					mData.setToDate(todate);
					mData.setIsOrigin(isOrigin);

					/** Database insertion **/
					mPieChartCarrierTableHelper.storeGraphOVData(mData);
					/***************************/

					carrierDatas.add(mData);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carrierDatas;
	}

	/* For InTransit Shipment */

	/* For Potential Shipment */
	private ArrayList<CarrierData> ParseTheDataPotential(JSONObject jsObg) {
		ArrayList<CarrierData> carrierDatas = null;
		try {
			JSONObject noofShipmentObj = jsObg.getJSONObject(POTENTIALDELAY_POTENTIALDELAYSHIPMENT);
			JSONArray CarrierjsonArray = noofShipmentObj.getJSONArray(POTENTIALDELAY_CARRIER);
			carrierDatas = new ArrayList<CarrierData>();

			if (CarrierjsonArray != null && CarrierjsonArray.length() >= 1) {

				for (int i = 0; i < CarrierjsonArray.length(); i++) {
					CarrierData mData = new CarrierData();
					JSONObject carrierjsonObj = CarrierjsonArray.getJSONObject(i);
					mData.setCarrierID(Util.getJSONKeyvalue(carrierjsonObj, POTENTIALDELAY_CARRIERID));
					mData.setCarrier_Name(Util.getJSONKeyvalue(carrierjsonObj, POTENTIALDELAY_CARRIER_NAME));
					mData.setNo_Of_Shipment(Util.getJSONKeyvalue(carrierjsonObj, POTENTIALDELAY_NO_OF_SHIPMENT));
					mData.setNo_Of_delay_Shipment(Util.getJSONKeyvalue(carrierjsonObj, POTENTIALDELAY_NO_OF_ON_TIME_SHIPMENT));
					mData.setOverall_Percentage(Util.getJSONKeyvalue(carrierjsonObj, POTENTIALDELAY_OVERALL_PERCENTAGE));
					mData.setIndividual_Percentage(Util.getJSONKeyvalue(carrierjsonObj, POTENTIALDELAY_INDIVIDUAL_PERCENTAGE));
					mData.setRGB(Util.getJSONKeyvalue(carrierjsonObj, POTENTIALDELAY_RGB));
					mData.setILNs(null);

					mData.setFromDate(fromdate);
					mData.setToDate(todate);
					mData.setIsOrigin(isOrigin);

					/** Database insertion **/
					mPieChartCarrierTableHelper.storeGraphOVData(mData);
					/***************************/

					carrierDatas.add(mData);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carrierDatas;
	}

	/* For Potential Shipment */

	public ShipmentParserInterface shipmentparserinterface;

	public ShipmentParserInterface getShipmentparserinterface() {
		return shipmentparserinterface;
	}

	public void setShipmentparserinterface(ShipmentParserInterface shipmentparserinterface) {
		this.shipmentparserinterface = shipmentparserinterface;
	}

	public interface ShipmentParserInterface {
		public void ONSuccess(ArrayList<CarrierData> carrierDatas);

		public void OnError();

		public void NoData();
	}
}
