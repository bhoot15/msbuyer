package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.SpinnerDataSet;

public class KPIReportSpinnerDataLoadParser {


	private Context mContext;

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";
	private String STATUS = "ResponseDetail";

	private String SHIPPINGLINEDETAILS = "ShippingLineDetails";
	
	private String ID = "ID";
	private String LINENAME = "LineName";
	
	private String POL = "POL";
	private String POL_ID = "ID";
	private String POL_NAME = "POLName"; 
	
	
	
	private String ILNPLANTFLOWDETAILS = "ILNPlantFlowDetails";
	
	private String ILN_ID = "ID";
	private String ILN_Name = "ILNName"; 
	
	private String PLANT = "Plant";
	private String PLANT_ID = "ID";
	private String PLANT_NAME = "PlantName"; 
	
	private String FLOW = "Flow";
	private String FLOW_ID = "ID";
	private String FLOW_NAME = "FlowName"; 
	
	
	private String ILNFLOWDETAILS = "ILNFlowDetails";
	
	private String COMP_ID = "ID";
	private String COMP_ILNNAME = "ILNName"; 
	
	private String COMP_Flow = "Flow";
	private String COMP_FlowID = "ID";
	private String COMP_FlowName = "FlowName";
	
	
	
	
	/* Response JSON key value */

	private String responsecode = "";
	private String responsedetails = "";
	
	
	public ArrayList<SpinnerDataSet> arrShipmentLineDetails=null; 
	public ArrayList<SpinnerDataSet> arraKPIReportMntSpnDetails=null;
	public ArrayList<SpinnerDataSet> arrComparisonDetails=null; 
	
	private String credentialService="";
	private String credentialForMonthly="";
	private String credentialForComparison="";
	
	




	public void parse(Context context) { 
		this.mContext = context;
		ServiceKPIReportAsync mServiceKPIReport = new ServiceKPIReportAsync(mContext);  
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mServiceKPIReport.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mServiceKPIReport.execute(); 
		}
	}
	
	private class ServiceKPIReportAsync extends AsyncTask<Void, Void, Void>{ 
		private Context context;
		private String authenticationJson;
		
		private boolean isTimeOut=false;
		
		public ServiceKPIReportAsync(Context mContext) {
			this.context = mContext;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata= Util.sendGet(credentialService);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205"))?true:false;
//				authenticationJson = Util.readXMLinString("KPIServiceDataJSON.json", context);
				if (authenticationJson!=null && !authenticationJson.equals("")) {
					ParseServiceData(authenticationJson); 
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				String[] responsedata= Util.sendGet(credentialForMonthly);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205"))?true:false;
//				authenticationJson = Util.readXMLinString("KPIMonthlyDataJSON.json", context);
				if (authenticationJson!=null && !authenticationJson.equals("")) {
					ParseMonthlyData(authenticationJson); 
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				String[] responsedata= Util.sendGet(credentialForComparison);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205"))?true:false;
				authenticationJson = Util.readXMLinString("KPIComparisonDataJSON.json", context);
				if (authenticationJson!=null && !authenticationJson.equals("")) {
					ParseComparisonData(authenticationJson); 
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);
//			if (isTimeOut) {
//				if (kpireportspinnerdataloadparserinterface != null) {
//					kpireportspinnerdataloadparserinterface.OnError(); 
//				}
//			}else {
				if (responsecode.equals("200")) {
					if (kpireportspinnerdataloadparserinterface != null) {
						kpireportspinnerdataloadparserinterface.OnSuccess();
					}
				}else if (responsecode.equals("1002")){
					if (kpireportspinnerdataloadparserinterface != null) {
						kpireportspinnerdataloadparserinterface.NoData();
					}
				}else {
					if (kpireportspinnerdataloadparserinterface != null) {
						kpireportspinnerdataloadparserinterface.OnError();  
					}
				}
//			}
			
		}
	}
	
	
	
	
	/**Shipment Line Parse Data**/
	private void ParseServiceData(String responseJSOn){
		try {
			JSONObject jsObg=new JSONObject(responseJSOn);
			responsecode = jsObg.getString(RESPONSECODE);
//			responsedetails = jsObg.getString(STATUS);

			if (responsecode.equals("200")) {
				JSONArray mJSONArray = jsObg.getJSONArray(SHIPPINGLINEDETAILS);
				
				if (mJSONArray!=null && mJSONArray.length()>=1) {
					arrShipmentLineDetails=new ArrayList<SpinnerDataSet>();
					
					for (int i = 0; i < mJSONArray.length(); i++) {
						
						JSONObject LineObj=mJSONArray.getJSONObject(i); 
						SpinnerDataSet mShipmentLineDetails=new SpinnerDataSet();
						
						mShipmentLineDetails.setId(Util.getJsonValue(LineObj, ID));
						mShipmentLineDetails.setName(Util.getJsonValue(LineObj, LINENAME)); 
						
						JSONArray polArray = LineObj.getJSONArray(POL);
						
						ArrayList<SpinnerDataSet> arraPol=null;
						if (polArray!=null && polArray.length()>=1) {
							arraPol=new ArrayList<SpinnerDataSet>();
							for (int j = 0; j < polArray.length(); j++) {
								JSONObject polObj=polArray.getJSONObject(j); 
								SpinnerDataSet mSpinnerDataSet=new SpinnerDataSet();
								mSpinnerDataSet.setId(Util.getJsonValue(polObj, POL_ID));
								mSpinnerDataSet.setName(Util.getJsonValue(polObj, POL_NAME));
								arraPol.add(mSpinnerDataSet);
							}
						}
						mShipmentLineDetails.setArra(arraPol);
						arrShipmentLineDetails.add(mShipmentLineDetails); 
					}
				}
				
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	/**Shipment Monthly Parse Data**/
	private void ParseMonthlyData(String responseJSOn){ 
		try {
			JSONObject jsObg=new JSONObject(responseJSOn);
			responsecode = jsObg.getString(RESPONSECODE);
//			responsedetails = jsObg.getString(STATUS);

			if (responsecode.equals("200")) {
				JSONArray ilnJSONArray = jsObg.getJSONArray(ILNPLANTFLOWDETAILS); 
				
				if (ilnJSONArray!=null && ilnJSONArray.length()>=1) {
					arraKPIReportMntSpnDetails=new ArrayList<SpinnerDataSet>();
					
					for (int i = 0; i < ilnJSONArray.length(); i++) {
						
						JSONObject inlObj=ilnJSONArray.getJSONObject(i); 
						SpinnerDataSet mKPIReportMonthlySpnDetails=new SpinnerDataSet();
						
						mKPIReportMonthlySpnDetails.setId(Util.getJsonValue(inlObj, ILN_ID));
						mKPIReportMonthlySpnDetails.setName(Util.getJsonValue(inlObj, ILN_Name));   
						
						JSONArray plantJSONArray = inlObj.getJSONArray(PLANT); 
						
						ArrayList<SpinnerDataSet> arraplant=null;
						
						if (plantJSONArray!=null && plantJSONArray.length()>=1) {
							arraplant=new ArrayList<SpinnerDataSet>();
							
							for (int j = 0; j < plantJSONArray.length(); j++) {
								
								JSONObject plantObj=plantJSONArray.getJSONObject(j); 
								SpinnerDataSet mSpinnerDataSet=new SpinnerDataSet();
								mSpinnerDataSet.setId(Util.getJsonValue(plantObj, PLANT_ID));
								mSpinnerDataSet.setName(Util.getJsonValue(plantObj, PLANT_NAME));
								
								
								JSONArray flowJSONArray = plantObj.getJSONArray(FLOW);
								ArrayList<SpinnerDataSet> flowArray=new ArrayList<SpinnerDataSet>();
								if (flowJSONArray!=null && flowJSONArray.length()>=1) {
									for (int k = 0; k < flowJSONArray.length(); k++) {
										JSONObject FLOWObj=flowJSONArray.getJSONObject(k); 
										SpinnerDataSet FLOWDetails=new SpinnerDataSet();
										FLOWDetails.setId(Util.getJsonValue(FLOWObj, FLOW_ID));
										FLOWDetails.setName(Util.getJsonValue(FLOWObj, FLOW_NAME));
										flowArray.add(FLOWDetails);
									}
									mSpinnerDataSet.setArra(flowArray); 
								}
								arraplant.add(mSpinnerDataSet);
							}
						}
						mKPIReportMonthlySpnDetails.setArra(arraplant);
						arraKPIReportMntSpnDetails.add(mKPIReportMonthlySpnDetails); 
					}
				}
				
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	/**Shipment Comparison Parse Data**/	
	private void ParseComparisonData(String responseJSOn){
		try {
			JSONObject jsObg=new JSONObject(responseJSOn);
			responsecode = jsObg.getString(RESPONSECODE);
//			responsedetails = jsObg.getString(STATUS);

			if (responsecode.equals("200")) {
				JSONArray mJSONArray = jsObg.getJSONArray(ILNFLOWDETAILS);
				
				if (mJSONArray!=null && mJSONArray.length()>=1) {
					arrComparisonDetails=new ArrayList<SpinnerDataSet>();
					
					for (int i = 0; i < mJSONArray.length(); i++) {
						
						JSONObject compilnflowObj=mJSONArray.getJSONObject(i);  
						SpinnerDataSet mShipmentLineDetails=new SpinnerDataSet();
						
						mShipmentLineDetails.setId(Util.getJsonValue(compilnflowObj, COMP_ID));
						mShipmentLineDetails.setName(Util.getJsonValue(compilnflowObj, COMP_ILNNAME)); 
						
						JSONArray compflowArray = compilnflowObj.getJSONArray(COMP_Flow); 
						
						ArrayList<SpinnerDataSet> arraPol=null;
						if (compflowArray!=null && compflowArray.length()>=1) {
							arraPol=new ArrayList<SpinnerDataSet>();
							for (int j = 0; j < compflowArray.length(); j++) {
								JSONObject compflowObj=compflowArray.getJSONObject(j);  
								SpinnerDataSet mSpinnerDataSet=new SpinnerDataSet();
								mSpinnerDataSet.setId(Util.getJsonValue(compflowObj, COMP_FlowID));
								mSpinnerDataSet.setName(Util.getJsonValue(compflowObj, COMP_FlowName));
								arraPol.add(mSpinnerDataSet);
							}
						}
						mShipmentLineDetails.setArra(arraPol);
						arrComparisonDetails.add(mShipmentLineDetails); 
					}
				}
				
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public String getCredentialService() {
		return credentialService;
	}
	public void setCredentialService(String credentialService) {
		this.credentialService = credentialService;
	}
	public String getCredentialForMonthly() {
		return credentialForMonthly;
	}
	public void setCredentialForMonthly(String credentialForMonthly) {
		this.credentialForMonthly = credentialForMonthly;
	}
	public String getCredentialForComparison() {
		return credentialForComparison;
	}
	public void setCredentialForComparison(String credentialForComparison) {
		this.credentialForComparison = credentialForComparison;
	}
	
	
	
	public KPIReportSpinnerDataLoadParserInterface kpireportspinnerdataloadparserinterface;

	public KPIReportSpinnerDataLoadParserInterface getKpireportspinnerdataloadparserinterface() {
		return kpireportspinnerdataloadparserinterface;
	}

	public void setKpireportspinnerdataloadparserinterface(
			KPIReportSpinnerDataLoadParserInterface kpireportspinnerdataloadparserinterface) {
		this.kpireportspinnerdataloadparserinterface = kpireportspinnerdataloadparserinterface;
	}

	public interface KPIReportSpinnerDataLoadParserInterface {
		public void OnSuccess();
		public void OnError();
		public void NoData();
	}

}
