//package com.myshipmentBuyer.parser;
//
//import java.util.ArrayList;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import android.content.Context;
//import android.os.AsyncTask;
//import android.os.Build;
//
//import com.myshipment.R;
//import com.myshipment.app.Util;
//import com.myshipment.app.model.KPIReportDetail;
//import com.myshipment.app.model.KPIReportObjReportDetail;
//import com.myshipment.app.model.SpinnerDataSet;
//
//public class KPIReportResultParser {
//
//	private Context mContext;
//
//	/* Response JSON key value */
//	private String RESPONSECODE = "ResponseCode";
//	private String STATUS = "ResponseDetails";
//
//	private String RESULTS = "Results";
//	
//	private String TOPHEADERNAME = "TopHeaderName";
//	private String HEADERNAME = "HeaderName";
//	private String DETAILS = "Details";
//	
//	private String HEADERVALUE = "HeaderValue";
//	private String TOTALMOVEMENT = "TotalMovement";
//	private String TOTALMOVEMENTBYFORECASTETD = "TotalMovementbyForecastETD"; 
//	private String TOTALMOVEMENTBYREALETD = "TotalMovementbyRealETD"; 
//	private String TRACKINGPERCENTAGEBYREALETD = "TrackingPercentagebyRealETD"; 
//	private String TRACKINGPERCENTAGEBYFORECASTETD = "TrackingPercentagebyForecastETD"; 
//	
//	
//	/* Response JSON key value */
//
//	private String responsecode = "";
//	private String responsedetails = "";
//	
//	
//	public ArrayList<SpinnerDataSet> arrShipmentLineDetails=null; 
//	public ArrayList<SpinnerDataSet> arraKPIReportMntSpnDetails=null;
//	public ArrayList<SpinnerDataSet> arrComparisonDetails=null; 
//	
//	
//	public void parse(Context context, String createUrl) { 
//		this.mContext = context;
//		KPIReportResultParserAsync mKPIReportResultParserAsync = new KPIReportResultParserAsync(mContext,createUrl);  
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) { 
//			mKPIReportResultParserAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR); 
//		} else {
//			mKPIReportResultParserAsync.execute(); 
//		}
//	}
//	
//	private class KPIReportResultParserAsync extends AsyncTask<Void, Void, Void>{ 
//		private Context context;
//		private String authenticationJson;
//		private ArrayList<KPIReportDetail> arrKpiReportDetails;
//		private String CREATEURL;
//		private boolean isTimeOut=false;
//		
//		public KPIReportResultParserAsync(Context mContext, String createUrl) {
//			this.context = mContext;
//			this.CREATEURL = createUrl;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			Util.showProgress(context, context.getResources().getString(R.string.loading));
//		}
//
//		@Override
//		protected Void doInBackground(Void... params) {
//			try {
//				String[] responsedata= Util.sendGet(CREATEURL);
//				authenticationJson = responsedata[1];
//				isTimeOut = (responsedata[0].equals("205"))?true:false;
//				
////				authenticationJson = Util.readXMLinString("KPIServiceResult.json", context);
//				if (authenticationJson!=null && !authenticationJson.equals("")) {
//					arrKpiReportDetails = ParseServiceData(authenticationJson); 
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			return null;
//		}
//
//		@Override
//		protected void onPostExecute(Void result) {
//			super.onPostExecute(result);
//			Util.hideProgress(context);
//			if (isTimeOut) {
//				if (kpireportresultparserinterface != null) {
//					kpireportresultparserinterface.OnError(); 
//				}
//			}else {
//				if (responsecode.equals("200")) {
//					if (kpireportresultparserinterface != null) {
//						kpireportresultparserinterface.OnSuccess(arrKpiReportDetails);
//					}
//				}else if (responsecode.equals("1002")){
//					if (kpireportresultparserinterface != null) {
//						kpireportresultparserinterface.NoData();
//					}
//				}else {
//					if (kpireportresultparserinterface != null) {
//						kpireportresultparserinterface.OnError();  
//					}
//				}
//			}
//			
//		}
//	}
//	
//	
//	
//	
//	/**Shipment Line Parse Data**/
//	private ArrayList<KPIReportDetail> ParseServiceData(String responseJSOn){
//		ArrayList<KPIReportDetail> arrayKPIReportDetail=null;
//		try {
//			JSONObject jsObg=new JSONObject(responseJSOn);
//			responsecode = jsObg.getString(RESPONSECODE);
//			responsedetails = jsObg.getString(STATUS);
//
//			if (responsecode.equals("200")) {
//				JSONArray mJSONArray = jsObg.getJSONArray(RESULTS);
//				
//				if (mJSONArray!=null && mJSONArray.length()>=1) {
//					arrayKPIReportDetail=new ArrayList<KPIReportDetail>();
//					
//					for (int i = 0; i < mJSONArray.length(); i++) {
//						JSONObject jsonObjKPIrpt = mJSONArray.getJSONObject(i);
//						KPIReportDetail mKpiReportDetail = new KPIReportDetail();
//						mKpiReportDetail.setTopheadername(Util.getJsonValue(jsonObjKPIrpt,TOPHEADERNAME));
//						mKpiReportDetail.setHeadername(Util.getJsonValue(jsonObjKPIrpt,HEADERNAME));
//						
//						JSONArray jsonArrayDetails = jsonObjKPIrpt.getJSONArray(DETAILS);
//						if (jsonArrayDetails!=null && jsonArrayDetails.length()>=1) {
//							ArrayList<KPIReportObjReportDetail> arrDetails=new ArrayList<KPIReportObjReportDetail>();
//							for (int k = 0; k < jsonArrayDetails.length(); k++) {
//								JSONObject jsonObjDetails = jsonArrayDetails.getJSONObject(k);
//								
//								KPIReportObjReportDetail mKPIReportObjReportDetail = new KPIReportObjReportDetail();
//								mKPIReportObjReportDetail.setHeadervalue(Util.getJsonValue(jsonObjDetails,HEADERVALUE));
//								mKPIReportObjReportDetail.setTotalmovement(Util.getJsonValue(jsonObjDetails,TOTALMOVEMENT));
//								mKPIReportObjReportDetail.setTotalmovementbyforecastetd(Util.getJsonValue(jsonObjDetails,TOTALMOVEMENTBYFORECASTETD));
//								mKPIReportObjReportDetail.setTotalmovementbyrealetd(Util.getJsonValue(jsonObjDetails,TOTALMOVEMENTBYREALETD));
//								mKPIReportObjReportDetail.setTrackingpercentagebyrealetd(Util.getJsonValue(jsonObjDetails,TRACKINGPERCENTAGEBYREALETD));
//								mKPIReportObjReportDetail.setTrackingpercentagebyforecastetd(Util.getJsonValue(jsonObjDetails,TRACKINGPERCENTAGEBYFORECASTETD));
//								arrDetails.add(mKPIReportObjReportDetail);
//							}
//							mKpiReportDetail.setDetails(arrDetails);
//						}
//						arrayKPIReportDetail.add(mKpiReportDetail);
//					}
//					
//				}
//				
//			}
//			
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		return arrayKPIReportDetail; 
//		
//	}
//	
//	public KPIReportResultParserInterface kpireportresultparserinterface;
//
//	public KPIReportResultParserInterface getKpireportresultparserinterface() {
//		return kpireportresultparserinterface;
//	}
//
//	public void setKpireportresultparserinterface(
//			KPIReportResultParserInterface kpireportresultparserinterface) {
//		this.kpireportresultparserinterface = kpireportresultparserinterface;
//	}
//
//	public interface KPIReportResultParserInterface {
//		public void OnSuccess(ArrayList<KPIReportDetail> arrKpiReportDetails); 
//		public void OnError();
//		public void NoData();
//	}
//
//
//}
