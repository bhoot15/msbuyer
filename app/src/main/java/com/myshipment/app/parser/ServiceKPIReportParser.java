package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.ServiceKPIReportDetails;
import com.myshipment.app.model.ServiceKPIReportVesselDetails;

public class ServiceKPIReportParser {

	private Context mContext;

	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";
	private String STATUS = "ResponseDetail";

	private String KPIREPORTSERVICERESULT = "KPIReportServiceResult";
	private String POL = "POL";
	private String NOOFONTIMESHIPMENT = "NoOfOntimeShipment";
	private String NOOFDELAYSHIPMENT_3TO7 = "NoOfDelayShipment_3to7"; 
	private String NOOFDELAYSHIPMENT_GREATER7 = "NoOfDelayShipment_greater7"; 
	private String RELIABILITY = "Reliability";
	private String RELIABILITYNO_OF_SHIPMENT = "Reliability/No_Of_Shipment";

	private String DETAILS = "Details";

	private String NOOFONTIMESHIPMENT_ = "NoOfOntimeShipment";
	private String VESSELNAME = "VesselName";
	private String RELIABILITY_ = "Reliability";
	private String NOOFDELAYSHIPMENT_3TO7_ = "NoOfDelayShipment_3to7";
	private String NOOFDELAYSHIPMENT_GREATER7_ = "NoOfDelayShipment_greater7";

	/* Response JSON key value */

	private String responsecode = "";
	private String responsedetails = "";

	public void parse(Context context, String createUrl) {
		this.mContext = context;
		ServiceKPIReportAsync mServiceKPIReport = new ServiceKPIReportAsync(mContext, createUrl);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mServiceKPIReport.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mServiceKPIReport.execute();
		}
	}

	private class ServiceKPIReportAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;
		private ArrayList<ServiceKPIReportDetails> ServiceKPIReportList_;
		private String CREATEURL;
		private boolean isTimeOut = false;

		public ServiceKPIReportAsync(Context mContext, String createUrl) {
			this.context = mContext;
			this.CREATEURL = createUrl;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(CREATEURL);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;

				if (authenticationJson != null && !authenticationJson.equals("")) {
					ServiceKPIReportList_ = ParseData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Util.hideProgress(context);
			if (isTimeOut) {
				if (servicekpireportparserinterface != null) {
					servicekpireportparserinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (servicekpireportparserinterface != null) {
						servicekpireportparserinterface.OnSuccess(ServiceKPIReportList_);
					}
				} else if (responsecode.equals("1002")) {
					if (servicekpireportparserinterface != null) {
						servicekpireportparserinterface.NoData();
					}
				} else {
					if (servicekpireportparserinterface != null) {
						servicekpireportparserinterface.OnError();
					}
				}
			}

		}
	}

	private ArrayList<ServiceKPIReportDetails> ParseData(String responseJSOn) {
		ArrayList<ServiceKPIReportDetails> ServiceKPIReportList = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.getString(RESPONSECODE);
			responsedetails = jsObg.getString(STATUS);

			if (responsecode.equals("200")) {
				JSONArray mJSONArray = jsObg.getJSONArray(KPIREPORTSERVICERESULT);

				if (mJSONArray != null && mJSONArray.length() >= 1) {
					ServiceKPIReportList = new ArrayList<ServiceKPIReportDetails>();

					for (int i = 0; i < mJSONArray.length(); i++) {

						JSONObject serviceObj = mJSONArray.getJSONObject(i);
						ServiceKPIReportDetails mDetails = new ServiceKPIReportDetails();

						mDetails.setPol(Util.getJsonValue(serviceObj, POL));
						mDetails.setReliability(Util.getJsonValue(serviceObj, RELIABILITY));
						mDetails.setNo_of_delayshipment_greater_7(Util.getJsonValue(serviceObj, NOOFDELAYSHIPMENT_GREATER7));
						mDetails.setNo_of_ontimeshipment(Util.getJsonValue(serviceObj, NOOFONTIMESHIPMENT));
						mDetails.setReliabilityno_of_shipment(Util.getJsonValue(serviceObj, RELIABILITYNO_OF_SHIPMENT));
						mDetails.setNo_of_delayshipment_3_to_7(Util.getJsonValue(serviceObj, NOOFDELAYSHIPMENT_3TO7));

						JSONArray vesselJSONArray = serviceObj.getJSONArray(DETAILS);
						if (vesselJSONArray != null && vesselJSONArray.length() >= 1) {
							ArrayList<ServiceKPIReportVesselDetails> vesselListArray = new ArrayList<ServiceKPIReportVesselDetails>();
							for (int j = 0; j < vesselJSONArray.length(); j++) {
								JSONObject vesselObj = vesselJSONArray.getJSONObject(j);
								ServiceKPIReportVesselDetails mVesselDetails = new ServiceKPIReportVesselDetails();
								mVesselDetails.setNoOfDelayShipment_3to7(Util.getJsonValue(vesselObj, NOOFDELAYSHIPMENT_3TO7_));
								mVesselDetails.setNoOfDelayShipment_greater7(Util.getJsonValue(vesselObj, NOOFDELAYSHIPMENT_GREATER7_));
								mVesselDetails.setNoOfOntimeShipment(Util.getJsonValue(vesselObj, NOOFONTIMESHIPMENT_));
								mVesselDetails.setReliability(Util.getJsonValue(vesselObj, RELIABILITY_));
								mVesselDetails.setVesselName(Util.getJsonValue(vesselObj, VESSELNAME));
								vesselListArray.add(mVesselDetails);
							}

							mDetails.setVesselList(vesselListArray);
						}

						ServiceKPIReportList.add(mDetails);
					}
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return ServiceKPIReportList;
	}

	public ServiceKPIReportParserInterface servicekpireportparserinterface;

	public ServiceKPIReportParserInterface getServicekpireportparserinterface() {
		return servicekpireportparserinterface;
	}

	public void setServicekpireportparserinterface(ServiceKPIReportParserInterface servicekpireportparserinterface) {
		this.servicekpireportparserinterface = servicekpireportparserinterface;
	}

	public interface ServiceKPIReportParserInterface {
		public void OnSuccess(ArrayList<ServiceKPIReportDetails> ServiceKPIReportList);

		public void OnError();

		public void NoData();
	}

}
