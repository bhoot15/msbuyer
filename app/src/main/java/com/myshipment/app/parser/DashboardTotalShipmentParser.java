package com.myshipment.app.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.myshipment.R;
import com.myshipment.app.Util;
import com.myshipment.app.model.CarrierData;
import com.myshipment.app.model.CarrierDetail;
import com.myshipment.app.model.Flow;
import com.myshipment.app.model.ILN;

public class DashboardTotalShipmentParser {

	private Context mContext;
	private String URL = "";
	/* Response JSON key value */
	private String RESPONSECODE = "ResponseCode";
	// private String STATUS = "Status";

	private String NOOFSHIPMENT = "No Of Shipment";

	private String CARRIER = "Carrier";

	private String CARRIERID = "CarrierID";
	private String CARRIER_NAME = "Carrier Name";
	private String NO_OF_SHIPMENT = "No Of Shipment";
	// private String NO_OF_DELAY_SHIPMENT = "No Of delay Shipment";
	private String OVERALL_PERCENTAGE = "Overall Percentage";
	// private String INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	private String RGB = "RGB";

	private String ILN = "ILN";

	private String ILNID = "ILNID";
	private String ILN_NAME = "ILN Name";
	// private String NO_OF_SHIPMENT = "No Of Shipment";
	// private String NO_OF_DELAY_SHIPMENT = "No Of delay Shipment";
	// private String OVERALL_PERCENTAGE = "Overall Percentage";
	// private String INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	// private String RGB = "RGB";

	private String FLOW = "Flow";

	private String FLOWID = "FlowID";
	private String FLOW_NAME = "Flow Name";
	// private String NO_OF_SHIPMENT = "No Of Shipment";
	// private String NO_OF_DELAY_SHIPMENT = "No Of delay Shipment";
	// private String OVERALL_PERCENTAGE = "Overall Percentage";
	// private String INDIVIDUAL_PERCENTAGE = "Individual Percentage";
	// private String RGB = "RGB";

	private String CARRIERDETAILS = "CarrierDetails";

	private String CARRIER_NAME_FORCARRIERDETAILS = "Carrier Name";
	private String CONTAINER_NUMBER = "Container Number";
	private String POL_POD = "POL-POD";
	private String INITIAL = "Initial";
	private String REAL = "Real";
	private String DELAY_DAYS = "Delay Days";

	/* Response JSON key value */
	private String responsecode = "";

	public void parse(Context context, String url) {
		this.mContext = context;
		this.URL = url;

		ILNPlantTotalShipmentAsync mILNPlantTotalShipmentAsync = new ILNPlantTotalShipmentAsync(mContext);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mILNPlantTotalShipmentAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			mILNPlantTotalShipmentAsync.execute();
		}
	}

	public String[] getListViewFieldName() {
		String[] fields = new String[3];
		fields[0] = CARRIER_NAME;
		fields[1] = NO_OF_SHIPMENT;
		fields[2] = OVERALL_PERCENTAGE;

		return fields;
	}

	private class ILNPlantTotalShipmentAsync extends AsyncTask<Void, Void, Void> {
		private Context context;
		private String authenticationJson;
		private ArrayList<CarrierData> CarrierDatas;
		private boolean isTimeOut = false;

		public ILNPlantTotalShipmentAsync(Context mContext) {
			this.context = mContext;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Util.showProgress(context, context.getResources().getString(R.string.loading));
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String[] responsedata = Util.sendGet(URL);
				authenticationJson = responsedata[1];
				isTimeOut = (responsedata[0].equals("205")) ? true : false;

//				isTimeOut=true;
//				authenticationJson = Util.readXMLinString("TotalShipment.json", context);

				if (authenticationJson != null && !authenticationJson.equals("")) {
					CarrierDatas = ParseTheData(authenticationJson);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			Util.hideProgress(context);

			if (isTimeOut) {
				if (ilnplanttotalshipmentparserinterface != null) {
					ilnplanttotalshipmentparserinterface.OnError();
				}
			} else {
				if (responsecode.equals("200")) {
					if (ilnplanttotalshipmentparserinterface != null) {
						ilnplanttotalshipmentparserinterface.ONSuccess(CarrierDatas);
					}
				} else if (responsecode.equals("1002")) {
					if (ilnplanttotalshipmentparserinterface != null) {
						ilnplanttotalshipmentparserinterface.NoData();
					}
				} else {
					if (ilnplanttotalshipmentparserinterface != null) {
						ilnplanttotalshipmentparserinterface.OnError();
					}
				}
			}

		}
	}

	private ArrayList<CarrierData> ParseTheData(String responseJSOn) {

		ArrayList<CarrierData> carrierDatas = null;
		try {
			JSONObject jsObg = new JSONObject(responseJSOn);
			responsecode = jsObg.getString(RESPONSECODE);

			if (responsecode.equals("200")) {
				JSONObject noofShipmentObj = jsObg.getJSONObject(NOOFSHIPMENT);
				JSONArray CarrierjsonArray = noofShipmentObj.getJSONArray(CARRIER);

				carrierDatas = new ArrayList<CarrierData>();
				for (int i = 0; i < CarrierjsonArray.length(); i++) {
					CarrierData mData = new CarrierData();
					JSONObject carrierjsonObj = CarrierjsonArray.getJSONObject(i);

					mData.setCarrierID(Util.getJSONKeyvalue(carrierjsonObj, CARRIERID));
					mData.setCarrier_Name(Util.getJSONKeyvalue(carrierjsonObj, CARRIER_NAME));
					mData.setNo_Of_Shipment(Util.getJSONKeyvalue(carrierjsonObj, NO_OF_SHIPMENT));
					mData.setOverall_Percentage(Util.getJSONKeyvalue(carrierjsonObj, OVERALL_PERCENTAGE));
					mData.setRGB(Util.getJSONKeyvalue(carrierjsonObj, RGB));
					mData.setILNs(getILNData(carrierjsonObj));
					carrierDatas.add(mData);

				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carrierDatas;
	}

	private ArrayList<ILN> getILNData(JSONObject carrierjsonObj) {
		ArrayList<ILN> ILNs = null;
		try {
			JSONArray ILNjsonArray = carrierjsonObj.getJSONArray(ILN);
			if (ILNjsonArray != null && ILNjsonArray.length() >= 1) {
				ILNs = new ArrayList<ILN>();
				for (int i = 0; i < ILNjsonArray.length(); i++) {
					JSONObject ILNjsonObj = ILNjsonArray.getJSONObject(i);
					ILN mIln = new ILN();
					mIln.setILNID(Util.getJSONKeyvalue(ILNjsonObj, ILNID));
					mIln.setILN_Name(Util.getJSONKeyvalue(ILNjsonObj, ILN_NAME));
					mIln.setNo_Of_Shipment(Util.getJSONKeyvalue(ILNjsonObj, NO_OF_SHIPMENT));
					mIln.setOverall_Percentage(Util.getJSONKeyvalue(ILNjsonObj, OVERALL_PERCENTAGE));
					mIln.setRGB(Util.getJSONKeyvalue(ILNjsonObj, RGB));
					mIln.setFlows(getFlowData(ILNjsonObj));
					ILNs.add(mIln);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ILNs;
	}

	private ArrayList<Flow> getFlowData(JSONObject ILNjsonObj) {
		ArrayList<Flow> Flows = null;
		try {
			JSONArray FlowsjsonArray = ILNjsonObj.getJSONArray(FLOW);
			if (FlowsjsonArray != null && FlowsjsonArray.length() >= 1) {
				Flows = new ArrayList<Flow>();
				for (int i = 0; i < FlowsjsonArray.length(); i++) {
					JSONObject flowjsonObg = FlowsjsonArray.getJSONObject(i);
					Flow mFlow = new Flow();
					mFlow.setFlowID(Util.getJSONKeyvalue(flowjsonObg, FLOWID));
					mFlow.setFlow_Name(Util.getJSONKeyvalue(flowjsonObg, FLOW_NAME));
					mFlow.setNo_Of_Shipment(Util.getJSONKeyvalue(flowjsonObg, NO_OF_SHIPMENT));
					mFlow.setOverall_Percentage(Util.getJSONKeyvalue(flowjsonObg, OVERALL_PERCENTAGE));
					mFlow.setRGB(Util.getJSONKeyvalue(flowjsonObg, RGB));
					mFlow.setCarrierDetails(getCarrierDetailsData(flowjsonObg));
					Flows.add(mFlow);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return Flows;
	}

	private ArrayList<CarrierDetail> getCarrierDetailsData(JSONObject flowjsonObg) {
		ArrayList<CarrierDetail> carDetailsList = null;
		try {
			JSONArray carrierJodnarray = flowjsonObg.getJSONArray(CARRIERDETAILS);
			if (carrierJodnarray != null && carrierJodnarray.length() >= 1) {
				carDetailsList = new ArrayList<CarrierDetail>();
				for (int i = 0; i < carrierJodnarray.length(); i++) {

					JSONObject carrierDetailjsonObj = carrierJodnarray.getJSONObject(i);
					CarrierDetail mCarrierDetail = new CarrierDetail();

					mCarrierDetail.setCarrier_Name(Util.getJSONKeyvalue(carrierDetailjsonObj, CARRIER_NAME_FORCARRIERDETAILS));
					mCarrierDetail.setContainer_Number(Util.getJSONKeyvalue(carrierDetailjsonObj, CONTAINER_NUMBER));
					mCarrierDetail.setPOL_POD(Util.getJSONKeyvalue(carrierDetailjsonObj, POL_POD));
					mCarrierDetail.setInitial(Util.getJSONKeyvalue(carrierDetailjsonObj, INITIAL));
					mCarrierDetail.setReal(Util.getJSONKeyvalue(carrierDetailjsonObj, REAL));
					mCarrierDetail.setDelay_Days(Util.getJSONKeyvalue(carrierDetailjsonObj, DELAY_DAYS));
					carDetailsList.add(mCarrierDetail);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return carDetailsList;
	}

	public ILNPlantTotalShipmentParserInterface ilnplanttotalshipmentparserinterface;

	public ILNPlantTotalShipmentParserInterface getIlnplanttotalshipmentparserinterface() {
		return ilnplanttotalshipmentparserinterface;
	}

	public void setIlnplanttotalshipmentparserinterface(ILNPlantTotalShipmentParserInterface ilnplanttotalshipmentparserinterface) {
		this.ilnplanttotalshipmentparserinterface = ilnplanttotalshipmentparserinterface;
	}

	public interface ILNPlantTotalShipmentParserInterface {
		public void ONSuccess(ArrayList<CarrierData> carrierDatas);

		public void NoData();

		public void OnError();
	}

}
