package com.myshipment.app;

import java.util.ArrayList;

import android.app.Application;
import android.content.Context;

import com.crittercism.app.Crittercism;
import com.myshipment.app.databasehelper.MyShipmentDBHelper;
import com.myshipment.app.model.INLData;
import com.myshipment.app.model.INLOverViewData;
import com.myshipment.app.model.IncidentRepor;
import com.myshipment.app.model.RegularSearchResult;
import com.myshipment.app.model.TrackResult;

public class MyShipmentApplication extends Application {

	public INLOverViewData mViewData;
	public ArrayList<INLData> ILNAdatas;
	public ArrayList<INLData> FilterDataSet;
	public ArrayList<TrackResult> trackresults = null;
	public ArrayList<IncidentRepor> incidentReports = null;
	public ArrayList<RegularSearchResult> searchresults = null;

	public String[] dashboardSearchData;

	public MyShipmentDBHelper DB_HELPER;
	public static volatile Context applicationContext = null;

	@Override
	public void onCreate() {
		super.onCreate();

		applicationContext = getApplicationContext();
		initilizeDB();
		Crittercism.initialize(getApplicationContext(), "54787d1d1787847fd5000007");
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	/**
	 * Related to Data Base.
	 */
	private void initilizeDB() {

		try {
			if (DB_HELPER == null) {
				DB_HELPER = new MyShipmentDBHelper(MyShipmentApplication.this);
			}
			DB_HELPER.getWritableDatabase();
			DB_HELPER.openDataBase();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void closeDB() {
		try {
			DB_HELPER.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
