package com.myshipment.app.model;

public class DateMaxMin {
	private int MinDay;
	private int MinMonth;
	private int MinYear;

	private int CurrentDay;
	private int CurrentMonth;
	private int CurrentYear;

	private int MaxDay;
	private int MaxMonth;
	private int MaxYear;
	
	public int getMinDay() {
		return MinDay;
	}
	public void setMinDay(int minDay) {
		MinDay = minDay;
	}
	public int getMinMonth() {
		return MinMonth;
	}
	public void setMinMonth(int minMonth) {
		MinMonth = minMonth;
	}
	public int getMinYear() {
		return MinYear;
	}
	public void setMinYear(int minYear) {
		MinYear = minYear;
	}
	public int getCurrentDay() {
		return CurrentDay;
	}
	public void setCurrentDay(int currentDay) {
		CurrentDay = currentDay;
	}
	public int getCurrentMonth() {
		return CurrentMonth;
	}
	public void setCurrentMonth(int currentMonth) {
		CurrentMonth = currentMonth;
	}
	public int getCurrentYear() {
		return CurrentYear;
	}
	public void setCurrentYear(int currentYear) {
		CurrentYear = currentYear;
	}
	public int getMaxDay() {
		return MaxDay;
	}
	public void setMaxDay(int maxDay) {
		MaxDay = maxDay;
	}
	public int getMaxMonth() {
		return MaxMonth;
	}
	public void setMaxMonth(int maxMonth) {
		MaxMonth = maxMonth;
	}
	public int getMaxYear() {
		return MaxYear;
	}
	public void setMaxYear(int maxYear) {
		MaxYear = maxYear;
	}
	
	
	

}
