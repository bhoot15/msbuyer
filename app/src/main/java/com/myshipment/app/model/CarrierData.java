package com.myshipment.app.model;

import java.util.ArrayList;

public class CarrierData {

	private String CarrierID = "";
	private String Carrier_Name = "";
	private String No_Of_Shipment = "";
	private String No_Of_delay_Shipment = "";
	private String Overall_Percentage = "";
	private String Individual_Percentage = "";
	private String RGB = "";

	private String fromDate = "";
	private String toDate = "";
	private String isOrigin = "";
	private String type = "";

	private ArrayList<ILN> ILNs = new ArrayList<ILN>();

	public String getCarrierID() {
		return CarrierID;
	}

	public void setCarrierID(String carrierID) {
		CarrierID = carrierID;
	}

	public String getCarrier_Name() {
		return Carrier_Name;
	}

	public void setCarrier_Name(String carrier_Name) {
		Carrier_Name = carrier_Name;
	}

	public String getNo_Of_Shipment() {
		return No_Of_Shipment;
	}

	public void setNo_Of_Shipment(String no_Of_Shipment) {
		No_Of_Shipment = no_Of_Shipment;
	}

	public String getNo_Of_delay_Shipment() {
		return No_Of_delay_Shipment;
	}

	public void setNo_Of_delay_Shipment(String no_Of_delay_Shipment) {
		No_Of_delay_Shipment = no_Of_delay_Shipment;
	}

	public String getOverall_Percentage() {
		return Overall_Percentage;
	}

	public void setOverall_Percentage(String overall_Percentage) {
		Overall_Percentage = overall_Percentage;
	}

	public String getIndividual_Percentage() {
		return Individual_Percentage;
	}

	public void setIndividual_Percentage(String individual_Percentage) {
		Individual_Percentage = individual_Percentage;
	}

	public String getRGB() {
		return RGB;
	}

	public void setRGB(String rGB) {
		RGB = rGB;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getIsOrigin() {
		return isOrigin;
	}

	public void setIsOrigin(String isOrigin) {
		this.isOrigin = isOrigin;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<ILN> getILNs() {
		return ILNs;
	}

	public void setILNs(ArrayList<ILN> iLNs) {
		ILNs = iLNs;
	}

}
