package com.myshipment.app.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.myshipment.app.Util;

public class UserDetails {

	
	private String username="";
	private String role="";
	private String firstname="";
	private String userid="";
	private String lastname="";
	private String vcusertype="";
	private String vcmasterusername="";
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getVcusertype() {
		return vcusertype;
	}
	public void setVcusertype(String vcusertype) {
		this.vcusertype = vcusertype;
	}
	public String getVcmasterusername() {
		return vcmasterusername;
	}
	public void setVcmasterusername(String vcmasterusername) {
		this.vcmasterusername = vcmasterusername;
	}
	
	
	public void saveInPreferense(Context context) {
		SharedPreferences.Editor prefsEditor = Util.getPrefs(context).edit();
		Gson gson = new Gson();
		String json = gson.toJson(this);
		prefsEditor.putString("LoggedInUser", json);
		prefsEditor.commit();
	}

	public static UserDetails getLoggedInUser(Context context) {
		SharedPreferences mPrefs = Util.getPrefs(context);
		Gson gson = new Gson();
		String json = mPrefs.getString("LoggedInUser", "");
		UserDetails obj = gson.fromJson(json, UserDetails.class);
		return obj;
	}
	
	public static void logoutUser(Context context) {
		SharedPreferences.Editor prefsEditor = Util.getPrefs(context).edit();
		Gson gson = new Gson();
		String json = gson.toJson(null);
		prefsEditor.putString("LoggedInUser", json);
		prefsEditor.commit();
	}
	
}
