package com.myshipment.app.model;

public class IncidentRepor {

	private String BookingNumber = "";
	private String IncidentNumber = "";
	private String DIFIPsupport = "";
	private String ILNname = "";
	private String InitialVesselweek = "";
	private String IncidentClosuredate = "";

	public String getBookingNumber() {
		return BookingNumber;
	}

	public void setBookingNumber(String bookingNumber) {
		BookingNumber = bookingNumber;
	}

	public String getIncidentNumber() {
		return IncidentNumber;
	}

	public void setIncidentNumber(String incidentNumber) {
		IncidentNumber = incidentNumber;
	}

	public String getDIFIPsupport() {
		return DIFIPsupport;
	}

	public void setDIFIPsupport(String dIFIPsupport) {
		DIFIPsupport = dIFIPsupport;
	}

	public String getILNname() {
		return ILNname;
	}

	public void setILNname(String iLNname) {
		ILNname = iLNname;
	}

	public String getInitialVesselweek() {
		return InitialVesselweek;
	}

	public void setInitialVesselweek(String initialVesselweek) {
		InitialVesselweek = initialVesselweek;
	}

	public String getIncidentClosuredate() {
		return IncidentClosuredate;
	}

	public void setIncidentClosuredate(String incidentClosuredate) {
		IncidentClosuredate = incidentClosuredate;
	}

}
