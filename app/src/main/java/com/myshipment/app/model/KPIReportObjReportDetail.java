package com.myshipment.app.model;

public class KPIReportObjReportDetail {
	public String headervalue = "";
	private String totalmovement = "";
	private String totalmovementbyforecastetd = "";
	private String totalmovementbyrealetd = "";
	private String trackingpercentagebyrealetd = "";
	private String trackingpercentagebyforecastetd = "";

	public String getHeadervalue() {
		return headervalue;
	}

	public void setHeadervalue(String headervalue) {
		this.headervalue = headervalue;
	}

	public String getTotalmovement() {
		return totalmovement;
	}

	public void setTotalmovement(String totalmovement) {
		this.totalmovement = totalmovement;
	}

	public String getTotalmovementbyforecastetd() {
		return totalmovementbyforecastetd;
	}

	public void setTotalmovementbyforecastetd(String totalmovementbyforecastetd) {
		this.totalmovementbyforecastetd = totalmovementbyforecastetd;
	}

	public String getTotalmovementbyrealetd() {
		return totalmovementbyrealetd;
	}

	public void setTotalmovementbyrealetd(String totalmovementbyrealetd) {
		this.totalmovementbyrealetd = totalmovementbyrealetd;
	}

	public String getTrackingpercentagebyrealetd() {
		return trackingpercentagebyrealetd;
	}

	public void setTrackingpercentagebyrealetd(
			String trackingpercentagebyrealetd) {
		this.trackingpercentagebyrealetd = trackingpercentagebyrealetd;
	}

	public String getTrackingpercentagebyforecastetd() {
		return trackingpercentagebyforecastetd;
	}

	public void setTrackingpercentagebyforecastetd(
			String trackingpercentagebyforecastetd) {
		this.trackingpercentagebyforecastetd = trackingpercentagebyforecastetd;
	}
}
