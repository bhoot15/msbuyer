package com.myshipment.app.model;

import java.util.ArrayList;

public class ILN {

	private String ILNID = "";
	private String ILN_Name = "";
	private String No_Of_Shipment = "";
	private String No_Of__Shipment = "";
	private String Overall_Percentage = "";
	private String Individual_Percentage = "";
	private String RGB = "";

	private String fromDate = "";
	private String toDate = "";
	private String isOrigin = "";
	private String type = "";
	private String CarrierName = "";

	private ArrayList<Flow> Flows = new ArrayList<Flow>();

	public String getILNID() {
		return ILNID;
	}

	public void setILNID(String iLNID) {
		ILNID = iLNID;
	}

	public String getILN_Name() {
		return ILN_Name;
	}

	public void setILN_Name(String iLN_Name) {
		ILN_Name = iLN_Name;
	}

	public String getNo_Of_Shipment() {
		return No_Of_Shipment;
	}

	public void setNo_Of_Shipment(String no_Of_Shipment) {
		No_Of_Shipment = no_Of_Shipment;
	}

	public String getNo_Of__Shipment() {
		return No_Of__Shipment;
	}

	public void setNo_Of__Shipment(String no_Of__Shipment) {
		No_Of__Shipment = no_Of__Shipment;
	}

	public String getOverall_Percentage() {
		return Overall_Percentage;
	}

	public void setOverall_Percentage(String overall_Percentage) {
		Overall_Percentage = overall_Percentage;
	}

	public String getIndividual_Percentage() {
		return Individual_Percentage;
	}

	public void setIndividual_Percentage(String individual_Percentage) {
		Individual_Percentage = individual_Percentage;
	}

	public String getRGB() {
		return RGB;
	}

	public void setRGB(String rGB) {
		RGB = rGB;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getIsOrigin() {
		return isOrigin;
	}

	public void setIsOrigin(String isOrigin) {
		this.isOrigin = isOrigin;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCarrierName() {
		return CarrierName;
	}

	public void setCarrierName(String carrierName) {
		CarrierName = carrierName;
	}

	public ArrayList<Flow> getFlows() {
		return Flows;
	}

	public void setFlows(ArrayList<Flow> flows) {
		Flows = flows;
	}

}
