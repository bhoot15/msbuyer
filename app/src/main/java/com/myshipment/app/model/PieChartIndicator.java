package com.myshipment.app.model;

public class PieChartIndicator {

	
	private String title;
	private String RGB;
	private String Value;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRGB() {
		return RGB;
	}
	public void setRGB(String rGB) {
		RGB = rGB;
	}
	public String getValue() {
		return Value;
	}
	public void setValue(String value) {
		Value = value;
	}
}
