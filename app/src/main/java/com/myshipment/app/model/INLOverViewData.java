package com.myshipment.app.model;

import java.util.ArrayList;

public class INLOverViewData {

	private String responsecode = "";
	private String status = "";

	private String maxcontainercount = "";
	private String maxdelaypercentagerate = "";
	private String totalshipment = "";
	private String totaldelayed = "";
	private String totalintransit = "";
	private String totalpotentialdelay = "";

	private String fromDate = "";
	private String toDate = "";
	private String isOrigin = "";

	private ArrayList<INLData> arrayINLData = new ArrayList<INLData>();

	public String getResponsecode() {
		return responsecode;
	}

	public void setResponsecode(String responsecode) {
		this.responsecode = responsecode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMaxcontainercount() {
		return maxcontainercount;
	}

	public void setMaxcontainercount(String maxcontainercount) {
		this.maxcontainercount = maxcontainercount;
	}

	public String getMaxdelaypercentagerate() {
		return maxdelaypercentagerate;
	}

	public void setMaxdelaypercentagerate(String maxdelaypercentagerate) {
		this.maxdelaypercentagerate = maxdelaypercentagerate;
	}

	public String getTotalshipment() {
		return totalshipment;
	}

	public void setTotalshipment(String totalshipment) {
		this.totalshipment = totalshipment;
	}

	public String getTotaldelayed() {
		return totaldelayed;
	}

	public void setTotaldelayed(String totaldelayed) {
		this.totaldelayed = totaldelayed;
	}

	public String getTotalintransit() {
		return totalintransit;
	}

	public void setTotalintransit(String totalintransit) {
		this.totalintransit = totalintransit;
	}

	public String getTotalpotentialdelay() {
		return totalpotentialdelay;
	}

	public void setTotalpotentialdelay(String totalpotentialdelay) {
		this.totalpotentialdelay = totalpotentialdelay;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getIsOrigin() {
		return isOrigin;
	}

	public void setIsOrigin(String isOrigin) {
		this.isOrigin = isOrigin;
	}

	public ArrayList<INLData> getArrayINLData() {
		return arrayINLData;
	}

	public void setArrayINLData(ArrayList<INLData> arrayINLData) {
		this.arrayINLData = arrayINLData;
	}

}
