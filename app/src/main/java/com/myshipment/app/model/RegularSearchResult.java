package com.myshipment.app.model;

public class RegularSearchResult {

	private String pol = "";
	private String iln = "";
	private String atd = "";
	private String etd = "";
	private String pod = "";
	private String initialvesselweek = "";
	private String ata = "ATA";
	private String booking = "";
	private String eta = "";
	private String container = "";

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getIln() {
		return iln;
	}

	public void setIln(String iln) {
		this.iln = iln;
	}

	public String getAtd() {
		return atd;
	}

	public void setAtd(String atd) {
		this.atd = atd;
	}

	public String getEtd() {
		return etd;
	}

	public void setEtd(String etd) {
		this.etd = etd;
	}

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public String getInitialvesselweek() {
		return initialvesselweek;
	}

	public void setInitialvesselweek(String initialvesselweek) {
		this.initialvesselweek = initialvesselweek;
	}

	public String getAta() {
		return ata;
	}

	public void setAta(String ata) {
		this.ata = ata;
	}

	public String getBooking() {
		return booking;
	}

	public void setBooking(String booking) {
		this.booking = booking;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}

}
