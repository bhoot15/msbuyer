package com.myshipment.app.model;

import java.util.ArrayList;

public class TrackResult {

	private String ActualETD = "";
	private String POL = "";
	private String BookingNo = "";
	private String ActualETA = "";
	private String ContainerNo = "";

	private String EstimatedETD = "";
	private String CarrierName = "";
	private String EstimatedETA = "";
	private String POD = "";

	private ArrayList<TrackResultDetail> Details = new ArrayList<TrackResultDetail>();

	public String getActualETD() {
		return ActualETD;
	}

	public void setActualETD(String actualETD) {
		ActualETD = actualETD;
	}

	public String getPOL() {
		return POL;
	}

	public void setPOL(String pOL) {
		POL = pOL;
	}

	public String getBookingNo() {
		return BookingNo;
	}

	public void setBookingNo(String bookingNo) {
		BookingNo = bookingNo;
	}

	public String getActualETA() {
		return ActualETA;
	}

	public void setActualETA(String actualETA) {
		ActualETA = actualETA;
	}

	public String getContainerNo() {
		return ContainerNo;
	}

	public void setContainerNo(String containerNo) {
		ContainerNo = containerNo;
	}

	public String getEstimatedETD() {
		return EstimatedETD;
	}

	public void setEstimatedETD(String estimatedETD) {
		EstimatedETD = estimatedETD;
	}

	public String getCarrierName() {
		return CarrierName;
	}

	public void setCarrierName(String carrierName) {
		CarrierName = carrierName;
	}

	public String getEstimatedETA() {
		return EstimatedETA;
	}

	public void setEstimatedETA(String estimatedETA) {
		EstimatedETA = estimatedETA;
	}

	public String getPOD() {
		return POD;
	}

	public void setPOD(String pOD) {
		POD = pOD;
	}

	public ArrayList<TrackResultDetail> getDetails() {
		return Details;
	}

	public void setDetails(ArrayList<TrackResultDetail> details) {
		Details = details;
	}

}
