package com.myshipment.app.model;

import java.util.ArrayList;

public class OceanScheduleSearchDetails {

	private String name = "";
	private String image_link = "";
	private String vesselcode = "";
	private String voyagecode = "";
	private String portcall = "";
	private String days = "";
	private String address = "";
	private String arrivedate = "";
	private String departdate = "";
	private String rootorigin = "";
	private String rootdestination = "";

	/**
	 * Child Section
	 */
	private String child_departdate = "";
	private String child_arrivedate = "";
	private String child_departing = "";
	private String child_arriving = "";

	private ArrayList<OceanScheduleSearchDetails> transhipmentResult = new ArrayList<OceanScheduleSearchDetails>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage_link() {
		return image_link;
	}

	public void setImage_link(String image_link) {
		this.image_link = image_link;
	}

	public String getVesselcode() {
		return vesselcode;
	}

	public void setVesselcode(String vesselcode) {
		this.vesselcode = vesselcode;
	}

	public String getVoyagecode() {
		return voyagecode;
	}

	public void setVoyagecode(String voyagecode) {
		this.voyagecode = voyagecode;
	}

	public String getPortcall() {
		return portcall;
	}

	public void setPortcall(String portcall) {
		this.portcall = portcall;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getArrivedate() {
		return arrivedate;
	}

	public void setArrivedate(String arrivedate) {
		this.arrivedate = arrivedate;
	}

	public String getDepartdate() {
		return departdate;
	}

	public void setDepartdate(String departdate) {
		this.departdate = departdate;
	}

	public String getRootorigin() {
		return rootorigin;
	}

	public void setRootorigin(String rootorigin) {
		this.rootorigin = rootorigin;
	}

	public String getRootdestination() {
		return rootdestination;
	}

	public void setRootdestination(String rootdestination) {
		this.rootdestination = rootdestination;
	}

	public String getChild_departdate() {
		return child_departdate;
	}

	public void setChild_departdate(String child_departdate) {
		this.child_departdate = child_departdate;
	}

	public String getChild_arrivedate() {
		return child_arrivedate;
	}

	public void setChild_arrivedate(String child_arrivedate) {
		this.child_arrivedate = child_arrivedate;
	}

	public String getChild_departing() {
		return child_departing;
	}

	public void setChild_departing(String child_departing) {
		this.child_departing = child_departing;
	}

	public String getChild_arriving() {
		return child_arriving;
	}

	public void setChild_arriving(String child_arriving) {
		this.child_arriving = child_arriving;
	}

	public ArrayList<OceanScheduleSearchDetails> getTranshipmentResult() {
		return transhipmentResult;
	}

	public void setTranshipmentResult(ArrayList<OceanScheduleSearchDetails> transhipmentResult) {
		this.transhipmentResult = transhipmentResult;
	}

}
