package com.myshipment.app.model;

import java.util.ArrayList;

public class Flow {

	private String FlowID = "";
	private String Flow_Name = "";
	private String No_Of_Shipment = "";
	private String No_Of__Shipment = "";
	private String Overall_Percentage = "";
	private String Individual_Percentage = "";
	private String RGB = "";

	private String fromdate = "";
	private String todate = "";
	private String isOrigin = "";
	private String carriername = "";
	private String ILNCode = "";
	private String type = "";

	public String getFromdate() {
		return fromdate;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public String getTodate() {
		return todate;
	}

	public void setTodate(String todate) {
		this.todate = todate;
	}

	public String getIsOrigin() {
		return isOrigin;
	}

	public void setIsOrigin(String isOrigin) {
		this.isOrigin = isOrigin;
	}

	public String getCarriername() {
		return carriername;
	}

	public void setCarriername(String carriername) {
		this.carriername = carriername;
	}

	public String getILNCode() {
		return ILNCode;
	}

	public void setILNCode(String iLNCode) {
		ILNCode = iLNCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private ArrayList<CarrierDetail> CarrierDetails = new ArrayList<CarrierDetail>();

	public String getFlowID() {
		return FlowID;
	}

	public void setFlowID(String flowID) {
		FlowID = flowID;
	}

	public String getFlow_Name() {
		return Flow_Name;
	}

	public void setFlow_Name(String flow_Name) {
		Flow_Name = flow_Name;
	}

	public String getNo_Of_Shipment() {
		return No_Of_Shipment;
	}

	public void setNo_Of_Shipment(String no_Of_Shipment) {
		No_Of_Shipment = no_Of_Shipment;
	}

	public String getNo_Of__Shipment() {
		return No_Of__Shipment;
	}

	public void setNo_Of__Shipment(String no_Of_delay_Shipment) {
		No_Of__Shipment = no_Of_delay_Shipment;
	}

	public String getOverall_Percentage() {
		return Overall_Percentage;
	}

	public void setOverall_Percentage(String overall_Percentage) {
		Overall_Percentage = overall_Percentage;
	}

	public String getIndividual_Percentage() {
		return Individual_Percentage;
	}

	public void setIndividual_Percentage(String individual_Percentage) {
		Individual_Percentage = individual_Percentage;
	}

	public String getRGB() {
		return RGB;
	}

	public void setRGB(String rGB) {
		RGB = rGB;
	}

	public ArrayList<CarrierDetail> getCarrierDetails() {
		return CarrierDetails;
	}

	public void setCarrierDetails(ArrayList<CarrierDetail> carrierDetails) {
		CarrierDetails = carrierDetails;
	}

}
