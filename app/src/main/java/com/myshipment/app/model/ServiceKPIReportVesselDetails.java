package com.myshipment.app.model;

public class ServiceKPIReportVesselDetails {
	private String NoOfOntimeShipment = "NoOfOntimeShipment";
	private String VesselName = "VesselName";
	private String Reliability = "Reliability";
	private String NoOfDelayShipment_3to7 = "NoOfDelayShipment_3to7";
	private String NoOfDelayShipment_greater7 = "NoOfDelayShipment_greater7";

	public String getNoOfOntimeShipment() {
		return NoOfOntimeShipment;
	}

	public void setNoOfOntimeShipment(String noOfOntimeShipment) {
		NoOfOntimeShipment = noOfOntimeShipment;
	}

	public String getVesselName() {
		return VesselName;
	}

	public void setVesselName(String vesselName) {
		VesselName = vesselName;
	}

	public String getReliability() {
		return Reliability;
	}

	public void setReliability(String reliability) {
		Reliability = reliability;
	}

	public String getNoOfDelayShipment_3to7() {
		return NoOfDelayShipment_3to7;
	}

	public void setNoOfDelayShipment_3to7(String noOfDelayShipment_3to7) {
		NoOfDelayShipment_3to7 = noOfDelayShipment_3to7;
	}

	public String getNoOfDelayShipment_greater7() {
		return NoOfDelayShipment_greater7;
	}

	public void setNoOfDelayShipment_greater7(String noOfDelayShipment_greater7) {
		NoOfDelayShipment_greater7 = noOfDelayShipment_greater7;
	}
}
