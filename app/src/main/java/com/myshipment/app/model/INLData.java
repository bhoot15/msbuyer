package com.myshipment.app.model;

public class INLData {

	private String id = "id";
	private String ILNname = "";
	private String TotalShipment = "";
	private String TotalDelayed = "";
	private String TotalIntransit = "";
	private String TotalReachedOnTime = "";
	private String DelayRate = "";
	public int Selected = 0;

	private String fromDate = "";
	private String toDate = "";
	private String isOrigin = "";
	private String maxcontainercount = "";

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getIsOrigin() {
		return isOrigin;
	}

	public void setIsOrigin(String isOrigin) {
		this.isOrigin = isOrigin;
	}

	public String getMaxcontainercount() {
		return maxcontainercount;
	}

	public void setMaxcontainercount(String maxcontainercount) {
		this.maxcontainercount = maxcontainercount;
	}

	public int getSelected() {
		return Selected;
	}

	public void setSelected(int selected) {
		Selected = selected;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getILNname() {
		return ILNname;
	}

	public void setILNname(String iLNname) {
		ILNname = iLNname;
	}

	public String getTotalShipment() {
		return TotalShipment;
	}

	public void setTotalShipment(String totalShipment) {
		TotalShipment = totalShipment;
	}

	public String getTotalDelayed() {
		return TotalDelayed;
	}

	public void setTotalDelayed(String totalDelayed) {
		TotalDelayed = totalDelayed;
	}

	public String getTotalIntransit() {
		return TotalIntransit;
	}

	public void setTotalIntransit(String totalIntransit) {
		TotalIntransit = totalIntransit;
	}

	public String getTotalReachedOnTime() {
		return TotalReachedOnTime;
	}

	public void setTotalReachedOnTime(String totalReachedOnTime) {
		TotalReachedOnTime = totalReachedOnTime;
	}

	public String getDelayRate() {
		return DelayRate;
	}

	public void setDelayRate(String delayRate) {
		DelayRate = delayRate;
	}

}
