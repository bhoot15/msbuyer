package com.myshipment.app.model;

import java.util.ArrayList;

public class CarrierDetail {

	private String Carrier_Name = "";
	private String Container_Number = "";
	private String POL_POD = "";
	private String Initial = "";
	private String Real = "";
	private String Delay_Days = "";

	private String Departing_port = "";
	private String Departing_Country = "";
	private String Arriving_port = "";
	private String Arriving_country = "";

	private String port_of_call = "";
	private ArrayList<CarrierDetail> moredata = new ArrayList<CarrierDetail>();

	private String fromdate = "";
	private String todate = "";
	private String isOrigin = "";
	private String carriername = "";
	private String ILNCode = "";
	private String type = "";

	public String getFromdate() {
		return fromdate;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public String getTodate() {
		return todate;
	}

	public void setTodate(String todate) {
		this.todate = todate;
	}

	public String getIsOrigin() {
		return isOrigin;
	}

	public void setIsOrigin(String isOrigin) {
		this.isOrigin = isOrigin;
	}

	public String getCarriername() {
		return carriername;
	}

	public void setCarriername(String carriername) {
		this.carriername = carriername;
	}

	public String getILNCode() {
		return ILNCode;
	}

	public void setILNCode(String iLNCode) {
		ILNCode = iLNCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPort_of_call() {
		return port_of_call;
	}

	public void setPort_of_call(String port_of_call) {
		this.port_of_call = port_of_call;
	}

	public ArrayList<CarrierDetail> getMoredata() {
		return moredata;
	}

	public void setMoredata(ArrayList<CarrierDetail> moredata) {
		this.moredata = moredata;
	}

	public String getDeparting_port() {
		return Departing_port;
	}

	public void setDeparting_port(String departing_port) {
		Departing_port = departing_port;
	}

	public String getDeparting_Country() {
		return Departing_Country;
	}

	public void setDeparting_Country(String departing_Country) {
		Departing_Country = departing_Country;
	}

	public String getArriving_port() {
		return Arriving_port;
	}

	public void setArriving_port(String arriving_port) {
		Arriving_port = arriving_port;
	}

	public String getArriving_country() {
		return Arriving_country;
	}

	public void setArriving_country(String arriving_country) {
		Arriving_country = arriving_country;
	}

	public String getCarrier_Name() {
		return Carrier_Name;
	}

	public void setCarrier_Name(String carrier_Name) {
		Carrier_Name = carrier_Name;
	}

	public String getContainer_Number() {
		return Container_Number;
	}

	public void setContainer_Number(String container_Number) {
		Container_Number = container_Number;
	}

	public String getPOL_POD() {
		return POL_POD;
	}

	public void setPOL_POD(String pOL_POD) {
		POL_POD = pOL_POD;
	}

	public String getInitial() {
		return Initial;
	}

	public void setInitial(String initial) {
		Initial = initial;
	}

	public String getReal() {
		return Real;
	}

	public void setReal(String real) {
		Real = real;
	}

	public String getDelay_Days() {
		return Delay_Days;
	}

	public void setDelay_Days(String delay_Days) {
		Delay_Days = delay_Days;
	}

}
