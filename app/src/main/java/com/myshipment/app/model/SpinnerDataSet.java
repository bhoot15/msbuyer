package com.myshipment.app.model;

import java.util.ArrayList;

public class SpinnerDataSet {

	private String id = "";
	private String name = "";
	
	private ArrayList<SpinnerDataSet> arra =new ArrayList<SpinnerDataSet>();

	public ArrayList<SpinnerDataSet> getArra() {
		return arra;
	}

	public void setArra(ArrayList<SpinnerDataSet> arra) {
		this.arra = arra;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
