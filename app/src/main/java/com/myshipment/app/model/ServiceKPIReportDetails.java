package com.myshipment.app.model;

import java.util.ArrayList;

public class ServiceKPIReportDetails {

	private String servicelist = "";
	private String pol = "";
	private String reliability = "";
	private String pod = "";
	private String no_of_delayshipment_greater_7 = "";
	private String no_of_ontimeshipment = "";
	private String reliabilityno_of_shipment = "";
	private String no_of_delayshipment_3_to_7 = "";
	private ArrayList<ServiceKPIReportVesselDetails> vesselList = new ArrayList<ServiceKPIReportVesselDetails>();

	public ArrayList<ServiceKPIReportVesselDetails> getVesselList() {
		return vesselList;
	}

	public void setVesselList(ArrayList<ServiceKPIReportVesselDetails> vesselList) {
		this.vesselList = vesselList;
	}

	public String getServicelist() {
		return servicelist;
	}

	public void setServicelist(String servicelist) {
		this.servicelist = servicelist;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getReliability() {
		return reliability;
	}

	public void setReliability(String reliability) {
		this.reliability = reliability;
	}

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public String getNo_of_delayshipment_greater_7() {
		return no_of_delayshipment_greater_7;
	}

	public void setNo_of_delayshipment_greater_7(String no_of_delayshipment_greater_7) {
		this.no_of_delayshipment_greater_7 = no_of_delayshipment_greater_7;
	}

	public String getNo_of_ontimeshipment() {
		return no_of_ontimeshipment;
	}

	public void setNo_of_ontimeshipment(String no_of_ontimeshipment) {
		this.no_of_ontimeshipment = no_of_ontimeshipment;
	}

	public String getReliabilityno_of_shipment() {
		return reliabilityno_of_shipment;
	}

	public void setReliabilityno_of_shipment(String reliabilityno_of_shipment) {
		this.reliabilityno_of_shipment = reliabilityno_of_shipment;
	}

	public String getNo_of_delayshipment_3_to_7() {
		return no_of_delayshipment_3_to_7;
	}

	public void setNo_of_delayshipment_3_to_7(String no_of_delayshipment_3_to_7) {
		this.no_of_delayshipment_3_to_7 = no_of_delayshipment_3_to_7;
	}

}
