package com.myshipment.app.model;

public class TrackResultDetail {
	private String VesselName = "";
	private String Date = "";
	private String Voyage = "";
	private String StatusDescription = "";
	private String Location = "";

	public String getVesselName() {
		return VesselName;
	}

	public void setVesselName(String vesselName) {
		VesselName = vesselName;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getVoyage() {
		return Voyage;
	}

	public void setVoyage(String voyage) {
		Voyage = voyage;
	}

	public String getStatusDescription() {
		return StatusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		StatusDescription = statusDescription;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}
}
