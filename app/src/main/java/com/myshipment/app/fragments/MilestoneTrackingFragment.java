package com.myshipment.app.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myshipment.R;
import com.myshipment.app.Utility.GeneralUtils;
import com.myshipment.app.Utility.SharedPreferenceManager;
import com.myshipment.app.activity.NewDashboardActivity;
import com.myshipment.app.adapters.ItemDetailAdapter;
import com.myshipment.app.adapters.ProductDetailsAdapter;
import com.myshipment.app.adapters.TimelineViewAdapter;
import com.myshipment.app.beans.ArrayListManagerModel;


/**
 * Created by Akshay Thapliyal on 26-05-2016.
 */
public class MilestoneTrackingFragment extends Fragment implements View.OnClickListener {

    private RecyclerView timelineRecyclerView, productDetailsRecycler, scheduleRecycler;
    private RecyclerView.LayoutManager layoutManager;
    //private ScrollView milestoneScrollView;
    private RelativeLayout bottomDetailsHeader, allDetailsLayout;
    private Toolbar toolbar;
    private ImageView menuIcon, packageArrow, scheduleArrow;
    private TextView tvTitleToolbar, startingPointTV, endingPointTV, eta_value, etd_value;
    private boolean isPackageOpen = true, isScheduleOpen = false;
    private RelativeLayout headerLayout, scheduleHeader;

    public MilestoneTrackingFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.milestone_tracking_fragment, container, false);

        init(view);
        setListeners();
        setAdapter();

        tvTitleToolbar.setText("Tracking Details");
        String pod = SharedPreferenceManager.getPod(getContext());
        String pol = SharedPreferenceManager.getPol(getContext());
        String eta = SharedPreferenceManager.getEta(getContext());
        String etd = SharedPreferenceManager.getEtd(getContext());

        System.out.println("pod = "+pod+" pol = "+pol+" eta = "+eta+" etd = "+etd);

        startingPointTV.setText(pol);
        endingPointTV.setText(pod);
        eta_value.setText(GeneralUtils.getDate(eta));
        etd_value.setText(GeneralUtils.getDate(etd));

        /*milestoneScrollView.fullScroll(ScrollView.FOCUS_UP);
        milestoneScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                Rect scrollBounds = new Rect();
                milestoneScrollView.getHitRect(scrollBounds);

            }
        });*/

        return view;
    }

    private void init(View v) {
        scheduleHeader = (RelativeLayout) v.findViewById(R.id.scheduleHeader);
        headerLayout = (RelativeLayout) v.findViewById(R.id.headerLayout);
        timelineRecyclerView = (RecyclerView) v.findViewById(R.id.timelineRecyclerView);
        productDetailsRecycler = (RecyclerView) v.findViewById(R.id.productDetailsRecycler);
        //milestoneScrollView = (ScrollView) v.findViewById(R.id.milestoneScrollView);
        scheduleRecycler = (RecyclerView) v.findViewById(R.id.scheduleRecycler);
        //bottomDetailsHeader = (RelativeLayout)v.findViewById(R.id.bottomDetailsHeader);
        allDetailsLayout = (RelativeLayout) v.findViewById(R.id.allDetailsLayout);
        toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        menuIcon = (ImageView) toolbar.findViewById(R.id.menuIcon);
        tvTitleToolbar = (TextView) toolbar.findViewById(R.id.tvTitleToolbar);
        scheduleArrow = (ImageView) v.findViewById(R.id.scheduleArrow);
        packageArrow = (ImageView) v.findViewById(R.id.packageArrow);

        startingPointTV = (TextView) v.findViewById(R.id.startingPointTV);
        endingPointTV = (TextView) v.findViewById(R.id.endingPointTV);
        eta_value = (TextView) v.findViewById(R.id.eta_value);
        etd_value = (TextView) v.findViewById(R.id.etd_value);
    }

    private void setListeners() {
        menuIcon.setOnClickListener(this);
        headerLayout.setOnClickListener(this);
        scheduleHeader.setOnClickListener(this);
    }

    private void setAdapter() {

        timelineRecyclerView.setNestedScrollingEnabled(false);

        timelineRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        timelineRecyclerView.setLayoutManager(layoutManager);
        timelineRecyclerView.setItemAnimator(new DefaultItemAnimator());
        TimelineViewAdapter timelineViewAdapter = new TimelineViewAdapter(getActivity(), ArrayListManagerModel.getInstance());
        timelineRecyclerView.setAdapter(timelineViewAdapter);

        //set Adapter for Product details
        productDetailsRecycler.setNestedScrollingEnabled(false);

        productDetailsRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        productDetailsRecycler.setLayoutManager(layoutManager);
        productDetailsRecycler.setItemAnimator(new DefaultItemAnimator());
        ProductDetailsAdapter productDetailsAdapter = new ProductDetailsAdapter(getActivity(), ArrayListManagerModel.getInstance());
        productDetailsRecycler.setAdapter(productDetailsAdapter);

        //set Adapter for Product details
        scheduleRecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        scheduleRecycler.setLayoutManager(layoutManager);
        scheduleRecycler.setItemAnimator(new DefaultItemAnimator());
        ItemDetailAdapter scheduleDetailAdapter = new ItemDetailAdapter(getActivity(), ArrayListManagerModel.getInstance());
        scheduleRecycler.setAdapter(scheduleDetailAdapter);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == menuIcon.getId()) {
            Intent intent = new Intent(getContext(), NewDashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        } else if (v.getId() == headerLayout.getId()) {
            if (isPackageOpen) {
                productDetailsRecycler.setVisibility(View.GONE);
                packageArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
                isPackageOpen = false;
            } else {
                productDetailsRecycler.setVisibility(View.VISIBLE);
                packageArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up));
                isPackageOpen = true;

                if (isScheduleOpen) {
                    scheduleRecycler.setVisibility(View.GONE);
                    scheduleArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
                    isScheduleOpen = false;
                }
            }

        } else if (v.getId() == scheduleHeader.getId()) {
            if (isScheduleOpen) {
                scheduleRecycler.setVisibility(View.GONE);
                scheduleArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
                isScheduleOpen = false;
            } else {
                scheduleRecycler.setVisibility(View.VISIBLE);
                isScheduleOpen = true;
                scheduleArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up));
                if (isPackageOpen) {
                    productDetailsRecycler.setVisibility(View.GONE);
                    isPackageOpen = false;
                    packageArrow.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
                }
            }

        }
    }
}
